import {
  VariableByName,
  walkParameters,
  type NodeParameter,
} from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const parametersFilePath = path.join(
    options.jsonDir,
    "editable_processed_parameters.json",
  )
  const parameters = (await fs.readJson(parametersFilePath)) as NodeParameter

  const variablesSummaryFilePath = path.join(
    options.jsonDir,
    "variables_summaries.json",
  )
  const variableSummaryByName = (await fs.readJson(
    variablesSummaryFilePath,
  )) as VariableByName

  const usedParametersName = new Set<string>()
  for (const variableSummary of Object.values(variableSummaryByName)) {
    if (variableSummary.formulas === undefined) {
      continue
    }
    for (const formula of Object.values(variableSummary.formulas)) {
      if (formula === null || formula.parameters === undefined) {
        continue
      }
      for (const parameterName of formula.parameters) {
        usedParametersName.add(parameterName)
      }
    }
  }

  const unusedParametersData: Array<{ description?: string; name?: string }> =
    []
  nextParameter: for (const parameter of walkParameters(parameters)) {
    for (const usedParameterName of usedParametersName) {
      if (
        parameter.name === usedParameterName ||
        (parameter.name as string).startsWith(usedParameterName + ".")
      ) {
        continue nextParameter
      }
    }
    unusedParametersData.push({
      description: parameter.description,
      name: parameter.name,
    })
  }

  await fs.writeFile(
    path.join(options.jsonDir, "unused_parameters.csv"),
    Papa.unparse(unusedParametersData, {
      columns: ["name", "description"],
    }),
  )
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
