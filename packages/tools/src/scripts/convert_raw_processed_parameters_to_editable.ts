import { strictAudit } from "@auditors/core"
import {
  auditRawParameterToEditable,
  auditUnits,
  jsonReplacer,
  unitsFromYaml,
  type NodeParameter,
  type Unit,
} from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main(): Promise<number> {
  const [units, unitsError] = auditUnits(
    strictAudit,
    unitsFromYaml(
      await fs.readFile(path.join(options.jsonDir, "units.yaml"), "utf-8"),
    ),
  ) as [Unit[], unknown]
  if (unitsError !== null) {
    console.log(`An error occurred when validating units metadata:`)
    console.error(JSON.stringify(unitsError, null, 2))
    return 1
  }

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const parameters = (await fs.readJson(
      path.join(taxBenefitDir, "raw_processed_parameters.json"),
    )) as NodeParameter
    const [leximpactParameter, error] = auditRawParameterToEditable(units)(
      strictAudit,
      parameters,
    )
    if (error !== null) {
      // console.error(JSON.stringify(leximpactParameter, null, 2))
      console.error(JSON.stringify(error, null, 2))
      return 1
    }
    await fs.writeJson(
      path.join(taxBenefitDir, "editable_processed_parameters.json"),
      leximpactParameter,
      {
        replacer: jsonReplacer,
        spaces: 2,
      },
    )
  }
  return 0
}

main()
  .then((exitCode) => {
    process.exit(exitCode)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(2)
  })
