import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import {
  auditSituation,
  jsonReplacer,
  type EntityByKey,
  type Situation,
  type VariableByName,
} from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "S",
    help: "value of Smic",
    name: "smic",
    required: true,
    type: Number,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    alias: "y",
    help: "simulation year",
    name: "year",
    required: true,
    type: Number,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const entityByKey = (await fs.readJson(
    path.join(options.jsonDir, "entities.json"),
  )) as EntityByKey
  const variableByName = (await fs.readJson(
    path.join(options.jsonDir, "variables_summaries.json"),
  )) as VariableByName
  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const reformDir of reformDirArray) {
    const reformChangeVariableByName = (await fs.readJson(
      path.join(reformDir, "variables_changes_summaries.json"),
    )) as VariableByName
    for (const [variableName, variable] of Object.entries(
      reformChangeVariableByName,
    )) {
      variableByName[variableName] ??= variable
    }
  }

  // First pass
  const testCasesDir = path.join(options.jsonDir, "custom", "test_cases")
  if (!(await fs.pathExists(testCasesDir))) {
    return
  }
  const testCases: Situation[] = []
  const testCasesIds: string[] = []
  for (const testCaseFilename of (await fs.readdir(testCasesDir)).sort()) {
    if (testCaseFilename.startsWith(".")) {
      continue
    }
    if (!testCaseFilename.endsWith(".json")) {
      continue
    }
    const [testCase, error] = auditChain(
      auditSituation(
        options.smic,
        options.year,
        entityByKey,
        undefined,
        variableByName,
        false,
      ),
      auditRequire,
    )(
      strictAudit,
      await fs.readJson(path.join(testCasesDir, testCaseFilename)),
    ) as [Situation, unknown]
    if (error !== null) {
      console.error(`Errors in test case "${testCaseFilename}":`)
      console.error(JSON.stringify(testCase, null, 2))
      console.error(JSON.stringify(error, null, 2))
      process.exit(1)
    }

    testCase.id = testCaseFilename.replace(/\.json$/, "")
    testCasesIds.push(testCase.id)
    testCases.push(testCase)
  }

  // Second pass
  for (const [testCaseIndex, testCaseCandidate] of testCases.entries()) {
    const [testCase, error] = auditChain(
      auditSituation(
        options.smic,
        options.year,
        entityByKey,
        testCasesIds,
        variableByName,
        true,
      ),
      auditRequire,
    )(strictAudit, testCaseCandidate) as [Situation, unknown]
    if (error !== null) {
      console.error(`Errors in test case number ${testCaseIndex}:`)
      console.error(JSON.stringify(testCase, null, 2))
      console.error(JSON.stringify(error, null, 2))
      process.exit(1)
    }
    testCases[testCaseIndex] = testCase
  }

  await fs.writeJson(path.join(options.jsonDir, `test_cases.json`), testCases, {
    replacer: jsonReplacer,
    spaces: 2,
  })
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
