import {
  type Audit,
  auditArray,
  auditDateIso8601String,
  auditFunction,
  auditHttpUrl,
  auditInteger,
  auditKeyValueDictionary,
  auditNullish,
  auditNumber,
  auditOptions,
  type Auditor,
  auditRequire,
  auditString,
  auditSwitch,
  auditTest,
  auditTrimString,
  cleanAudit,
  auditStringToNumber,
} from "@auditors/core"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import {
  jsonReplacer,
  type Percentile,
  type Variable,
} from "@openfisca/json-model"

type CategoricalValues = { [key: string]: number | "secret" }

interface DataItem extends DataItemBase {
  data_structure: "dict"
  values: CategoricalValues | NumericalValues
}

interface DataItemBase {
  data_structure: string
  date: "2018" | "2019"
  extraction_date: string
  reference: Array<{
    href: string
    title: string
  }>
}

interface NumericalValues {
  lenzero: number
  mean: number
  nb_line: number
  pct_zero: number
  sum: number
}

interface PercentileDataItem extends DataItemBase {
  data_structure: "decile_10"
  values: PercentileValue[]
}

interface PercentileValue {
  bucket_count: number
  bucket_mean: number
  bucket_stdev: number
  bucket_sum: number
  count_above_upper_bound: number
  lower_bound: number
  mean_above_upper_bound: number
  ratio_count_above_upper_bound: number
  sum_above_upper_bound: number
  upper_bound: number
}

interface VariablePoteBase {
  description?: string
  label_of_keys?: { [key: string]: string }
  openfisca_variable?: string
  perimeter: {
    entity: "foyer"
    geographic: "France entière"
    period: "year"
  }
  short_name?: string
  source_variable: string
  unit: "foyer" | "euros"
}

interface VariablePotePercentile extends VariablePoteBase {
  data: PercentileDataItem
}

interface VariablePoteMixed extends VariablePoteBase {
  data: Array<DataItem>
}

const optionsDefinitions = [
  {
    alias: "a",
    help: "directory of project leximpact-aggregates",
    name: "aggregatesDir",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

const auditCategoricalValues = auditKeyValueDictionary(auditString, [
  auditSwitch(auditOptions(["secret"]), auditNumber),
  auditRequire,
])

function auditDataItem(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  auditDataItemBaseContent(audit, data, errors, remainingKeys)

  audit.attribute(
    data,
    "data_structure",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["dict"]),
    auditRequire,
  )
  audit.attribute(
    data,
    "values",
    true,
    errors,
    remainingKeys,
    auditSwitch(auditNumericalValues, auditCategoricalValues),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditDataItemBaseContent(
  audit: Audit,
  data: { [key: string]: unknown },
  errors: { [key: string]: unknown },
  remainingKeys: Set<string>,
): void {
  audit.attribute(
    data,
    "date",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["2018", "2019"]),
    auditRequire,
  )
  audit.attribute(
    data,
    "extraction_date",
    true,
    errors,
    remainingKeys,
    auditDateIso8601String,
    auditRequire,
  )
  audit.attribute(
    data,
    "reference",
    true,
    errors,
    remainingKeys,
    auditArray(auditReference, auditRequire),
    auditRequire,
  )
}

function auditNumericalValues(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["lenzero", "mean", "nb_line", "sum"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditInteger,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "pct_zero",
    true,
    errors,
    remainingKeys,
    auditNumber,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditPercentileDataItem(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  auditDataItemBaseContent(audit, data, errors, remainingKeys)

  audit.attribute(
    data,
    "data_structure",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["distribution_10"]),
    auditRequire,
  )
  audit.attribute(
    data,
    "values",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(
      [auditStringToNumber, auditInteger],
      auditPercentileValue,
    ),
    auditFunction((values) =>
      Object.entries(values)
        .sort(([key1], [key2]) => parseInt(key1) - parseInt(key2))
        .map(([, value]) => value),
    ),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditPercentileValue(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["bucket_count", "count_above_upper_bound"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditInteger,
      auditRequire,
    )
  }
  for (const key of [
    "bucket_mean",
    "bucket_stdev",
    "bucket_sum",
    "lower_bound",
    "mean_above_upper_bound",
    "ratio_count_above_upper_bound",
    "sum_above_upper_bound",
    "upper_bound",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditNumber,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditPerimeter(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "entity",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["foyer"]),
    auditRequire,
  )
  audit.attribute(
    data,
    "geographic",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["France entière"]),
    auditRequire,
  )
  audit.attribute(
    data,
    "period",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["year"]),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditReference(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "href",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )
  audit.attribute(
    data,
    "title",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditVariablePoteBaseContent(
  audit: Audit,
  data: { [key: string]: unknown },
  errors: { [key: string]: unknown },
  remainingKeys: Set<string>,
): void {
  for (const key of ["description", "short_name"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  audit.attribute(
    data,
    "label_of_keys",
    true,
    errors,
    remainingKeys,
    auditSwitch(
      [auditString, auditTrimString, auditNullish],
      auditKeyValueDictionary(auditString, [auditTrimString, auditRequire]),
    ),
  )
  for (const key of ["source_variable"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  for (const key of ["openfisca_variable", "ux_template"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  audit.attribute(
    data,
    "perimeter",
    true,
    errors,
    remainingKeys,
    auditPerimeter,
    auditRequire,
  )
  audit.attribute(
    data,
    "unit",
    true,
    errors,
    remainingKeys,
    auditString,
    auditOptions(["foyer", "euros"]),
    auditRequire,
  )

  if (
    errors.data === undefined &&
    errors.label_of_keys === undefined &&
    data.label_of_keys !== undefined
  ) {
    const keys = Object.keys(data.label_of_keys as { [key: string]: string })
    for (const [index, dataItem] of (data.data as DataItem[]).entries()) {
      const [, valuesError] = auditKeyValueDictionary(auditOptions(keys), [])(
        cleanAudit,
        dataItem.values,
      )
      if (valuesError !== null) {
        if (errors.data === undefined) {
          errors.data = {}
        }
        ;(errors.data as { [key: string]: unknown })[index] = {
          values: valuesError,
        }
      }
    }
  }
}

function auditVariablePotePercentile(date: string): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    auditVariablePoteBaseContent(audit, data, errors, remainingKeys)

    audit.attribute(
      data,
      "data",
      true,
      errors,
      remainingKeys,
      auditArray(auditPercentileDataItem, auditRequire),
      auditTest(
        (data: DataItem[]) => data.length === 1,
        "In a POTE percentile file, data should be an array of exactly one item",
      ),
      auditFunction((data) => data[0]),
      auditTest(
        (data: DataItem) => data.date === date,
        "Date of percentile doesn't match date of its directory",
      ),
      auditRequire,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

function auditVariablePoteMixed(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  auditVariablePoteBaseContent(audit, data, errors, remainingKeys)

  audit.attribute(
    data,
    "data",
    true,
    errors,
    remainingKeys,
    auditArray(auditDataItem, auditRequire),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

async function main() {
  assert(
    await fs.pathExists(options.aggregatesDir),
    `Directory of leximpact-aggregates project "${options.aggregatesDir}" not found`,
  )
  const poteDir = path.join(options.aggregatesDir, "aggregates", "POTE")
  assert(
    await fs.pathExists(poteDir),
    `Directory of POTE aggregates "${poteDir}" not found`,
  )
  const poteDecileDir = path.join(poteDir, "distribution_10")
  assert(
    await fs.pathExists(poteDecileDir),
    `Directory of POTE decile aggregates "${poteDecileDir}" not found`,
  )
  const poteMixedDir = path.join(poteDir, "mixed")
  assert(
    await fs.pathExists(poteMixedDir),
    `Directory of POTE mixed aggregates "${poteMixedDir}" not found`,
  )

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const variablesDir = path.join(taxBenefitDir, "variables")
    for (const variableFilename of await fs.readdir(variablesDir)) {
      if (variableFilename.startsWith(".")) {
        continue
      }
      if (!variableFilename.endsWith(".json")) {
        continue
      }
      const variablePoteMixedFilePath = path.join(
        poteMixedDir,
        variableFilename,
      )
      if (!(await fs.pathExists(variablePoteMixedFilePath))) {
        continue
      }

      const [variablePoteMixed, variablePoteMixedError] =
        auditVariablePoteMixed(
          cleanAudit,
          await fs.readJson(variablePoteMixedFilePath),
        ) as [VariablePoteMixed, unknown]
      if (variablePoteMixedError !== null) {
        console.error(
          `Errors in JSON of ${variablePoteMixedFilePath}:\n${JSON.stringify(
            variablePoteMixed,
            null,
            2,
          )}\n${JSON.stringify(variablePoteMixedError, null, 2)}`,
        )
        process.exit(1)
      }

      const variableName = variableFilename.replace(/\.json$/, "")
      if (
        variableName !==
        (variablePoteMixed.openfisca_variable ??
          variablePoteMixed.source_variable)
      ) {
        console.error(
          `Invalid name of OpenFisca variable in ${variablePoteMixedFilePath}:\n${JSON.stringify(
            variablePoteMixed,
            null,
            2,
          )}\n"${
            variablePoteMixed.openfisca_variable ??
            variablePoteMixed.source_variable
          }" differs from "${variableName}".`,
        )
        process.exit(1)
      }

      if (
        (variablePoteMixed.data[0]?.values as NumericalValues)?.sum !==
        undefined
      ) {
        let latestYear: string | undefined
        let latestDeciles: Percentile[] | undefined = undefined
        for (const dataItem of variablePoteMixed.data) {
          const variablePoteDecileFilePath = path.join(
            poteDecileDir,
            dataItem.date,
            variableFilename,
          )
          if (!(await fs.pathExists(variablePoteDecileFilePath))) {
            continue
          }
          const [variablePoteDecile, variablePoteDecileError] =
            auditVariablePotePercentile(dataItem.date)(
              cleanAudit,
              await fs.readJson(variablePoteDecileFilePath),
            ) as [VariablePotePercentile, unknown]
          if (variablePoteDecileError !== null) {
            console.error(
              `Errors in JSON of ${variablePoteDecileFilePath}:\n${JSON.stringify(
                variablePoteDecile,
                null,
                2,
              )}\n${JSON.stringify(variablePoteDecileError, null, 2)}`,
            )
            process.exit(1)
          }

          if (latestYear === undefined || dataItem.date > latestYear) {
            latestDeciles = variablePoteDecile.data.values
            latestYear = dataItem.date
          }
        }

        if (latestYear !== undefined) {
          const variableFilePath = path.join(variablesDir, variableFilename)
          const variable = (await fs.readJson(variableFilePath)) as Variable
          variable.aggregate = {
            deciles: latestDeciles,
            year: parseInt(latestYear as string),
          }
          await fs.writeJson(variableFilePath, variable, {
            replacer: jsonReplacer,
            spaces: 2,
          })
        }
      }
    }
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
