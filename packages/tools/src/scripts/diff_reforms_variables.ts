import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const taxBenefitVariablesDir = path.join(options.jsonDir, "variables")

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const reformDir of reformDirArray) {
    const remainingTaxBenefitVariablesFilename = new Set(
      (await fs.readdir(taxBenefitVariablesDir)).filter(
        (variableFilename) =>
          !variableFilename.startsWith(".") &&
          variableFilename.endsWith(".json"),
      ),
    )

    const changedVariablesDir = path.join(reformDir, "variables_changes")
    await fs.remove(changedVariablesDir)
    await fs.ensureDir(changedVariablesDir)

    const reformVariablesDir = path.join(reformDir, "variables")
    for (const variableFilename of await fs.readdir(reformVariablesDir)) {
      if (variableFilename.startsWith(".")) {
        continue
      }
      if (!variableFilename.endsWith(".json")) {
        continue
      }
      const reformVariableFilePath = path.join(
        reformVariablesDir,
        variableFilename,
      )
      if (remainingTaxBenefitVariablesFilename.delete(variableFilename)) {
        const taxBenefitVariableFilePath = path.join(
          taxBenefitVariablesDir,
          variableFilename,
        )
        const taxBenefitVariableJson = await fs.readFile(
          taxBenefitVariableFilePath,
          "utf-8",
        )
        const reformVariableJson = await fs.readFile(
          reformVariableFilePath,
          "utf-8",
        )
        if (taxBenefitVariableJson === reformVariableJson) {
          continue
        }
        if (!options.silent) {
          console.log(
            `Keeping reform variable ${reformVariableFilePath}, because is different from ${taxBenefitVariableFilePath}.`,
          )
        }
      }
      await fs.copy(
        reformVariableFilePath,
        path.join(changedVariablesDir, variableFilename),
      )
    }

    for (const variableFilename of remainingTaxBenefitVariablesFilename) {
      await fs.writeJson(path.join(changedVariablesDir, variableFilename), null)
    }

    if ((await fs.readdir(changedVariablesDir)).length === 0) {
      await fs.remove(changedVariablesDir)
    }
    await fs.remove(reformVariablesDir)
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
