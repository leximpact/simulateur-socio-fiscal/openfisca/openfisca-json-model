import { $, cd, /* fs, */ path } from "zx"

async function main() {
  // const aggregatesDir = path.join("..", "..", "..", "leximpact-aggregates")
  const jsonDir = path.join(
    "..",
    "..",
    "..",
    "leximpact-socio-fiscal-openfisca-json",
  )
  const openfiscaPackageName = "openfisca_france_with_indirect_taxation"

  cd("../extractor")
  await $`poetry run python -m openfisca_json_extractor.scripts.export_tax_benefit_system --country-package ${openfiscaPackageName} --data-dir ${jsonDir}`
  await $`poetry run python -m openfisca_json_extractor.scripts.export_reforms --country-package ${openfiscaPackageName} --data-dir ${jsonDir}`
  cd("../tools")

  await $`node build/scripts/convert_raw_unprocessed_parameters_to_editable_to_raw.js --silent ${jsonDir}`

  await $`node build/scripts/convert_raw_processed_parameters_to_editable.js ${jsonDir}`
  await $`node build/scripts/validate_editable_processed_parameters.js ${jsonDir}`
  await $`node build/scripts/add_parameters_and_variables_to_formulas.js --silent ${jsonDir}`
  await $`node build/scripts/add_referring_variables.js --silent ${jsonDir}`
  // if (await fs.pathExists(aggregatesDir)) {
  //   await $`node build/scripts/add_variables_aggregates.js -a ${aggregatesDir} ${jsonDir}`
  // }

  // Create decompositions & customizations.
  await $`node build/scripts/customize_variables.js ${jsonDir}`
  await $`node build/scripts/extract_decompositions.js --silent ${jsonDir}`
  await $`node build/scripts/extract_decompositions.js --customizations --silent ${jsonDir}`
  await $`node build/scripts/customize_decompositions.js ${jsonDir}`
  await $`node build/scripts/add_unit_to_decomposition_variables.js ${jsonDir}`

  await $`node build/scripts/diff_reforms_parameters.js ${jsonDir}`
  await $`node build/scripts/diff_reforms_variables.js ${jsonDir}`
  await $`node build/scripts/merge_variables_summaries.js ${jsonDir}`
  await $`node build/scripts/diff_reforms_decompositions.js ${jsonDir}`
  await $`node build/scripts/merge_reforms.js ${jsonDir}`
  await $`node build/scripts/merge_test_cases.js ${jsonDir} --smic=21627 --year 2025`

  await $`node build/scripts/clean_metadata.js ${jsonDir}`
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
