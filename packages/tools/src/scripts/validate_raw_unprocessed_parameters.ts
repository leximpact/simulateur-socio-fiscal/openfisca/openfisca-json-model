import { strictAudit } from "@auditors/core"
import {
  auditEditableParameter,
  auditRawParameterToEditable,
  auditUnits,
  rawParameterFromYaml,
  unitsFromYaml,
  type Unit,
  type Parameter,
} from "@openfisca/json-model"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { walkDir } from "../file_systems"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "u",
    help: "path of units.yaml file",
    name: "units",
    type: String,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the YAML parameters of an OpenFisca (country) package",
    name: "parametersDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main(): Promise<number> {
  assert.notStrictEqual(
    options.units,
    undefined,
    "Missing required --units argument",
  )
  const [units, unitsError] = auditUnits(
    strictAudit,
    unitsFromYaml(await fs.readFile(options.units, "utf-8")),
  ) as [Unit[], unknown]
  if (unitsError !== null) {
    console.log(
      `An error occurred when validating units metadata at "${options.units}":`,
    )
    console.error(JSON.stringify(unitsError, null, 2))
    return 1
  }

  let exitCode = 0
  for (const relativeSplitPath of walkDir(options.parametersDir)) {
    const filePath = path.join(options.parametersDir, ...relativeSplitPath)
    if (!filePath.endsWith(".yaml")) {
      continue
    }

    if (!options.silent) {
      console.log(`Validating ${filePath}…`)
    }
    let rawParameter = rawParameterFromYaml(
      await fs.readFile(filePath, "utf-8"),
    )
    const name = relativeSplitPath
      .join(".")
      .replace(/\.yaml$/, "")
      .replace(/\.index$/, "")

    if (path.basename(filePath) === "index.yaml") {
      // Add empty children to ParameterNode (needed to validate `order` attribute).
      if (rawParameter == null) {
        rawParameter = {}
      }
      for (const childFilename of await fs.readdir(path.dirname(filePath))) {
        if (childFilename.startsWith(".") || childFilename === "index.yaml") {
          continue
        }
        const childName = childFilename.replace(/\.yaml$/, "")
        assert.strictEqual(
          (rawParameter as { [key: string]: undefined })[childName],
          undefined,
        )
        ;(rawParameter as { [key: string]: any })[childName] = {}
      }
    }

    ;(rawParameter as { [key: string]: unknown }).name = name
    const [editableParameter, error] = auditRawParameterToEditable(units)(
      strictAudit,
      rawParameter,
    ) as [Parameter, unknown]
    if (error !== null) {
      console.error(filePath)
      // console.error(JSON.stringify(editableParameter, null, 2))
      console.error(JSON.stringify(error, null, 2))
      exitCode = 1
      continue
    }
    if (
      editableParameter === undefined &&
      relativeSplitPath[relativeSplitPath.length - 1] !== "index.yaml"
    ) {
      console.error(filePath)
      console.error("Undefined parameter")
      exitCode = 1
      continue
    }

    if (!options.silent) {
      console.log("Validating editable parameter…")
    }
    const [validEditableParameter, validEditableError] = auditEditableParameter(
      units,
    )(strictAudit, editableParameter)
    if (validEditableError !== null) {
      console.error(filePath)
      console.error(JSON.stringify(validEditableParameter, null, 2))
      console.error(JSON.stringify(validEditableError, null, 2))
      exitCode = 1
      continue
    }
  }
  return exitCode
}

main()
  .then((exitCode) => {
    process.exit(exitCode)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(2)
  })
