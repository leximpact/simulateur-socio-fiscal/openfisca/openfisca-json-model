import {
  ParameterClass,
  walkParameters,
  type NodeParameter,
} from "@openfisca/json-model"
import { JorfArticle, LegiArticle } from "@tricoteuses/legal-explorer"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

interface ArticleData {
  id: string
  contexte: {
    nature?: string
    num?: string
    date_publi: string
    date_signature: string
  }
  num?: string
  type?: string
  date_debut: string
  date_fin: string
  texte?: string
}

interface ReferenceData {
  name: string
  description?: string
  short_label?: string
  value: {
    date: string
    unit?: string
    value: unknown
  }
  reference_url: string

  article_jorf?: ArticleData
  article_legi?: ArticleData
  articles_legi_modifies?: ArticleData[]
}

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const parametersFilePath = path.join(
    options.jsonDir,
    "editable_processed_parameters.json",
  )
  const parameters = (await fs.readJson(parametersFilePath)) as NodeParameter

  const referencesData: ReferenceData[] = []
  for (const parameter of walkParameters(parameters)) {
    if (parameter.class === ParameterClass.Value) {
      if (parameter.reference !== undefined) {
        for (const [date, references] of Object.entries(parameter.reference)) {
          const value = parameter.values[date]
          if (value !== undefined && value !== "expected") {
            for (const { href } of references) {
              if (href === undefined) {
                continue
              }
              const match = href.match(
                /^https:\/\/www\.legifrance\.gouv\.fr\/jorf\/article_jo\/(JORFARTI\d{12})\/?$/,
              )
              if (match !== null) {
                const jorfArticleId = match[1]

                const referenceData: ReferenceData = {
                  name: parameter.name!,
                  description: parameter.description,
                  short_label: parameter.short_label,
                  value: {
                    date: date,
                    unit: value.unit,
                    value: value.value,
                  },
                  reference_url: href,
                }
                referencesData.push(referenceData)

                const jorfResponse = await fetch(
                  `http://localhost:5173/api/articles/${jorfArticleId}`,
                )
                if (jorfResponse.status === 404) {
                  console.warn(
                    "################################################################################",
                  )
                  console.warn(
                    `Référence artticle non trouvée: ${jorfArticleId}`,
                  )
                  console.warn(JSON.stringify(referenceData, null, 2))
                  console.warn(
                    "################################################################################",
                  )
                  continue
                }
                assert(jorfResponse.ok)
                const jorfResult = await jorfResponse.json()
                const jorfArticle = jorfResult.article[
                  jorfArticleId
                ] as JorfArticle
                const jorfMetaArticle = jorfArticle.META.META_SPEC.META_ARTICLE
                const jorfContexteTexte = jorfArticle.CONTEXTE.TEXTE

                referenceData.article_jorf = {
                  id: jorfArticleId,
                  contexte: {
                    nature: jorfContexteTexte["@nature"],
                    num: jorfContexteTexte["@num"],
                    // TODO: Add title at date.
                    date_publi: jorfContexteTexte["@date_publi"],
                    date_signature: jorfContexteTexte["@date_signature"],
                  },
                  num: jorfMetaArticle.NUM,
                  type: jorfMetaArticle.TYPE,
                  date_debut: jorfMetaArticle.DATE_DEBUT,
                  date_fin: jorfMetaArticle.DATE_FIN,
                  texte: jorfArticle.BLOC_TEXTUEL?.CONTENU,
                }

                const jorfVersionsLegi = jorfArticle.VERSIONS.VERSION.filter(
                  (version) => version.LIEN_ART["@origine"] === "LEGI",
                ).sort((version1, version2) =>
                  // It is not sure that sort is needed.
                  version1.LIEN_ART["@debut"].localeCompare(
                    version2.LIEN_ART["@debut"],
                  ),
                )
                const jorfVersionLegi = jorfVersionsLegi.find(
                  (version) =>
                    version.LIEN_ART["@debut"] >=
                    jorfContexteTexte["@date_publi"],
                )
                if (jorfVersionLegi === undefined) {
                  // Article autonome non transcrit dans la base LEGI
                  continue
                }

                const legiArticleId = jorfVersionLegi.LIEN_ART["@id"]
                const legiResponse = await fetch(
                  `http://localhost:5173/api/articles/${legiArticleId}`,
                )
                assert(
                  legiResponse.ok,
                  `Error ${legiResponse.status} at ${legiResponse.url} (JORF ARTICLE ${jorfArticleId})`,
                )
                const legiResult = await legiResponse.json()
                const legiArticle = legiResult.article[
                  legiArticleId
                ] as LegiArticle
                const legiMetaArticle = legiArticle.META.META_SPEC.META_ARTICLE
                const legiContexteTexte = legiArticle.CONTEXTE.TEXTE

                referenceData.article_legi = {
                  id: legiArticleId,
                  contexte: {
                    nature: legiContexteTexte["@nature"],
                    num: legiContexteTexte["@num"],
                    // TODO: Add title at date.
                    date_publi: legiContexteTexte["@date_publi"] as string,
                    date_signature: legiContexteTexte[
                      "@date_signature"
                    ] as string,
                  },
                  num: legiMetaArticle.NUM,
                  type: legiMetaArticle.TYPE,
                  date_debut: legiMetaArticle.DATE_DEBUT,
                  date_fin: legiMetaArticle.DATE_FIN,
                  texte: legiArticle.BLOC_TEXTUEL?.CONTENU,
                }

                const legiArticleLiensModifie = legiArticle.LIENS?.LIEN.filter(
                  (lien) =>
                    lien["@typelien"] === "MODIFIE" &&
                    lien["@sens"] === "source" &&
                    lien["@id"]?.startsWith("LEGIARTI"), // TODO: Handle LEGISECT…
                )
                if (
                  legiArticleLiensModifie === undefined ||
                  legiArticleLiensModifie.length === 0
                ) {
                  // Article autonome
                  continue
                }

                const articlesLegiModifies = []
                for (const legiArticleLienModifie of legiArticleLiensModifie) {
                  const legiArticleLienModifieId = legiArticleLienModifie["@id"]
                  if (legiArticleLienModifieId === undefined) {
                    continue
                  }
                  const response = await fetch(
                    `http://localhost:5173/api/articles/${legiArticleLienModifieId}`,
                  )
                  assert(
                    response.ok,
                    `Error ${response.status} at ${response.url} (JORF ARTICLE ${jorfArticleId})`,
                  )
                  const result = await response.json()
                  const legiArticleModifie = result.article[
                    legiArticleLienModifieId
                  ] as LegiArticle
                  const legiMetaArticleModifie =
                    legiArticleModifie.META.META_SPEC.META_ARTICLE
                  const legiContexteTexteModifie =
                    legiArticleModifie.CONTEXTE.TEXTE
                  articlesLegiModifies.push({
                    id: legiArticleLienModifieId,
                    contexte: {
                      nature: legiContexteTexteModifie["@nature"],
                      num: legiContexteTexteModifie["@num"],
                      // TODO: Add title at date.
                      date_publi: legiContexteTexteModifie[
                        "@date_publi"
                      ] as string,
                      date_signature: legiContexteTexteModifie[
                        "@date_signature"
                      ] as string,
                    },
                    num: legiMetaArticleModifie.NUM,
                    type: legiMetaArticleModifie.TYPE,
                    date_debut: legiMetaArticleModifie.DATE_DEBUT,
                    date_fin: legiMetaArticleModifie.DATE_FIN,
                    texte: legiArticleModifie.BLOC_TEXTUEL?.CONTENU,
                  })
                }
                referenceData.articles_legi_modifies = articlesLegiModifies
              }
            }
          }
        }
      }
    }
  }
  console.log(JSON.stringify(referencesData, null, 2))
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
