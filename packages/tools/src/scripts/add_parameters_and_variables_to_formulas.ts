import type {
  EntityByKey,
  NodeParameter,
  Parameter,
  Variable,
} from "@openfisca/json-model"
import {
  extractFromFormulaAst,
  jsonReplacer,
  walkParameters,
} from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const entityByKey = (await fs.readJson(
    path.join(options.jsonDir, "extracted", "entities.json"),
  )) as EntityByKey

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const parametersFilePath = path.join(
      taxBenefitDir,
      "editable_processed_parameters.json",
    )
    const parameters = (await fs.readJson(parametersFilePath)) as NodeParameter

    const leafParametersName = new Set<string>()
    for (const parameter of walkParameters(parameters)) {
      leafParametersName.add(parameter.name!)
    }

    const variablesDir = path.join(taxBenefitDir, "variables")
    for (const variableFilename of await fs.readdir(variablesDir)) {
      if (variableFilename.startsWith(".")) {
        continue
      }
      if (!variableFilename.endsWith(".json")) {
        continue
      }
      if (!options.silent) {
        console.log(
          `Extracting parameters & variables from formulas of "${variableFilename}"…`,
        )
      }
      const variableFilePath = path.join(variablesDir, variableFilename)
      const variable = (await fs.readJson(variableFilePath)) as Variable
      const formulas = variable.formulas
      if (formulas === undefined) {
        continue
      }
      let variableChanged = false
      for (const formula of Object.values(formulas)) {
        if (formula === null) {
          continue
        }
        const { openFiscaParametersName, openFiscaVariablesName } =
          extractFromFormulaAst(formula.ast!, entityByKey, leafParametersName)
        const realOpenFiscaParametersName = [
          ...new Set(
            [...openFiscaParametersName].map((parameterName) => {
              let parameter: Parameter | undefined = parameters
              if (parameterName === "") {
                if (!options.silent) {
                  console.warn(
                    `Caution: Root parameter "" is referenced by variable "${variable.name}".`,
                  )
                }
              } else {
                const parameterIds: string[] = []
                for (const parameterId of parameterName.split(".")) {
                  parameter = (parameter as NodeParameter).children?.[
                    parameterId
                  ]
                  if (parameter === undefined) {
                    if (!options.silent) {
                      console.warn(
                        `Ignoring non-existing parameter "${parameterName}" referenced by variable "${variable.name}".`,
                      )
                    }
                    return parameterIds.join(".")
                  }
                  parameterIds.push(parameterId)
                }
              }
              return parameterName
            }),
          ),
        ].sort()
        if (realOpenFiscaParametersName.length > 0) {
          formula.parameters = realOpenFiscaParametersName
          variableChanged = true
        }
        if (openFiscaVariablesName.size > 0) {
          formula.variables = [...openFiscaVariablesName].sort()
          variableChanged = true
        }
      }
      if (variableChanged) {
        await fs.writeJson(variableFilePath, variable, {
          replacer: jsonReplacer,
          spaces: 2,
        })
      }
    }
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
