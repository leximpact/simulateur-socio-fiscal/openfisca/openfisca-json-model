import toml from "@ltd/j-toml"
import assert from "assert"
import { $, cd, fetch, fs, nothrow, path } from "zx"

interface Package {
  name: string
  peerDependencies?: { [name: string]: string }
  peerDependenciesMeta?: { [name: string]: { optional?: boolean } }
  version: string
}

interface PyProject {
  tool: {
    poetry: {
      dependencies: { [name: string]: string | PyProjectDependencyObject }
      extras: { [name: string]: string[] }
      name: string
      version: string
    }
  }
}

interface PyProjectDependencyObject {
  branch?: string
  git?: string
  optional?: boolean
  version?: string
}

export interface VersionObject {
  major: number
  minor: number
  patch: number
}

// Change current dir to repository dir
// (because this script is launched from "packages/tools").
process.chdir(path.join("..", ".."))
const rootDir = process.cwd()

const {
  CI_COMMIT_BRANCH,
  CI_COMMIT_TAG,
  CI_COMMIT_TITLE,
  CI_DEFAULT_BRANCH,
  CI_JOB_TOKEN,
  CI_PIPELINE_SOURCE,
  CI_PROJECT_NAME,
  CI_PROJECT_NAMESPACE,
  CI_SERVER_HOST,
  CI_SERVER_URL,
  /// Branch of the OpenFisca JSON repository to use
  JSON_BRANCH,
  NPM_EMAIL,
  NPM_TOKEN,
  /// Should the OpenFisca JSON package be updated
  /// (ie committed, pushed and published on npm)?
  PUBLISH_JSON,
  SSH_KNOWN_HOSTS,
  SSH_PRIVATE_KEY,
} = process.env
const extractorDir = path.join(rootDir, "packages", "extractor")
const extractorPoetryLockPath = path.join(extractorDir, "poetry.lock")
const extractorPyProjectTomlPath = path.join(extractorDir, "pyproject.toml")
const libDir = path.join(rootDir, "packages", "lib")
const libPackagePath = path.join(libDir, "package.json")
const rootPackagePath = "package.json"
const toolsDir = path.join(rootDir, "packages", "tools")
const toolsPackagePath = path.join(toolsDir, "package.json")

function checkVersionObject(
  {
    major: majorReference,
    minor: minorReference,
    patch: patchReference,
  }: VersionObject,
  versionObject: VersionObject | undefined,
): boolean {
  if (versionObject === undefined) {
    return true
  }
  const { major, minor, patch } = versionObject
  if (major < majorReference) {
    return true
  }
  if (major === majorReference) {
    if (minor < minorReference) {
      return true
    }
    if (minor === minorReference) {
      return patch <= patchReference || patch === patchReference + 1
    }
    return minor === minorReference + 1 && patch === 0
  }
  return major === majorReference + 1 && minor === 0 && patch === 0
}

async function commitAndPushOpenFiscaJson() {
  const libPackage = (await fs.readJson(libPackagePath)) as Package
  const libName = libPackage.name
  const libVersion = libPackage.version

  const jsonDir = path.join(
    rootDir,
    "..",
    "leximpact-socio-fiscal-openfisca-json",
  )
  cd(jsonDir)
  await $`git add .`
  if ((await $`git diff --quiet --staged`.exitCode) === 1) {
    // Content of json directory has changed.

    const jsonVersionObject = (await latestVersionObjectFromTags()) ?? {
      major: 0,
      minor: 0,
      patch: 0,
    }
    const jsonNextVersionObject = {
      ...jsonVersionObject!,
      patch: jsonVersionObject!.patch + 1,
    }
    const jsonNextVersion = versionFromObject(jsonNextVersionObject)

    const jsonPackagePath = path.join(jsonDir, "package.json")
    let jsonPackage = (await fs.readJson(jsonPackagePath)) as Package

    const metadata = await fs.readJson(path.join(jsonDir, "metadata.json"))
    const jsonDependenciesLine = [
      { name: libName, version: libVersion },
      ...(metadata.packages as { name: string; version: string }[]),
    ]
      .map(({ name, version }) => `${name}@${version}`)
      .join(", ")

    jsonPackage.version = jsonNextVersion
    let peerDependencies = jsonPackage.peerDependencies
    if (peerDependencies === undefined) {
      peerDependencies = jsonPackage.peerDependencies = {}
    }
    peerDependencies[libName] = `^${libVersion}`
    let peerDependenciesMeta = jsonPackage.peerDependenciesMeta
    if (peerDependenciesMeta === undefined) {
      peerDependenciesMeta = jsonPackage.peerDependenciesMeta = {}
    }
    peerDependenciesMeta[libName] = { optional: true }
    await fs.writeJson(jsonPackagePath, jsonPackage, { spaces: 2 })

    const message = `${jsonNextVersion} (${jsonDependenciesLine})`
    await $`git add .`
    if ((await $`git diff --quiet --staged`.exitCode) !== 0) {
      await $`git commit -m ${message}`
      await $`git push`
    }
    await $`git tag -a ${jsonNextVersion} -m ${message}`
    await $`git push --tags`
    // Note: Don't fail when there is nothing new to publish.
    if (NPM_EMAIL !== undefined && NPM_TOKEN !== undefined) {
      await nothrow($`npm publish`)
    }
  }
}

async function commitAndPushWithUpdatedVersions(
  extraDependenciesByName: {
    [name: string]: string[]
  },
  nextVersionObject?: VersionObject | undefined,
) {
  // Retrieve current versions of OpenFisca extra dependencies
  // (OpenFisca-France, etc).
  const extractorPoetryLockToml = await fs.readFile(extractorPoetryLockPath)
  const extractorPoetryLock = toml.parse(extractorPoetryLockToml, "\n")
  const extractorPackages = extractorPoetryLock.package as Package[]
  const versionByDependencyName: { [name: string]: string } = {}
  for (const extraDependenciesName of Object.values(extraDependenciesByName)) {
    for (const extraDependencyName of extraDependenciesName) {
      const dependencyPackage = extractorPackages.find(
        (pkg) => pkg.name === extraDependencyName,
      )
      versionByDependencyName[extraDependencyName] = dependencyPackage!.version
    }
  }
  const dependencyMessageArray = Object.entries(versionByDependencyName)
    .sort(([name1, name2]) => name1.localeCompare(name2))
    .map(([name, version]) => `${name}@${version}`)
    .reduce((accumulator: string[], message: string): string[] => {
      accumulator.push("-m")
      accumulator.push(message)
      return accumulator
    }, [])

  if (nextVersionObject === undefined) {
    // Retrieve next version of OpenFisca-JSON-Model.
    nextVersionObject = await latestVersionObjectFromTags()
    assert.notStrictEqual(nextVersionObject, undefined)
    nextVersionObject!.patch++
  }
  const nextVersion = versionFromObject(nextVersionObject!)

  // Update pyproject.toml with next version of project…
  let extractorPyProjectToml = await fs.readFile(
    extractorPyProjectTomlPath,
    "utf-8",
  )
  extractorPyProjectToml = extractorPyProjectToml.replace(
    /^version = "(.*?)"$/m,
    `version = "${nextVersion}"`,
  )
  // … and the latest versions of each of the OpenFisca packages.
  for (const extraDependenciesName of Object.values(extraDependenciesByName)) {
    for (const extraDependencyName of extraDependenciesName) {
      extractorPyProjectToml = extractorPyProjectToml.replace(
        new RegExp(
          `^${escapeRegExp(
            extraDependencyName,
          )} *= *(\{.*)version *= *".*?"(.*\})$`,
          "im",
        ),
        `${extraDependencyName} = $1version = ">= ${versionByDependencyName[extraDependencyName]}"$2`,
      )
    }
  }
  await fs.writeFile(
    extractorPyProjectTomlPath,
    extractorPyProjectToml,
    "utf-8",
  )

  for (const packageDir of [".", libDir, toolsDir]) {
    if ((await $`git diff --quiet --staged ${packageDir}`.exitCode) !== 0) {
      const packageJsonPath = path.join(packageDir, "package.json")
      let packageJson = await fs.readFile(packageJsonPath, "utf-8")
      packageJson = packageJson.replace(
        /^  "version": "(.*?)",$/m,
        `  "version": "${nextVersion}",`,
      )
      await fs.writeFile(packageJsonPath, packageJson, "utf-8")
    }
  }

  await $`git add .`
  if ((await $`git diff --quiet --staged`.exitCode) !== 0) {
    await $`git commit -m ${nextVersion} ${dependencyMessageArray}`
    await $`git push --set-upstream`
  }
  await $`git tag -a ${nextVersion} -m ${nextVersion} ${dependencyMessageArray}`
  await $`git push --set-upstream --tags`

  cd(libDir)
  // Note: Don't fail when there is nothing new to publish.
  if (NPM_EMAIL !== undefined && NPM_TOKEN !== undefined) {
    await nothrow($`npm publish`)
  }
  cd(rootDir)
}

async function configureGit() {
  console.log("Configuring git…")

  // Set the Git user name and email.
  await $`git config --global user.email "admin+openfisca-json-model-ci@tax-benefit.org"`
  await $`git config --global user.name "OpenFisca JSON Model CI"`

  console.log("Git configuration completed.")
}

async function configureNpm() {
  console.log("Configuring npm…")

  // Configure npm to be able to publish packages.
  if (NPM_EMAIL !== undefined && NPM_TOKEN !== undefined) {
    await $`echo ${`//registry.npmjs.org/:_authToken=${NPM_TOKEN}`} > ~/.npmrc`
    await $`echo ${`email=${NPM_EMAIL}`} >> ~/.npmrc`
  }

  console.log("Npm configuration completed.")
}

async function configureSsh() {
  console.log("Configuring ssh…")

  // Note: `eval $(ssh-agent -s)` must be done before calling this script because it
  // creates 2 environment variables (then used by ssh clients).
  // // Install ssh-agent if not already installed, to be able to use git with ssh.
  // if ((await $`which ssh-agent`.exitCode) !== 0) {
  //   await $`apt install -y openssh-client`
  // }
  // // Run ssh-agent (inside the build environment)
  // await $`eval $(ssh-agent -s)`

  // Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
  // Use `tr` to fix line endings which makes ed25519 keys work without
  // extra base64 encoding.
  // https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
  if (SSH_PRIVATE_KEY !== undefined) {
    await $`echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -`
  }

  if (SSH_KNOWN_HOSTS !== undefined) {
    // Create the SSH directory and give it the right permissions
    await $`mkdir -p ~/.ssh`
    await $`chmod 700 ~/.ssh`
    // Accept the SSH host keys of GitLab server.
    await $`echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts`
    await $`chmod 644 ~/.ssh/known_hosts`
  }

  console.log("Ssh configuration completed.")
}

/// RegExp escape function taken from
/// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
function escapeRegExp(s: string): string {
  return s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&") // $& means the whole matched string
}

async function extractOpenFiscaJson() {
  // Create or clean up Leximpact aggregates directory.
  const aggregatesDir = path.join(rootDir, "..", `leximpact-aggregates`)
  if (await fs.pathExists(aggregatesDir)) {
    await fs.remove(aggregatesDir)
  }
  // Since leximpact-aggregates is a private repository use (read-only) SSH access to retrieve it.
  // const aggregatesRepository = `https://${CI_SERVER_HOST}/leximpact/simulateur-socio-fiscal/leximpact-aggregates.git`
  const aggregatesRepository = `git@${CI_SERVER_HOST}:leximpact/simulateur-socio-fiscal/budget/leximpact-aggregates.git`
  cd(rootDir)
  await $`git clone ${aggregatesRepository} ${aggregatesDir}`
  cd(toolsDir)

  // Create or clean up OpenFisca JSON directory.
  const jsonDir = path.join(
    rootDir,
    "..",
    "leximpact-socio-fiscal-openfisca-json",
  )
  if (await fs.pathExists(jsonDir)) {
    await fs.remove(jsonDir)
  }
  const repository =
    CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH
      ? `git@${CI_SERVER_HOST}:leximpact/simulateur-socio-fiscal/leximpact-socio-fiscal-openfisca-json.git`
      : `https://${CI_SERVER_HOST}/leximpact/simulateur-socio-fiscal/leximpact-socio-fiscal-openfisca-json.git`
  cd(rootDir)
  await $`git clone ${
    JSON_BRANCH?.trim() ? ["--branch", JSON_BRANCH] : ""
  } ${repository} ${jsonDir}`
  cd(toolsDir)

  // Extract JSON from OpenFisca source code.
  const scriptRelativeDir = path.join(
    "build",
    "scripts",
    `extract_openfisca_json.js`,
  )
  await $`node ${scriptRelativeDir}`
}

async function latestVersionObjectFromTags(): Promise<
  VersionObject | undefined
> {
  // Retrieve all tags.
  await $`git fetch --all --tags`

  return (await $`git tag --list`).stdout
    .split("\n")
    .map(objectFromVersion)
    .filter((versionObject) => versionObject !== undefined)
    .sort((versionObject1, versionObject2) =>
      versionObject1!.major !== versionObject2!.major
        ? versionObject2!.major - versionObject1!.major
        : versionObject1!.minor !== versionObject2!.minor
          ? versionObject2!.minor - versionObject1!.minor
          : versionObject2!.patch - versionObject1!.patch,
    )[0]
}

async function main() {
  console.log("Starting gitlab-ci.ts…")

  await configureGit()
  await configureNpm()
  await configureSsh()

  console.log(`Handling "${CI_PIPELINE_SOURCE}" event…`)
  switch (CI_PIPELINE_SOURCE) {
    case "api":
    case "pipeline":
    case "schedule":
    case "trigger":
    case "web": {
      if (CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH) {
        // A pipeline has been triggered (for example when OpenFisca-France has been updated or a user as
        // launched a new pipeline manually or…).
        // => A new version of OpenFisca-JSON-Model must be created if and only if a dependency has
        // changed.

        console.log(
          `Handling trigger: JSON_BRANCH=${JSON_BRANCH} PUBLISH_JSON=${PUBLISH_JSON}…`,
        )

        await resetGitRepository()

        // Extract OpenFisca packages (eg OpenFisca-France, etc), that have
        // a script to extract their JSON data.
        const extractorPyProjectToml = await fs.readFile(
          extractorPyProjectTomlPath,
        )
        const extractorPyProject = toml.parse(
          extractorPyProjectToml,
          "\n",
        ) as unknown as PyProject
        const { poetry } = extractorPyProject.tool
        const extraDependenciesByName: { [name: string]: string[] } = {}
        for (const [extraName, extraDependenciesName] of Object.entries(
          poetry.extras,
        )) {
          extraDependenciesByName[extraName] = extraDependenciesName
        }

        // Extract npm name of `lib` package.
        const libPackage = (await fs.readJson(libPackagePath)) as Package
        const { name: libPackageName } = libPackage

        // Test OpenFisca-JSON-Model with the latest dependencies.
        cd(extractorDir)
        await $`poetry update`
        await $`poetry install --extras="${Object.keys(
          extraDependenciesByName,
        )}"`
        cd(libDir)
        await $`npm run build`
        await $`npm link`
        cd(toolsDir)
        await $`npm link ${libPackageName}`
        await $`npm run build:js`
        await extractOpenFiscaJson()
        cd(rootDir)

        if (PUBLISH_JSON?.trim()) {
          // Commit & push JSON data (if it has changed).
          await commitAndPushOpenFiscaJson()
          await triggerUiPipeline("france-with-indirect-taxation")
        }
      } else {
        console.log(
          `Unhandled event "${CI_PIPELINE_SOURCE}" in branch "${CI_COMMIT_BRANCH}".`,
        )
      }
      break
    }
    case "push": {
      if (CI_COMMIT_BRANCH !== undefined) {
        console.log(`Push to branch ${CI_COMMIT_BRANCH}`)

        if (CI_COMMIT_BRANCH.match(/^\d+_\d+_\d+$/) != null) {
          console.log(
            `Ignoring commit in version branch (for branch ${CI_COMMIT_BRANCH}).`,
          )
        } else if (CI_COMMIT_TITLE?.match(/^\d+\.\d+\.\d+( |$)/) != null) {
          console.log(
            `Ignoring version commit (for version ${CI_COMMIT_TITLE}).`,
          )
        } else {
          await resetGitRepository()

          // Extract OpenFisca packages (eg OpenFisca-France, etc), that have
          // a script to extract their JSON data.
          const extractorPyProjectToml = await fs.readFile(
            extractorPyProjectTomlPath,
          )
          const extractorPyProject = toml.parse(
            extractorPyProjectToml,
            "\n",
          ) as unknown as PyProject
          const { poetry } = extractorPyProject.tool
          const extraDependenciesByName: { [name: string]: string[] } = {}
          for (const [extraName, extraDependenciesName] of Object.entries(
            poetry.extras,
          )) {
            extraDependenciesByName[extraName] = extraDependenciesName
          }

          // Extract npm name of `lib` package.
          const libPackage = (await fs.readJson(libPackagePath)) as Package
          const { name: libPackageName } = libPackage

          // Retrieve current version of OpenFisca-JSON-Model.
          const tagVersionObject = (await latestVersionObjectFromTags()) ?? {
            major: 0,
            minor: 0,
            patch: 0,
          }
          const tagVersion = versionFromObject(tagVersionObject)
          let nextVersionObject = {
            ...tagVersionObject,
            patch: tagVersionObject!.patch + 1,
          }

          // Ensure that the version numbers of openfisca-json-model packages are
          // compatible with last version tag.
          const { version: extractorVersion } = poetry
          const extractorVersionObject = objectFromVersion(extractorVersion)
          assert(
            checkVersionObject(tagVersionObject, extractorVersionObject),
            `In ${extractorPyProjectTomlPath}, project version should be compatible with ${tagVersion}, got "${extractorVersion}" instead.`,
          )
          nextVersionObject = maxVersionObject(
            nextVersionObject,
            extractorVersionObject,
          )
          for (const packagePath of [
            rootPackagePath,
            libPackagePath,
            toolsPackagePath,
          ]) {
            const pkg = await fs.readJson(packagePath)
            const { version: packageVersion } = pkg
            const packageVersionObject = objectFromVersion(packageVersion)
            assert(
              checkVersionObject(tagVersionObject, packageVersionObject),
              `In ${packagePath}, project version should be compatible with ${tagVersion}, got "${packageVersion}" instead.`,
            )
            nextVersionObject = maxVersionObject(
              nextVersionObject,
              packageVersionObject,
            )
          }

          // Ensure that extras dependencies are optional.
          for (const extraDependenciesName of Object.values(
            extraDependenciesByName,
          )) {
            for (const extraDependencyName of extraDependenciesName) {
              const extraDependency = poetry.dependencies[extraDependencyName]
              assert.notStrictEqual(
                extraDependency,
                undefined,
                `Package ${extraDependencyName} is missing from ${extractorPyProjectTomlPath} dependencies.`,
              )
              assert.notStrictEqual(
                typeof extraDependency,
                "string",
                `In ${extractorPyProjectTomlPath}, ${extraDependencyName} dependency should not be a string but an object. Got ${JSON.stringify(
                  extraDependency,
                )}.`,
              )
              assert.strictEqual(
                (extraDependency as PyProjectDependencyObject).optional,
                true,
                `In ${extractorPyProjectTomlPath}, ${extraDependencyName} dependency should be optional, got ${JSON.stringify(
                  extraDependency,
                )} instead.`,
              )
              if (
                (extraDependency as PyProjectDependencyObject).version ===
                undefined
              ) {
                assert.notStrictEqual(
                  (extraDependency as PyProjectDependencyObject).git,
                  undefined,
                  `In ${extractorPyProjectTomlPath}, ${extraDependencyName} dependency is missing "git" or "version" attribute.`,
                )
              }
            }
          }

          // Test OpenFisca-JSON-Model with its current dependencies.
          cd(extractorDir)
          await $`poetry install --extras="${Object.keys(
            extraDependenciesByName,
          )}"`
          cd(libDir)
          await $`npm run build`
          await $`npm link`
          cd(toolsDir)
          await $`npm link ${libPackageName}`
          await $`npm run build`
          await extractOpenFiscaJson()
          cd(rootDir)

          // Test OpenFisca-JSON-Model with the latest dependencies.
          cd(extractorDir)
          await $`poetry update`
          await $`poetry install --extras="${Object.keys(
            extraDependenciesByName,
          )}"`
          cd(toolsDir)
          await extractOpenFiscaJson()
          cd(rootDir)

          if (CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH) {
            // A merge request has been merged into master (ie content of OpenFisca-JSON-Model has been changed).
            // => Create a new version of OpenFisca-JSON-Model.
            await commitAndPushWithUpdatedVersions(
              extraDependenciesByName,
              nextVersionObject,
            )
            await commitAndPushOpenFiscaJson()
            await triggerUiPipeline("france-with-indirect-taxation")
          }
        }
      } else if (CI_COMMIT_TAG !== undefined) {
        console.log(`Pushing commit tag "${CI_COMMIT_TAG}"…`)
      } else if (CI_COMMIT_TAG !== undefined) {
        console.log(`Unhandled push event.`)
      }
      break
    }
    default:
      console.log(
        `Unhandled event "${CI_PIPELINE_SOURCE}" in branch "${CI_COMMIT_BRANCH}".`,
      )
  }
}

function maxVersionObject(
  versionObject1: VersionObject,
  versionObject2: VersionObject | undefined,
): VersionObject {
  if (versionObject2 === undefined) {
    return versionObject1
  }
  const { major: major1, minor: minor1, patch: patch1 } = versionObject1
  const { major: major2, minor: minor2, patch: patch2 } = versionObject2
  if (major1 < major2) {
    return versionObject2
  }
  if (major1 > major2) {
    return versionObject1
  }
  if (minor1 < minor2) {
    return versionObject2
  }
  if (minor1 > minor2) {
    return versionObject1
  }
  if (patch1 < patch2) {
    return versionObject2
  }
  if (patch1 > patch2) {
    return versionObject1
  }
  // Both versions are the same. Return one of them.
  return versionObject1
}

function objectFromVersion(
  version: string | undefined,
): VersionObject | undefined {
  if (version === undefined) {
    return undefined
  }
  const match = version.match(/^(\d+)\.(\d+)\.(\d+)$/) as string[]
  if (match === null) {
    return undefined
  }
  return {
    major: parseInt(match[1]),
    minor: parseInt(match[2]),
    patch: parseInt(match[3]),
  }
}

async function resetGitRepository() {
  // Reset current repo, because it may have been tranformed by a previous CI that failed.
  await $`git reset --hard`
  await $`git switch ${CI_COMMIT_BRANCH}`
  await $`git fetch origin ${CI_COMMIT_BRANCH}`
  await $`git reset --hard origin/${CI_COMMIT_BRANCH}`
  await $`git status`
  if (CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH) {
    await $`git remote set-url origin git@${CI_SERVER_HOST}:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git`
  } else {
    await $`git remote set-url origin https://${CI_SERVER_HOST}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git`
  }
}

async function triggerUiPipeline(name: string) {
  const url = new URL(
    `/api/v4/projects/4/trigger/pipeline`,
    CI_SERVER_URL,
  ).toString()
  console.log("Triggering UI pipeline on master branch…")
  const body = {
    ref: "master",
    token: CI_JOB_TOKEN!,
    "variables[NAME]": name,
  } as { [key: string]: string }
  const response = await fetch(url, {
    body: new URLSearchParams(body).toString(),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    method: "POST",
  })
  assert(
    response.ok,
    `Unexpected response from ${url}: ${response.status} ${
      response.statusText
    }\n${await response.text()}`,
  )
}

function versionFromObject({ major, minor, patch }: VersionObject): string {
  return `${major}.${minor}.${patch}`
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
