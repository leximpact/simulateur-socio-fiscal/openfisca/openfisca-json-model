import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import type { CustomizationByName, Variable } from "@openfisca/json-model"
import { auditCustomizationByName, jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const customizationsFilePath = path.join(
    options.jsonDir,
    "custom",
    "customizations.json",
  )
  const [customizationByName, customizationByNameError] = auditChain(
    auditCustomizationByName,
    auditRequire,
  )(strictAudit, await fs.readJson(customizationsFilePath)) as [
    CustomizationByName,
    unknown,
  ]
  if (customizationByNameError !== null) {
    console.error(customizationsFilePath)
    // console.error(JSON.stringify(customizationByName, null, 2))
    console.error(JSON.stringify(customizationByNameError, null, 2))
    process.exit(1)
  }

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const linkedOutputVariablesNameByVariableName: {
      [name: string]: string[]
    } = {}
    const variablesDir = path.join(taxBenefitDir, "variables")

    for (const [name, customization] of Object.entries(customizationByName)) {
      if (customization.virtual) {
        // Variable doesn't exist in OpenFisca.
        continue
      }
      const variableFilePath = path.join(variablesDir, `${name}.json`)
      if (!(await fs.pathExists(variableFilePath))) {
        // Variable doesn't exist in this tax-benefit system or reform.
        continue
      }
      const variable = (await fs.readJson(variableFilePath)) as Variable

      if (customization.description !== undefined) {
        variable.description = customization.description
      }
      if (customization.input) {
        variable.input = true
      }
      if (customization.label !== undefined) {
        variable.label = customization.label
      }
      if (customization.last_value_still_valid_on !== undefined) {
        if (
          variable.last_value_still_valid_on !== undefined &&
          variable.last_value_still_valid_on >
            customization.last_value_still_valid_on
        ) {
          console.warn(
            `Attribute last_value_still_valid_on is more recent in variable ${name} (${variable.last_value_still_valid_on}) than in customization (${customization.last_value_still_valid_on})`,
          )
        } else {
          variable.last_value_still_valid_on =
            customization.last_value_still_valid_on
        }
      }
      if (customization.linked_added_variables !== undefined) {
        let linkedAddedVariablesName: string[] = []
        if (Array.isArray(customization.linked_added_variables)) {
          linkedAddedVariablesName = customization.linked_added_variables
        } else if (customization.linked_added_variables) {
          const latest_formula = Object.entries(variable.formulas ?? {}).sort(
            ([date1], [date2]) => date2.localeCompare(date1),
          )[0][1]
          if (latest_formula?.variables !== undefined) {
            linkedAddedVariablesName = latest_formula.variables
          }
        }
        if (linkedAddedVariablesName.length > 0) {
          variable.linked_added_variables = linkedAddedVariablesName
          // Also add reverse links.
          for (const linkedAddedVariableName of linkedAddedVariablesName) {
            linkedOutputVariablesNameByVariableName[linkedAddedVariableName] ??=
              []
            linkedOutputVariablesNameByVariableName[
              linkedAddedVariableName
            ].push(variable.name)
          }
        }
      }
      if (customization.linked_other_variables !== undefined) {
        let linkedOtherVariablesName: string[] = []
        if (Array.isArray(customization.linked_other_variables)) {
          linkedOtherVariablesName = customization.linked_other_variables
        } else if (customization.linked_other_variables) {
          const latest_formula = Object.entries(variable.formulas ?? {}).sort(
            ([date1], [date2]) => date2.localeCompare(date1),
          )[0][1]
          if (latest_formula?.variables !== undefined) {
            linkedOtherVariablesName = latest_formula.variables
          }
        }
        if (linkedOtherVariablesName.length > 0) {
          variable.linked_other_variables = linkedOtherVariablesName
          // Commented out because we don't want to add reverse links of "other" variables.
          // // Also add reverse links.
          // for (const linkedOtherVariableName of linkedOtherVariablesName) {
          //   linkedOutputVariablesNameByVariableName[linkedOtherVariableName] ??=
          //     []
          //   linkedOutputVariablesNameByVariableName[
          //     linkedOtherVariableName
          //   ].push(variable.name)
          // }
        }
      }
      if (customization.main_parameters !== undefined) {
        // In theory only the latest formula should be updated with customization.main_parameters.
        // but sometimes, the latest formula is for next year and the UI will use the formula before
        // const latest_formula = Object.entries(variable.formulas ?? {}).sort(
        //   ([date1], [date2]) => date2.localeCompare(date1),
        // )[0][1]
        // if (latest_formula !== null) {
        //   latest_formula.parameters = [
        //     ...new Set([
        //       ...customization.main_parameters,
        //       ...(latest_formula.parameters ?? []).sort(),
        //     ]),
        //   ]
        // }
        for (const formula of Object.values(variable.formulas ?? {})) {
          if (formula !== null) {
            formula.parameters = [
              ...new Set([
                ...customization.main_parameters,
                ...(formula.parameters ?? []).sort(),
              ]),
            ]
          }
        }
      }
      if (customization.obsolete) {
        variable.obsolete = true
      }
      if (customization.reference !== undefined) {
        variable.reference = customization.reference
      }
      if (customization.short_label !== undefined) {
        variable.short_label = customization.short_label
      }
      if (customization.variables !== undefined) {
        const latest_formula = Object.entries(variable.formulas ?? {}).sort(
          ([date1], [date2]) => date2.localeCompare(date1),
        )[0][1]
        if (latest_formula !== null) {
          latest_formula.variables = [
            ...new Set([
              ...(latest_formula.variables ?? []),
              ...customization.variables,
            ]),
          ].sort()
        }
      }

      await fs.writeJson(variableFilePath, variable, {
        replacer: jsonReplacer,
        spaces: 2,
      })
    }

    for (const variableFilename of await fs.readdir(variablesDir)) {
      if (variableFilename.startsWith(".")) {
        continue
      }
      if (!variableFilename.endsWith(".json")) {
        continue
      }

      let changed = false
      const variableFilePath = path.join(variablesDir, variableFilename)
      const variable = (await fs.readJson(variableFilePath)) as Variable

      if (
        variable.last_value_still_valid_on === undefined &&
        variable.formulas !== undefined
      ) {
        const last_date = Object.keys(variable.formulas).sort((date1, date2) =>
          date2.localeCompare(date1),
        )[0]
        if (last_date !== "0001-01-01") {
          variable.last_value_still_valid_on = last_date
          changed = true
        }
      }

      const linkedOutputVariablesName =
        linkedOutputVariablesNameByVariableName[variable.name]
      if (linkedOutputVariablesName !== undefined) {
        variable.linked_output_variables = linkedOutputVariablesName
        changed = true
      }

      if (changed) {
        await fs.writeJson(variableFilePath, variable, {
          replacer: jsonReplacer,
          spaces: 2,
        })
      }
    }
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
