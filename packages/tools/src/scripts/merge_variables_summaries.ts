import type { Variable } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const [taxBenefitDir, variableDirName] of [
    [options.jsonDir, "variables"],
    ...reformDirArray.map((reformDir) => [reformDir, "variables_changes"]),
  ]) {
    const variablesDir = path.join(taxBenefitDir, variableDirName)
    const variableSummaryByName: { [name: string]: Variable } = {}
    if (await fs.pathExists(variablesDir)) {
      for (const variableFilename of await fs.readdir(variablesDir)) {
        if (variableFilename.startsWith(".")) {
          continue
        }
        if (!variableFilename.endsWith(".json")) {
          continue
        }
        const variable = (await fs.readJson(
          path.join(variablesDir, variableFilename),
        )) as Variable
        variableSummaryByName[variable.name] = variable
        const formulas = variable.formulas
        if (formulas === undefined) {
          continue
        }
        for (const formula of Object.values(formulas)) {
          if (formula === null) {
            continue
          }
          delete formula.ast
          delete formula.source_code
        }
      }
    }
    await fs.writeJson(
      path.join(taxBenefitDir, `${variableDirName}_summaries.json`),
      variableSummaryByName,
      { replacer: jsonReplacer, spaces: 2 },
    )
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
