import type { Metadata } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const metadataFilePath = path.join(options.jsonDir, "metadata.json")
  const metadata = (await fs.readJson(metadataFilePath)) as Metadata
  for (const packageMetadata of metadata.packages) {
    delete packageMetadata.dir
  }
  await fs.writeJson(metadataFilePath, metadata, {
    replacer: jsonReplacer,
    spaces: 2,
  })
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
