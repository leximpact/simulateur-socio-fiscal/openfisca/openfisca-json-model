import type {
  EntityByKey,
  NodeParameter,
  Variable,
} from "@openfisca/json-model"
import { extractFromFormulaAst, walkParameters } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "V",
    help: "name of variable to extract decomposition from",
    name: "variable",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const entityByKey = (await fs.readJson(
    path.join(options.jsonDir, "extracted", "entities.json"),
  )) as EntityByKey

  const parametersFilePath = path.join(
    options.jsonDir,
    "editable_processed_parameters.json",
  )
  const parameters = (await fs.readJson(parametersFilePath)) as NodeParameter
  const leafParametersName = new Set<string>()
  for (const parameter of walkParameters(parameters)) {
    leafParametersName.add(parameter.name!)
  }

  const variablesDir = path.join(options.jsonDir, "variables")
  const variableFilename = options.variable + ".json"
  const variable = (await fs.readJson(
    path.join(variablesDir, variableFilename),
  )) as Variable
  if (!options.silent) {
    console.log(`Extracting decomposition from variable ${variable.name}…`)
  }
  const formulas = variable.formulas
  if (formulas !== undefined) {
    const formula = Object.entries(formulas)
      .sort(([date1, _formula1], [date2, _formula2]) =>
        date2.localeCompare(date1),
      )
      .map(([_date, formula]) => formula)[0]
    if (formula !== null) {
      console.log(
        JSON.stringify(
          extractFromFormulaAst(formula.ast!, entityByKey, leafParametersName),
          null,
          2,
        ),
      )
    }
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
