import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import type {
  CustomizationByName,
  DecompositionReference,
  EntityByKey,
  NodeParameter,
  Variable,
} from "@openfisca/json-model"
import {
  auditCustomizationByName,
  extractFromFormulaAst,
  jsonReplacer,
  walkParameters,
} from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "c",
    help: "use children specified in customizations of decompositions",
    name: "customizations",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  let customizationByName: CustomizationByName
  if (options.customizations) {
    const customizationsFilePath = path.join(
      options.jsonDir,
      "custom",
      "customizations.json",
    )
    const [validCustomizationByName, customizationByNameError] = auditChain(
      auditCustomizationByName,
      auditRequire,
    )(strictAudit, await fs.readJson(customizationsFilePath)) as [
      CustomizationByName,
      unknown,
    ]
    if (customizationByNameError !== null) {
      console.error(customizationsFilePath)
      console.error(JSON.stringify(validCustomizationByName, null, 2))
      console.error(JSON.stringify(customizationByNameError, null, 2))
      process.exit(1)
    }
    customizationByName = validCustomizationByName
  } else {
    customizationByName = {}
  }

  const entityByKey = (await fs.readJson(
    path.join(options.jsonDir, "extracted", "entities.json"),
  )) as EntityByKey

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const parametersFilePath = path.join(
      taxBenefitDir,
      "editable_processed_parameters.json",
    )
    const parameters = (await fs.readJson(parametersFilePath)) as NodeParameter
    const leafParametersName = new Set<string>()
    for (const parameter of walkParameters(parameters)) {
      leafParametersName.add(parameter.name!)
    }

    const neededVariablesName = new Set<string>()
    for (const [name, customization] of Object.entries(customizationByName)) {
      neededVariablesName.add(name)
      for (const { name: childName } of customization.children ?? []) {
        neededVariablesName.add(childName)
      }
      for (const options of customization.options ?? []) {
        for (const { name: childName } of (
          options.then as { children: DecompositionReference[] }
        )?.children ?? []) {
          neededVariablesName.add(childName)
        }
        for (const { name: childName } of (
          options.else as { children: DecompositionReference[] }
        )?.children ?? []) {
          neededVariablesName.add(childName)
        }
      }
    }

    const decompositionByName: {
      [variableName: string]: {
        children?: DecompositionReference[]
        hidden?: boolean
        label: string
        last_value_still_valid_on?: string
      }
    } = {}
    const variablesDir = path.join(taxBenefitDir, "variables")
    for (const variableFilename of await fs.readdir(variablesDir)) {
      if (variableFilename.startsWith(".")) {
        continue
      }
      if (!variableFilename.endsWith(".json")) {
        continue
      }
      const variable = (await fs.readJson(
        path.join(variablesDir, variableFilename),
      )) as Variable
      if (!options.silent) {
        console.log(`Extracting decomposition from variable ${variable.name}…`)
      }
      const customization = customizationByName[variable.name]
      const childrenReferenceCustomization = customization?.children
      let childrenReference = undefined
      let hidden: boolean | undefined = undefined
      if (childrenReferenceCustomization === undefined) {
        const formulas = variable.formulas
        if (formulas !== undefined) {
          const formula = Object.entries(formulas)
            .sort(([date1, _formula1], [date2, _formula2]) =>
              date2.localeCompare(date1),
            )
            .map(([_date, formula]) => formula)[0]
          if (formula === null) {
            // Formula is no more active => hide it from decompositions.
            hidden = true
          } else {
            childrenReference = extractFromFormulaAst(
              formula.ast!,
              entityByKey,
              leafParametersName,
            ).decomposition
          }
        }
      } else if (childrenReferenceCustomization !== null) {
        childrenReference = childrenReferenceCustomization
      }
      if (childrenReference !== undefined) {
        for (const childReference of childrenReference) {
          neededVariablesName.add(childReference.name)
        }
      }
      decompositionByName[variable.name] = {
        children: childrenReference,
        hidden,
        label: variable.label ?? variable.name,
        last_value_still_valid_on: variable.last_value_still_valid_on,
      }
    }

    // Remove variables that are not used by decompositions.
    for (const [variableName, decomposition] of Object.entries(
      decompositionByName,
    )) {
      if (
        decomposition.children === undefined &&
        !neededVariablesName.has(variableName)
      ) {
        delete decompositionByName[variableName]
      }
    }

    const extractedDir = path.join(taxBenefitDir, "extracted")
    await fs.ensureDir(extractedDir)
    await fs.writeJson(
      path.join(
        extractedDir,
        options.customizations
          ? "decompositions_extracted_customized.json"
          : "decompositions_extracted.json",
      ),
      decompositionByName,
      { replacer: jsonReplacer, spaces: 2 },
    )
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
