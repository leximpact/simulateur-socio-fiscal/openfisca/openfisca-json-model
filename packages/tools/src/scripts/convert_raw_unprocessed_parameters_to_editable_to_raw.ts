import { strictAudit } from "@auditors/core"
import {
  auditEditableParameter,
  auditRawParameterToEditable,
  auditUnits,
  convertEditableParameterToRaw,
  rawParameterFromYaml,
  unitsFromYaml,
  yamlFromRawParameter,
  type Metadata,
  type Parameter,
  type Unit,
} from "@openfisca/json-model"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { walkDir } from "../file_systems"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main(): Promise<number> {
  const metadata = (await fs.readJson(
    path.join(options.jsonDir, "metadata.json"),
  )) as Metadata
  const taxBenefitPackageMetadata = metadata.packages.filter(
    (packageMetadata) => packageMetadata.tax_benefit_system,
  )[0]

  const [units, unitsError] = auditUnits(
    strictAudit,
    unitsFromYaml(
      await fs.readFile(path.join(options.jsonDir, "units.yaml"), "utf-8"),
    ),
  ) as [Unit[], unknown]
  if (unitsError !== null) {
    console.log(`An error occurred when validating units metadata:`)
    console.error(JSON.stringify(unitsError, null, 2))
    return 1
  }

  const rawParametersDir = path.join(
    options.jsonDir,
    "raw_unprocessed_parameters",
  )
  if (await fs.pathExists(rawParametersDir)) {
    await fs.remove(rawParametersDir)
  }
  await fs.ensureDir(rawParametersDir)

  const parametersDir = path.join(taxBenefitPackageMetadata.dir!, "parameters")
  if (!(await fs.pathExists(parametersDir))) {
    if (!options.silent) {
      console.info("OpenFisca package has no parameters directory.")
    }
    return 0
  }

  let exitCode = 0
  for (const relativeSplitPath of walkDir(parametersDir)) {
    const filePath = path.join(parametersDir, ...relativeSplitPath)
    if (!filePath.endsWith(".yaml")) {
      continue
    }

    if (!options.silent) {
      console.log(`Converting raw parameter ${filePath} to editable…`)
    }
    let rawParameter = rawParameterFromYaml(
      await fs.readFile(filePath, "utf-8"),
    )

    let dummyChildrenName = new Set<string>()
    if (path.basename(filePath) === "index.yaml") {
      // Add empty children to ParameterNode (needed to validate `order` attribute).
      if (rawParameter == null) {
        rawParameter = {}
      }
      for (const childFilename of await fs.readdir(path.dirname(filePath))) {
        if (childFilename.startsWith(".") || childFilename === "index.yaml") {
          continue
        }
        const childName = childFilename.replace(/\.yaml$/, "")
        assert.strictEqual(
          (rawParameter as { [key: string]: undefined })[childName],
          undefined,
        )
        ;(rawParameter as { [key: string]: any })[childName] = {}
        dummyChildrenName.add(childName)
      }
    }

    const name = relativeSplitPath
      .join(".")
      .replace(/\.yaml$/, "")
      .replace(/\.index$/, "")
    ;(rawParameter as { [key: string]: unknown }).name = name
    const [editableParameter, error] = auditRawParameterToEditable(units)(
      strictAudit,
      rawParameter,
    ) as [Parameter, unknown]
    if (error !== null) {
      console.log(
        `An error occurred when converting raw parameter ${filePath} to editable:`,
      )
      console.error(JSON.stringify(editableParameter, null, 2))
      console.error(JSON.stringify(error, null, 2))
      exitCode = 1
      continue
    }
    if (editableParameter === undefined) {
      if (relativeSplitPath[relativeSplitPath.length - 1] !== "index.yaml") {
        console.error(`Undefined parameter at ${filePath}`)
        exitCode = 1
      }
      continue
    }

    if (!options.silent) {
      console.log("Validating editable parameter…")
    }
    const [validEditableParameter, validEditableError] = auditEditableParameter(
      units,
    )(strictAudit, editableParameter) as [Parameter, unknown]
    if (validEditableError !== null) {
      console.log(
        `An error occurred when validating editable parameter ${filePath}:`,
      )
      console.error(JSON.stringify(validEditableParameter, null, 2))
      console.error(JSON.stringify(validEditableError, null, 2))
      exitCode = 1
      continue
    }

    if (!options.silent) {
      console.log("Converting editable parameter to raw…")
    }
    rawParameter = convertEditableParameterToRaw(validEditableParameter)
    delete (rawParameter as { [key: string]: unknown }).name
    for (const childName of dummyChildrenName) {
      delete (rawParameter as { [key: string]: unknown })[childName]
    }

    const rawParameterDir = path.join(
      rawParametersDir,
      ...relativeSplitPath.slice(0, -1),
    )
    await fs.ensureDir(rawParameterDir)
    await fs.writeFile(
      path.join(rawParametersDir, ...relativeSplitPath),
      yamlFromRawParameter(rawParameter),
    )
  }
  return exitCode
}

main()
  .then((exitCode) => {
    process.exit(exitCode)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(2)
  })
