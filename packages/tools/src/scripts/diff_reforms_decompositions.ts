import type { Decomposition, DecompositionByName } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import deepEqual from "fast-deep-equal"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const taxBenefitDecompositionsFilePath = path.join(
    options.jsonDir,
    "decompositions.json",
  )
  const taxBenefitDecompositionsByName = (await fs.readJson(
    taxBenefitDecompositionsFilePath,
  )) as DecompositionByName

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const reformDir of reformDirArray) {
    const changedDecompositionByName: { [name: string]: Decomposition | null } =
      {}
    const reformDecompositionsFilePath = path.join(
      reformDir,
      "decompositions.json",
    )
    const reformDecompositionByName = (await fs.readJson(
      reformDecompositionsFilePath,
    )) as DecompositionByName
    const remainingTaxBenefitDecompositionsName = new Set(
      Object.keys(taxBenefitDecompositionsByName),
    )
    for (const [name, reformDecomposition] of Object.entries(
      reformDecompositionByName,
    )) {
      if (
        !remainingTaxBenefitDecompositionsName.delete(name) ||
        !deepEqual(reformDecomposition, taxBenefitDecompositionsByName[name])
      ) {
        changedDecompositionByName[name] = reformDecomposition
      }
    }

    for (const name of remainingTaxBenefitDecompositionsName) {
      changedDecompositionByName[name] = null
    }

    await fs.writeJson(
      path.join(reformDir, "decompositions_changes.json"),
      changedDecompositionByName,
      { replacer: jsonReplacer, spaces: 2 },
    )
    await fs.remove(reformDecompositionsFilePath)
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
