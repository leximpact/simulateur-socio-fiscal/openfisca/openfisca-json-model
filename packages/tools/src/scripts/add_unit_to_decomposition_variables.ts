import type { DecompositionByName, Variable } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const decompositionsFilePath = path.join(
      taxBenefitDir,
      "decompositions.json",
    )
    const decompositionByName = (await fs.readJson(
      decompositionsFilePath,
    )) as DecompositionByName

    // Also add decomposition variables automatically extracted
    // from OpenFisca, that were not kept in decompositions.json.
    const decompositionsExtractedFilePath = path.join(
      taxBenefitDir,
      "extracted",
      "decompositions_extracted.json",
    )
    const decompositionExtractedByName = (await fs.readJson(
      decompositionsExtractedFilePath,
    )) as DecompositionByName

    const decompositionVariablesName = new Set([
      ...Object.keys(decompositionByName),
      ...Object.keys(decompositionExtractedByName),
    ])
    const variablesDir = path.join(taxBenefitDir, "variables")
    for (const variableName of decompositionVariablesName) {
      const variableFilePath = path.join(variablesDir, `${variableName}.json`)
      if (!(await fs.pathExists(variableFilePath))) {
        // Ignore variables from virtual decompositions.
        continue
      }
      const variable = (await fs.readJson(variableFilePath)) as Variable
      if (variable.possible_values === undefined) {
        variable.unit = "currency"
      }
      await fs.writeJson(variableFilePath, variable, {
        replacer: jsonReplacer,
        spaces: 2,
      })
    }
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
