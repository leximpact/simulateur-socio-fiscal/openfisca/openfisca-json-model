import type { Parameter } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import deepEqual from "fast-deep-equal"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

function diffParameters(
  source: Parameter,
  target: Parameter,
): { [key: string]: unknown } {
  const changes: { [key: string]: unknown } = {}
  const remainingSourceKeys = new Set(Object.keys(source))
  for (const [key, targetValue] of Object.entries(target)) {
    if (!remainingSourceKeys.delete(key)) {
      changes[key] = targetValue
    } else {
      const sourceValue = (source as unknown as { [key: string]: unknown })[key]
      if (key === "children") {
        const childrenChanges = diffParametersChildren(
          sourceValue as { [id: string]: Parameter },
          targetValue as { [id: string]: Parameter },
        )
        if (Object.keys(childrenChanges).length > 0) {
          changes[key] = childrenChanges
        }
      } else if (!deepEqual(sourceValue, targetValue)) {
        changes[key] = targetValue
      }
    }
  }
  for (const key of remainingSourceKeys) {
    changes[key] = null
  }
  return changes
}

function diffParametersChildren(
  source: { [id: string]: Parameter },
  target: { [id: string]: Parameter },
): { [id: string]: unknown } {
  const changes: { [id: string]: unknown } = {}
  const remainingSourceIds = new Set(Object.keys(source))
  for (const [id, targetChild] of Object.entries(target)) {
    if (!remainingSourceIds.delete(id)) {
      changes[id] = targetChild
    } else {
      const sourceChild = source[id]
      const childChanges = diffParameters(sourceChild, targetChild)
      if (Object.keys(childChanges).length > 0) {
        changes[id] = childChanges
      }
    }
  }
  for (const id of remainingSourceIds) {
    changes[id] = null
  }
  return changes
}

async function main() {
  const parametersFileName = "editable_processed_parameters.json"
  const taxBenefitRootParameter = (await fs.readJson(
    path.join(options.jsonDir, parametersFileName),
  )) as Parameter

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const reformDir of reformDirArray) {
    const reformParametersFilePath = path.join(reformDir, parametersFileName)
    const reformRootParameter = (await fs.readJson(
      reformParametersFilePath,
    )) as Parameter
    const changedRootParameter = diffParameters(
      taxBenefitRootParameter,
      reformRootParameter,
    )

    await fs.writeJson(
      path.join(reformDir, "editable_processed_parameters_changes.json"),
      changedRootParameter,
      { replacer: jsonReplacer, spaces: 2 },
    )
    await fs.remove(reformParametersFilePath)
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
