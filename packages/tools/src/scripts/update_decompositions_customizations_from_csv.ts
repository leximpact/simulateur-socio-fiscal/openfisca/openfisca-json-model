import type { Audit } from "@auditors/core"
import type { Customization } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import {
  auditCleanArray,
  auditDateIso8601String,
  auditFunction,
  auditHttpUrl,
  auditTrimString,
  cleanAudit,
} from "@auditors/core"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"

const optionsDefinitions = [
  {
    alias: "c",
    help: "path of CSV file containing customizations updates",
    name: "csv",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

export function auditContinuation(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["instant", "href", "name", "note", "title"]) {
    audit.attribute(data, key, true, errors, remainingKeys)
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditEntry(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["label", "name", "note", "title", "short_label"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  for (const key of ["hidden", "input", "obsolete"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditFunction((value) => value || undefined),
    )
  }
  audit.attribute(
    data,
    "instant",
    true,
    errors,
    remainingKeys,
    auditDateIso8601String,
  )
  audit.attribute(data, "href", true, errors, remainingKeys, auditHttpUrl)

  return audit.reduceRemaining(data, errors, remainingKeys)
}

async function main() {
  const customizationByName = (await fs.readJson(
    path.join(options.jsonDir, "custom", "customizations.json"),
  )) as { [name: string]: Partial<Customization> }

  const csv = await fs.readFile(options.csv, "utf-8")
  const { /* metadata, */ data } = Papa.parse(csv, { header: true })
  const [validData, errors] = auditCleanArray(auditEntry)(cleanAudit, data)
  if (errors !== null) {
    console.error(errors)
    process.exit(1)
  }
  let previousName: string | undefined = undefined
  for (const entry of validData) {
    const { name } = entry
    if (name === null) {
      console.log("Skipping blank line...")
      continue
    }
    let customization: Partial<Customization> | undefined =
      customizationByName[name]
    if (customization === undefined) {
      customization = customizationByName[name] = {}
    }

    if (name === previousName) {
      const [continuation, continuationErrors] = auditContinuation(
        cleanAudit,
        entry,
      ) as [
        {
          instant?: string
          note?: string
          title?: string
          href?: string
        },
        unknown,
      ]
      if (continuationErrors !== null) {
        console.error("Errors in continuation line:", continuationErrors, entry)
        process.exit(1)
      }
      const reference = Object.fromEntries(
        ["note", "href", "title"]
          .map((key) => [key, (continuation as { [key: string]: string })[key]])
          .filter(([_key, value]) => value !== undefined),
      )
      if (Object.keys(reference).length > 0) {
        let referencesByInstant = customization.reference
        if (referencesByInstant === undefined) {
          referencesByInstant = customization.reference = {}
        }
        const instant = continuation.instant ?? "0001-01-01"
        let references = referencesByInstant[instant]
        if (references === undefined) {
          references = referencesByInstant[instant] = []
        }
        references.push(reference)
      }
    } else {
      const { hidden, input, label, obsolete, short_label } = entry
      if (hidden) {
        customization.hidden = true
      } else {
        delete customization.hidden
      }
      if (input) {
        customization.input = true
      } else {
        delete customization.input
      }
      if (label !== undefined) {
        customization.label = label
      }
      if (obsolete) {
        customization.obsolete = true
      } else {
        delete customization.obsolete
      }
      if (short_label !== undefined) {
        customization.short_label = short_label
      }

      const reference = Object.fromEntries(
        ["note", "href", "title"]
          .map((key) => [key, (entry as { [key: string]: string })[key]])
          .filter(([_key, value]) => value !== undefined),
      )
      if (Object.keys(reference).length > 0) {
        const instant = entry.instant ?? "0001-01-01"
        customization.reference = { [instant]: [reference] }
      }
    }
    previousName = name
  }

  await fs.writeJson(
    path.join(options.jsonDir, "custom/customizations.json"),
    customizationByName,
    { replacer: jsonReplacer, spaces: 2 },
  )
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
