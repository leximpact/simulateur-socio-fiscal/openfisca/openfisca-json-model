import type { Parameter, NodeParameter, Variable } from "@openfisca/json-model"
import { jsonReplacer } from "@openfisca/json-model"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const parametersFilePath = path.join(
      taxBenefitDir,
      "editable_processed_parameters.json",
    )
    const parameters = (await fs.readJson(parametersFilePath)) as NodeParameter

    const variableByName: { [name: string]: Variable } = {}
    const variablesDir = path.join(taxBenefitDir, "variables")
    for (const variableFilename of await fs.readdir(variablesDir)) {
      if (variableFilename.startsWith(".")) {
        continue
      }
      if (!variableFilename.endsWith(".json")) {
        continue
      }
      const variable = (await fs.readJson(
        path.join(variablesDir, variableFilename),
      )) as Variable
      variableByName[variable.name] = variable
    }

    for (const [variableName, variable] of Object.entries(variableByName)) {
      const formulas = variable.formulas
      if (formulas === undefined) {
        continue
      }
      if (!options.silent) {
        console.log(`Adding references to variable "${variable.name}"…`)
      }
      for (const formula of Object.values(formulas)) {
        if (formula === null) {
          continue
        }

        const referredParametersName = formula.parameters
        if (referredParametersName !== undefined) {
          for (const referredParameterName of referredParametersName) {
            let referredParameter: Parameter = parameters
            if (referredParameterName === "") {
              if (!options.silent) {
                console.warn(
                  `  Caution: Root parameter "" is referenced by variable "${variable.name}"…`,
                )
              }
            } else {
              for (const referredParameterId of referredParameterName.split(
                ".",
              )) {
                referredParameter = (referredParameter as NodeParameter)
                  .children![referredParameterId]
              }
            }
            assert.notStrictEqual(
              referredParameter,
              undefined,
              `Non existing referred parameter: ${referredParameterName}`,
            )
            const referringVariablesName = referredParameter.referring_variables
            if (referringVariablesName === undefined) {
              referredParameter.referring_variables = [variableName]
            } else if (!referringVariablesName.includes(variableName)) {
              referringVariablesName.push(variableName)
              referringVariablesName.sort()
            }
          }
        }

        const referredVariablesName = formula.variables
        if (referredVariablesName !== undefined) {
          for (const referredVariableName of referredVariablesName) {
            const referredVariable = variableByName[referredVariableName]
            assert.notStrictEqual(
              referredVariable,
              undefined,
              `Non existing referred variable: ${referredVariableName}`,
            )
            const referringVariablesName = referredVariable.referring_variables
            if (referringVariablesName === undefined) {
              referredVariable.referring_variables = [variableName]
            } else if (!referringVariablesName.includes(variableName)) {
              referringVariablesName.push(variableName)
              referringVariablesName.sort()
            }
          }
        }
      }
    }

    await fs.writeJson(parametersFilePath, parameters, {
      replacer: jsonReplacer,
      spaces: 2,
    })

    for (const variable of Object.values(variableByName)) {
      await fs.writeJson(
        path.join(variablesDir, `${variable.name}.json`),
        variable,
        { replacer: jsonReplacer, spaces: 2 },
      )
    }
  }
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
