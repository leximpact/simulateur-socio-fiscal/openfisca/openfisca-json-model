import { jsonReplacer } from "@openfisca/json-model"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const reformsChanges: { [name: string]: unknown } = {}
  const reformsDir = path.join(options.jsonDir, "reforms")
  if (await fs.pathExists(reformsDir)) {
    for (const reformDirName of (await fs.readdir(reformsDir)).sort()) {
      if (reformDirName.startsWith(".")) {
        continue
      }
      const reformDir = path.join(reformsDir, reformDirName)
      reformsChanges[reformDirName] = {
        decompositions: await fs.readJson(
          path.join(reformDir, "decompositions_changes.json"),
        ),
        editable_processed_parameters: await fs.readJson(
          path.join(reformDir, "editable_processed_parameters_changes.json"),
        ),
        variables_summaries: await fs.readJson(
          path.join(reformDir, "variables_changes_summaries.json"),
        ),
      }
    }
  }
  await fs.writeJson(
    path.join(options.jsonDir, `reforms_changes.json`),
    reformsChanges,
    {
      replacer: jsonReplacer,
      spaces: 2,
    },
  )
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
