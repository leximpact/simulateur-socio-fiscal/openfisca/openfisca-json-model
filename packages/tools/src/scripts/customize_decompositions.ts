import {
  auditChain,
  auditCleanArray,
  auditRequire,
  strictAudit,
} from "@auditors/core"
import type {
  CustomizationByName,
  Decomposition,
  DecompositionByName,
  Waterfall,
} from "@openfisca/json-model"
import {
  auditCustomizationByName,
  auditDecompositionByName,
  auditWaterfall,
  jsonReplacer,
} from "@openfisca/json-model"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing the JSON files of an OpenFisca (country) package",
    name: "jsonDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const customizationsFilePath = path.join(
    options.jsonDir,
    "custom",
    "customizations.json",
  )
  const [customizationByName, customizationByNameError] = auditChain(
    auditCustomizationByName,
    auditRequire,
  )(strictAudit, await fs.readJson(customizationsFilePath)) as [
    CustomizationByName,
    unknown,
  ]
  if (customizationByNameError !== null) {
    console.error(customizationsFilePath)
    console.error(JSON.stringify(customizationByName, null, 2))
    console.error(JSON.stringify(customizationByNameError, null, 2))
    process.exit(1)
  }

  const waterfallsFilePath = path.join(options.jsonDir, "waterfalls.json")
  const [waterfalls, waterfallsError] = auditChain(
    auditCleanArray(auditWaterfall),
    auditRequire,
  )(strictAudit, await fs.readJson(waterfallsFilePath)) as [
    Waterfall[],
    unknown,
  ]
  if (waterfallsError !== null) {
    console.error(waterfallsFilePath)
    console.error(JSON.stringify(waterfalls, null, 2))
    console.error(JSON.stringify(waterfallsError, null, 2))
    process.exit(1)
  }

  const reformsDir = path.join(options.jsonDir, "reforms")
  const reformDirArray = (await fs.pathExists(reformsDir))
    ? (await fs.readdir(reformsDir)).map((reformDirName) =>
        path.join(reformsDir, reformDirName),
      )
    : []
  for (const taxBenefitDir of [options.jsonDir, ...reformDirArray]) {
    const decompositionsFilePath = path.join(
      taxBenefitDir,
      "extracted",
      "decompositions_extracted_customized.json",
    )
    const decompositionByName = (await fs.readJson(
      decompositionsFilePath,
    )) as DecompositionByName

    const rootDecompositionsName = waterfalls.map(({ root }) => root)
    const encounteredNames = new Set(rootDecompositionsName)
    const mergedDecompositionByName: DecompositionByName = {}
    const remainingNames = new Set(rootDecompositionsName)
    while (remainingNames.size > 0) {
      const name = remainingNames.values().next().value as string
      remainingNames.delete(name)
      const decomposition = decompositionByName[name] ?? {}
      mergedDecompositionByName[name] = decomposition
      const customization = customizationByName[name] ?? {}
      if (customization.children === null) {
        delete decomposition.children
      } else if (customization.children !== undefined) {
        decomposition.children = customization.children
      }
      if (customization.description !== undefined && customization.virtual) {
        decomposition.description = customization.description
      }
      if (customization.hidden) {
        decomposition.hidden = customization.hidden
      } else if (customization.hidden === false) {
        // Customization explicitly requests to unhide decomposition.
        delete decomposition.hidden
      }
      if (customization.label !== undefined) {
        decomposition.label = customization.label
      }
      if (customization.last_value_still_valid_on !== undefined) {
        if (
          decomposition.last_value_still_valid_on !== undefined &&
          decomposition.last_value_still_valid_on >
            customization.last_value_still_valid_on
        ) {
          console.warn(
            `Attribute last_value_still_valid_on is more recent in variable ${name} (${decomposition.last_value_still_valid_on}) than in customization (${customization.last_value_still_valid_on})`,
          )
        } else {
          decomposition.last_value_still_valid_on =
            customization.last_value_still_valid_on
        }
      }
      if (customization.obsolete) {
        decomposition.obsolete = customization.obsolete
      }
      if (customization.options !== undefined) {
        decomposition.options = customization.options
      }
      if (customization.reference !== undefined && customization.virtual) {
        decomposition.reference = customization.reference
      }
      if (customization.short_label !== undefined) {
        decomposition.short_label = customization.short_label
      }
      if (customization.virtual) {
        decomposition.virtual = customization.virtual
      }
      for (const childReference of decomposition.children ?? []) {
        if (!encounteredNames.has(childReference.name)) {
          encounteredNames.add(childReference.name)
          remainingNames.add(childReference.name)
        }
      }
    }

    for (const name of Object.keys(mergedDecompositionByName)) {
      setRecursiveAttributes(mergedDecompositionByName, name, new Set())
    }

    const [validMergedDecompositionByName, mergedDecompositionByNameError] =
      auditChain(auditDecompositionByName, auditRequire)(
        strictAudit,
        mergedDecompositionByName,
      ) as [DecompositionByName, unknown]
    if (mergedDecompositionByNameError !== null) {
      console.error("Errors in merged decompositions:")
      console.error(JSON.stringify(validMergedDecompositionByName, null, 2))
      console.error(JSON.stringify(mergedDecompositionByNameError, null, 2))
      process.exit(1)
    }

    await fs.writeJson(
      path.join(taxBenefitDir, "decompositions.json"),
      mergedDecompositionByName,
      { replacer: jsonReplacer, spaces: 2 },
    )
  }
}

function setRecursiveAttributes(
  decompositionByName: DecompositionByName,
  name: string,
  handledNames: Set<string>,
): Decomposition {
  const decomposition = decompositionByName[name]
  assert.notStrictEqual(decomposition, undefined)
  if (handledNames.has(name)) {
    return decomposition
  }
  handledNames.add(name)
  if (decomposition.children !== undefined) {
    let hidden = decomposition.hidden
    if (!hidden) {
      hidden = true
      for (const { name: childName } of decomposition.children) {
        const child = setRecursiveAttributes(
          decompositionByName,
          childName,
          handledNames,
        )
        if (!child.hidden || !child.obsolete) {
          hidden = false
          break
        }
      }
      if (hidden) {
        decomposition.hidden = true
      }
    }
  }
  return decomposition
}

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
