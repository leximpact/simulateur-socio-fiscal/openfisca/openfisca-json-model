module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        corejs: 3,
        modules: false,
        targets: { node: true },
        useBuiltIns: "usage",
      },
    ],
    "@babel/preset-typescript",
  ],
  plugins: ["babel-plugin-add-import-extension"],
}
