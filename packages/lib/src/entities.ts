export type Entity = GroupEntity | PersonEntity

export interface EntityBase {
  documentation?: string
  is_person?: boolean
  key: string
  key_plural?: string
  label?: string
  label_plural?: string
}

export type EntityByKey = { [key: string]: Entity }

export interface GroupEntity extends EntityBase {
  composition_label?: string
  is_person?: false
  is_single_label?: string
  roles: Role[]
}

export interface PersonEntity extends EntityBase {
  is_person: true
}

export interface Role extends RoleBase {
  adult?: boolean // Can adults be added to this role?
  child?: boolean // Can children be added to this role?
  subroles?: SubRole[]
}

export interface RoleBase {
  documentation?: string
  key: string
  key_plural?: string
  label?: string
  max?: number
}

type SubRole = RoleBase

export function getRolePersonsIdKey(role: Role | SubRole): string {
  return role.max === undefined || role.max > 1 ? role.key_plural! : role.key
}

export function isAdultRole(role: Role): boolean {
  return role.adult || (role.adult === undefined && role.child === undefined)
}

export function isChildRole(role: Role): boolean {
  return role.child || (role.adult === undefined && role.child === undefined)
}

export function* iterFlattenedRole(
  role: Role,
): Generator<Role | SubRole, void, undefined> {
  if (role.subroles === undefined) {
    if (role.max === undefined) {
      while (true) {
        yield role
      }
    } else {
      for (let index = 0; index < role.max; index++) {
        yield role
      }
    }
  } else {
    if (role.max === undefined) {
      for (const subrole of role.subroles) {
        yield* iterSubRole(subrole)
      }
    } else {
      let index = 0
      for (const subrole of role.subroles) {
        for (const subrole1 of iterSubRole(subrole)) {
          if (index >= role.max) {
            return
          }
          yield subrole1
          index++
        }
      }
    }
  }
}

export function* iterSubRole(
  subrole: SubRole,
): Generator<SubRole, void, undefined> {
  if (subrole.max === undefined) {
    while (true) {
      yield subrole
    }
  } else {
    for (let index = 0; index < subrole.max; index++) {
      yield subrole
    }
  }
}
