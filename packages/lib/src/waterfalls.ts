import { DecompositionReference } from "./decompositions"

export interface Waterfall {
  advanced?: boolean
  icon?: string
  label: string
  name: string
  root: string
  total?: DecompositionReference[]
  totalLabel: string
}
