export interface Reference {
  href?: string
  note?: string
  title?: string
}

export type ReferencesByInstant = { [instant: string]: Reference[] }
