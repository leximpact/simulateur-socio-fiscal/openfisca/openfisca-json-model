/// Structure and functions of editable parameters
/// Note: raw parameters have no structure.

import type { Metadata } from "./metadata"
import type { Reference, ReferencesByInstant } from "./references"

export interface AmountBracket extends BracketBase {
  amount: { [instant: string]: NumberValue | "expected" }
}

export interface AmountBracketAtInstant extends BracketAtInstantBase {
  amount: NumberValue | "expected"
}

export interface BooleanParameter extends ValueParameterBase {
  type: ValueType.Boolean
  values: { [instant: string]: MaybeBooleanValue | "expected" }
}

export interface BooleanValue extends ValueBase {
  value: boolean
}

export type Bracket = AmountBracket | RateBracket

export type BracketAtInstant = AmountBracketAtInstant | RateBracketAtInstant

export interface BracketAtInstantBase {
  threshold: MaybeNumberValue | "expected"
}

export interface BracketBase {
  threshold: { [instant: string]: MaybeNumberValue | "expected" }
}

export type BracketValueAtInstant = MaybeNumberValue | NumberValue | "expected"

export interface LinearAverageRateScaleParameter extends ScaleParameterBase {
  brackets: RateBracket[]
  rate_unit?: string
  type: ScaleType.LinearAverageRate
}

export interface MarginalAmountScaleParameter extends ScaleParameterBase {
  amount_unit?: string
  brackets: AmountBracket[]
  type: ScaleType.MarginalAmount
}

export interface MarginalRateScaleParameter extends ScaleParameterBase {
  brackets: RateBracket[]
  rate_unit?: string
  type: ScaleType.MarginalRate
}

export interface MaybeBooleanValue extends ValueBase {
  value: boolean | null
}

export interface MaybeNumberValue extends ValueBase {
  value: number | null
}

export interface MaybeStringArrayValue extends ValueBase {
  value: Array<string | null> | null
}

export interface MaybeStringByStringValue extends ValueBase {
  value: { [key: string]: string | null } | null
}

export interface NodeParameter extends ParameterBase {
  // Children are present in every ParameterNode, but are removed from ancestors.
  children?: { [id: string]: Parameter }
  class: ParameterClass.Node
  order?: string[]
  unit?: string
}

export interface NumberValue extends ValueBase {
  value: number
}

export interface NumberParameter extends ValueParameterBase {
  type: ValueType.Number
  values: { [instant: string]: MaybeNumberValue | "expected" }
}

export type OfficialJournalDatesByInstant = { [instant: string]: string | null }

export type Parameter = NodeParameter | ScaleParameter | ValueParameter

export interface ParameterBase {
  class: ParameterClass
  description?: string
  label_en?: string
  documentation?: string
  documentation_start?: boolean
  file_path?: string
  id?: string // Generated attribute (last part of name)
  /// Name of parameter that is used to automatically revaluate this parameter with inflation
  inflator?: string
  inflator_reference?: ReferencesByInstant
  last_value_still_valid_on?: string
  name?: string
  notes?: ReferencesByInstant
  official_journal_date?: OfficialJournalDatesByInstant
  parent?: NodeParameter // Generated attribute
  reference?: ReferencesByInstant
  referring_variables?: string[]
  short_label?: string
  short_label_en?: string
  title?: string // Generated attribute constructed using short_label or description or name
  titles?: string[] // Generated attribute aggregating the titles of every ancestors
}

export enum ParameterClass {
  Node = "Node",
  Scale = "Scale",
  Value = "Value",
}

export interface ParameterWithAncestors {
  parameter: Parameter
  ancestors: NodeParameter[]
}

export interface RateBracket extends BracketBase {
  base?: { [instant: string]: NumberValue | "expected" }
  rate: { [instant: string]: MaybeNumberValue | "expected" }
}

export interface RateBracketAtInstant extends BracketAtInstantBase {
  base?: NumberValue | "expected"
  rate: MaybeNumberValue | "expected"
}

export type ScaleAtInstant = BracketAtInstant[]

export type ScaleParameter =
  | LinearAverageRateScaleParameter
  | MarginalAmountScaleParameter
  | MarginalRateScaleParameter
  | SingleAmountScaleParameter

export interface ScaleParameterBase extends ParameterBase {
  brackets: AmountBracket[] | RateBracket[]
  class: ParameterClass.Scale
  ipp_csv_id?: string | { [bracketName: string]: string }
  threshold_unit?: string
  type: ScaleType
}

export enum ScaleType {
  LinearAverageRate = "linear_average_rate",
  MarginalAmount = "marginal_amount",
  MarginalRate = "marginal_rate",
  SingleAmount = "single_amount",
}

export interface SingleAmountScaleParameter extends ScaleParameterBase {
  amount_unit?: string
  brackets: AmountBracket[]
  type: ScaleType.SingleAmount
}

export interface StringArrayParameter extends ValueParameterBase {
  type: ValueType.StringArray
  values: { [instant: string]: MaybeStringArrayValue }
}

export interface StringByStringParameter extends ValueParameterBase {
  type: ValueType.StringByString
  values: { [instant: string]: MaybeStringByStringValue }
}

export type ValueAtInstant =
  | BooleanValue
  | MaybeBooleanValue
  | MaybeNumberValue
  | MaybeStringArrayValue
  | MaybeStringByStringValue
  | NumberValue
  | "expected"

export interface ValueBase {
  reference?: Reference[]
  unit?: string // Note: A bracket value has no unit.
  value?: unknown
}

export type ValueParameter =
  | BooleanParameter
  | NumberParameter
  | StringArrayParameter
  | StringByStringParameter

export interface ValueParameterBase extends ParameterBase {
  class: ParameterClass.Value
  ipp_csv_id?: string
  type: ValueType
  unit?: string
  values: { [instant: string]: ValueAtInstant }
}

export enum ValueType {
  Boolean = "boolean",
  Number = "number",
  StringArray = "string_array",
  StringByString = "string_by_string",
}

export function accessParameterFromIds(
  parameter: Parameter,
  ids: string[],
): [Parameter, unknown] {
  for (const id of ids) {
    if (parameter.class !== ParameterClass.Node) {
      return [
        parameter,
        `Can't access to child "${id}" in parameter ${parameter.name}, because it is not a node`,
      ]
    }
    const child = parameter.children?.[id]
    if (child === undefined) {
      return [
        parameter,
        `Parameter ${parameter.name} has no child named "${id}"`,
      ]
    }
    parameter = child
  }
  return [parameter, null]
}

/// Convert a dictionary of brackets by instant to a list of brackets
/// containing amounts, bases, rates & thresholds by instant.
export function bracketsFromScaleByInstant(scaleByInstant: {
  [instant: string]: ScaleAtInstant
}): AmountBracket[] | RateBracket[] {
  const brackets: Partial<Bracket>[] = []
  for (const key of ["amount", "base", "rate", "threshold"]) {
    let latestValue: MaybeNumberValue | "expected" | undefined = undefined
    for (const [instant, scaleAtInstant] of Object.entries(scaleByInstant).sort(
      ([instant1], [instant2]) => instant1.localeCompare(instant2),
    )) {
      for (const [bracketIndex, bracketAtInstant] of scaleAtInstant.entries()) {
        if (brackets.length <= bracketIndex) {
          brackets.push({})
        }
        const value = bracketAtInstant[key as keyof BracketAtInstant]
        if (value !== undefined) {
          const bracket = brackets[bracketIndex]
          let valueByInstant = bracket[key as keyof Bracket]
          if (valueByInstant === undefined) {
            valueByInstant = bracket[key as keyof Bracket] = {}
          }
          if (value === "expected") {
            if (value !== latestValue) {
              latestValue = valueByInstant[instant] = value
            }
          } else if (
            latestValue === undefined ||
            latestValue === "expected" ||
            value.value !== latestValue.value
          ) {
            latestValue = valueByInstant[instant] = value
          } else if (value.reference !== undefined) {
            let latestReferences = latestValue.reference
            if (latestReferences === undefined) {
              latestReferences = latestValue.reference = []
            }
            for (const reference of value.reference) {
              if (
                !latestReferences.some(
                  (latestReference) =>
                    latestReference.href === reference.href &&
                    latestReference.title === reference.title,
                )
              ) {
                latestReferences.push(reference)
              }
            }
          }
        }
      }
    }
  }
  return brackets as AmountBracket[] | RateBracket[]
}

export function improveParameter(
  parameter: Parameter,
  parent: NodeParameter | undefined | null = undefined,
  ids: string[] = [],
): void {
  const id = ids[ids.length - 1]
  if (id !== undefined) {
    parameter.id = id
  }
  if (parameter.name === undefined) {
    parameter.name = ids.join(".")
  }
  if (parent != null) {
    parameter.parent = parent
  }
  const title =
    parameter.short_label ??
    parameter.description ??
    id?.replace(/_/g, " ").replace(/^\w/, (c) => c.toUpperCase())
  if (title !== undefined) {
    parameter.title = title
  }
  parameter.titles = (
    title === undefined
      ? (parent?.titles ?? [])
      : [...(parent?.titles ?? []), title]
  ).filter(Boolean)

  switch (parameter.class) {
    case ParameterClass.Node:
      if (parameter.children !== undefined) {
        for (const [childId, child] of Object.entries(parameter.children)) {
          improveParameter(child, parameter, [...ids, childId])
        }
      }
      break
    default:
  }
}

export function isAmountScaleParameter(parameter: ScaleParameter) {
  return [ScaleType.MarginalAmount, ScaleType.SingleAmount].includes(
    parameter.type,
  )
}

export function isRateScaleParameter(parameter: ScaleParameter) {
  return ![ScaleType.MarginalAmount, ScaleType.SingleAmount].includes(
    parameter.type,
  )
}

export function isVectorialNodeParameter(node: NodeParameter): boolean {
  if (node.children === undefined || Object.keys(node.children).length <= 1) {
    return false
  }
  const children = Object.values(node.children) as ValueParameter[]
  if (children.some((child) => child.class !== ParameterClass.Value)) {
    return false
  }
  const commonType = children[0].type
  if (children.some((child) => child.type !== commonType)) {
    return false
  }
  const commonFilePath = children[0].file_path
  if (children.some((child) => child.file_path !== commonFilePath)) {
    return false
  }
  return true
}

export function* iterParameterAncestors(
  parameter?: Parameter | undefined | null,
): Generator<Parameter, void, unknown> {
  if (parameter == null || !parameter.id) {
    return
  }
  yield* iterParameterAncestors(parameter.parent)
  yield parameter
}

function mergeDuplicateParameters(
  existingParameter: Parameter,
  parameter: Parameter,
): void {
  if (existingParameter.class === ParameterClass.Node) {
    const existingChidren = existingParameter.children as {
      [id: string]: Parameter
    }
    for (const child of Object.values(
      (parameter as NodeParameter).children as {
        [id: string]: Parameter
      },
    )) {
      const existingChild = existingChidren[child.id ?? ""]
      if (existingChild === undefined) {
        existingChidren[child.id ?? ""] = child
        child.parent = existingParameter
      } else {
        mergeDuplicateParameters(existingChild, child)
      }
    }
  }
}

export function mergeParameters(parameters: Parameter[]): {
  [id: string]: Parameter
} {
  const rootParameterById: { [id: string]: Parameter } = {}
  for (const parameter of parameters) {
    // Create a copy of the parameters tree, containing only this parameter.
    let rootParameter = { ...parameter }
    for (
      let ancestor: NodeParameter | undefined = rootParameter.parent;
      ancestor !== undefined && ancestor.id;
      rootParameter = ancestor!, ancestor = rootParameter.parent
    ) {
      ancestor = {
        ...ancestor,
        children: { [rootParameter.id!]: rootParameter },
      }
      rootParameter.parent = ancestor
    }

    // Merge this simplified parameters tree with the others.
    const existingRootParameter = rootParameterById[rootParameter.id ?? ""]
    if (existingRootParameter === undefined) {
      rootParameterById[rootParameter.id ?? ""] = rootParameter
    } else {
      mergeDuplicateParameters(existingRootParameter, rootParameter)
    }
  }
  return rootParameterById
}

export function mergeReferences(
  references1: Reference[] | undefined | null,
  references2: Reference[] | undefined | null,
): Reference[] | undefined {
  if (references1 == null) {
    return references2 ?? undefined
  }
  if (references2 == null) {
    return references1 ?? undefined
  }
  const references = [...references1]
  for (const reference2 of references2) {
    let found = false
    if (reference2.href !== undefined) {
      const index = references.findIndex(
        (reference) => reference.href === reference2.href,
      )
      if (index >= 0) {
        found = true
        let reference = references[index]
        let changed = false
        if (reference.note === undefined && reference2.note !== undefined) {
          if (!changed) {
            changed = true
            reference = references[index] = { ...reference }
          }
          reference.note = reference2.note
        }
        if (reference.title === undefined && reference2.title !== undefined) {
          if (!changed) {
            changed = true
            reference = references[index] = { ...reference }
          }
          reference.title = reference2.title
        }
      }
    } else if (reference2.title !== undefined) {
      const index = references.findIndex(
        (reference) => reference.title === reference2.title,
      )
      if (index >= 0) {
        found = true
        let reference = references[index]
        if (reference.note === undefined && reference2.note !== undefined) {
          reference = references[index] = { ...reference }
          reference.note = reference2.note
        }
      }
    } else {
      // assert.notStrictEqual(reference2.note, undefined)
      const index = references.findIndex(
        (reference) => reference.note === reference2.note,
      )
      if (index >= 0) {
        found = true
      }
    }
    if (!found) {
      references.push(reference2)
    }
  }
  return references
}

export function newParameterRepositoryUrl(
  metadata: Metadata,
  parameter: Parameter,
): string | undefined {
  if (parameter.file_path === undefined) {
    return undefined
  }
  const packageName = parameter.file_path.split("/")[0]
  for (const packageMetadata of metadata.packages) {
    if (packageMetadata.name === packageName) {
      return `${packageMetadata.repository_url}/blob/${packageMetadata.version}/${parameter.file_path}`
    }
  }
  return undefined
}

export function parameterLastReviewOrChange(
  parameter: Parameter,
): string | undefined {
  switch (parameter.class) {
    case ParameterClass.Node:
      return parameter.last_value_still_valid_on

    case ParameterClass.Scale: {
      const latestInstant = [
        parameter.last_value_still_valid_on,
        ...Object.keys(scaleByInstantFromBrackets(parameter.brackets)),
      ]
        .filter((instant) => instant !== undefined)
        .sort((instant1, instant2) => instant2!.localeCompare(instant1!))[0]
      return latestInstant
    }

    case ParameterClass.Value: {
      const latestInstant = [
        parameter.last_value_still_valid_on,
        ...Object.keys(parameter.values),
      ]
        .filter((instant) => instant !== undefined)
        .sort((instant1, instant2) => instant2!.localeCompare(instant1!))[0]
      return latestInstant
    }
  }
}

export function parameterWithoutChildren(parameter: Parameter): Parameter {
  if (parameter.class === ParameterClass.Node) {
    parameter = { ...parameter }
    delete parameter.children
  }
  return parameter
}

/// Apply changes of a reform to (root) parameter.
export function patchParameter<ParameterType extends Parameter>(
  parameter: ParameterType,
  patch: { [key: string]: unknown },
): ParameterType {
  if (Object.keys(patch).length === 0) {
    return parameter
  }
  const patchedParameter = { ...parameter }
  for (const [key, value] of Object.entries(patch)) {
    if (value === null) {
      delete (patchedParameter as unknown as { [key: string]: unknown })[key]
    } else if (
      (patchedParameter as unknown as { [key: string]: unknown })[key] ===
      undefined
    ) {
      ;(patchedParameter as unknown as { [key: string]: unknown })[key] = value
    } else if (key === "children") {
      const patchedChildren = ((patchedParameter as NodeParameter).children = {
        ...(patchedParameter as NodeParameter).children,
      })
      for (const [childId, childPatch] of Object.entries(
        value as { [childId: string]: unknown },
      )) {
        if (childPatch === null) {
          delete patchedChildren[childId]
        } else if (patchedChildren[childId] === undefined) {
          patchedChildren[childId] = childPatch as Parameter
        } else {
          patchedChildren[childId] = patchParameter(
            patchedChildren[childId],
            childPatch as { [key: string]: unknown },
          )
        }
      }
    } else {
      ;(patchedParameter as unknown as { [key: string]: unknown })[key] = value
    }
  }
  return patchedParameter
}

/// Convert a list of brackets containing amounts, bases, rates & thresholds by instant
/// to a dictionary of brackets by instant.
/// For heuristic, see function `build_api_scale` of
/// https://github.com/openfisca/openfisca-core/blob/master/openfisca_web_api/loader/parameters.py
export function scaleByInstantFromBrackets(brackets: Bracket[]): {
  [instant: string]: ScaleAtInstant
} {
  const instants = new Set<string>()
  for (const bracket of brackets) {
    for (const instant of Object.keys(
      (bracket as AmountBracket).amount ?? {},
    )) {
      instants.add(instant)
    }
    for (const instant of Object.keys((bracket as RateBracket).base ?? {})) {
      instants.add(instant)
    }
    for (const instant of Object.keys((bracket as RateBracket).rate ?? {})) {
      instants.add(instant)
    }
    for (const instant of Object.keys(bracket.threshold)) {
      instants.add(instant)
    }
  }

  const scaleAtInstantByInstant: {
    [instant: string]: ScaleAtInstant
  } = {}
  for (const instant of instants) {
    const scaleAtInstant: ScaleAtInstant = []
    for (const bracket of brackets) {
      const bracketAtInstant = {} as { [key: string]: unknown }
      for (const key of ["amount", "base", "rate", "threshold"]) {
        const valueByInstant = (
          bracket as unknown as {
            [key: string]: { [instant: string]: unknown }
          }
        )?.[key]
        if (valueByInstant === undefined) {
          continue
        }
        let bestInstant: string | undefined = undefined
        for (const valueInstant of Object.keys(valueByInstant)) {
          if (
            (bestInstant === undefined ||
              bestInstant.localeCompare(valueInstant) < 0) &&
            valueInstant.localeCompare(instant) <= 0
          ) {
            bestInstant = valueInstant
          }
        }
        if (bestInstant === undefined) {
          continue
        }
        bracketAtInstant[key] = valueByInstant[bestInstant]
      }
      if (
        Object.keys(bracketAtInstant).length === 0 ||
        Object.values(bracketAtInstant).every(
          (value) =>
            value !== "expected" &&
            (value as { value?: unknown }).value === null,
        )
      ) {
        continue
      }
      scaleAtInstant.push(bracketAtInstant as unknown as BracketAtInstant)
    }
    if (scaleAtInstant.length > 0) {
      scaleAtInstantByInstant[instant] = scaleAtInstant
    }
  }
  return scaleAtInstantByInstant
}

export function scaleParameterUsesBase(parameter: ScaleParameter): boolean {
  return (
    isRateScaleParameter(parameter) &&
    (parameter.brackets as RateBracket[]).some(
      (bracket: RateBracket) => bracket.base !== undefined,
    )
  )
}

export function* walkParameters(
  parameter: Parameter,
  depthFirst = false,
): Generator<Parameter, void, unknown> {
  if (!depthFirst && parameter.class !== ParameterClass.Node) {
    yield parameter
  }
  switch (parameter.class) {
    case ParameterClass.Node:
      if (parameter.children !== undefined) {
        for (const child of Object.values(parameter.children)) {
          yield* walkParameters(child, depthFirst)
        }
      }
      break
    default:
  }
  if (depthFirst && parameter.class !== ParameterClass.Node) {
    yield parameter
  }
}
