import type { Entity, GroupEntity } from "./entities"
import { getRolePersonsIdKey } from "./entities"
import type { VariableValue } from "./variables"

/// A population is either a group (a familly, etc) or a person.
export interface Population extends PopulationWithoutId {
  id: string
}

export interface PopulationWithoutId {
  name?: string
  [key: string]:
    | { [instant: string]: VariableValue | null } // variable value by instant
    | string // id & name
    | string[] // ids of persons (when key is a role and entity is not a person)
    | undefined // name
}

export type Situation = {
  [entityKeyPlural: string]: { [populationId: string]: PopulationWithoutId }
} & {
  description?: string
  dixieme: number
  /** ID generated from test case filename, without the ".json" extension */
  id?: string
  /** Name of variables that this test case illustrates */
  linked_variables?: {
    [variableName: string]: Array<{
      description?: string
      /** optional ID of a situation that must be compared to this situation for this variable */
      compare_to?: string
    }>
  }
  title?: string
}

export function getPopulationReservedKeys(entity: Entity): Set<string> {
  return new Set(
    entity.is_person
      ? ["name"]
      : [
          "name",
          ...(entity as GroupEntity).roles.map((role) =>
            getRolePersonsIdKey(role),
          ),
        ],
  )
}
