import yaml from "js-yaml"

const indexByKey: { [key: string]: number } = Object.fromEntries(
  [
    "name",
    "file_path",
    "description",
    "brackets",
    "threshold",
    "amount",
    "base",
    "rate",
    "average_rate",
    "values",
    "metadata",
    "documentation",
    "documentation_start",

    // Metadata keys
    "short_label",
    "last_value_still_valid_on",
    "label_en",
    "short_label_en",
    "ipp_csv_id",
    "unit",
    "reference",
    "inflator",
    "inflator_reference",
    "official_journal_date",
    "notes",

    // References keys
    "title",
    "href",
  ].map((key, index) => [key, index]),
)

/// Add 0s to the left of numbers inside texts, to ensure that they are properly
/// sorted. For example "tranche_1" becomes "tranche_0001" & "tranche_10" becomes
/// "tranche_0010".
/// Also ensure that keys starting with "before_" are before keys starting with
/// "after_".
function sortKeyFromString(s: string): string {
  return s
    .replace(/\d+/g, (match) => match.padStart(10, "0"))
    .replace(/^after_/, "beforf_")
}

export function rawParameterFromYaml(rawParameterYaml: string) {
  return yaml.load(rawParameterYaml, {
    schema: yaml.JSON_SCHEMA, // Keep dates as strings.
  })
}

export function unitsFromYaml(unitsYaml: string) {
  return yaml.load(unitsYaml, {
    schema: yaml.JSON_SCHEMA, // Keep dates as strings.
  })
}

export function yamlFromRawParameter(rawParameter: unknown): string {
  return (
    yaml
      .dump(rawParameter, {
        lineWidth: -1,
        noArrayIndent: true,
        quotingType: '"',
        sortKeys: (keyA: string, keyB: string) => {
          const indexA = indexByKey[keyA]
          const indexB = indexByKey[keyB]
          if (indexA === undefined && indexB === undefined) {
            const numberA = Number(keyA)
            const numberB = Number(keyB)
            return Number.isNaN(numberA) && Number.isNaN(numberB)
              ? sortKeyFromString(keyA).localeCompare(sortKeyFromString(keyB))
              : numberA === undefined
                ? -1 // KeyA must be before numberB.
                : numberB === undefined
                  ? 1 // NumberA must be after keyB.
                  : numberA - numberB
          }
          return indexA === undefined
            ? 1 // KeyA must be after keyB.
            : indexB === undefined
              ? -1 // KeyA must be before keyB.
              : indexA - indexB
        },
      })
      // Remove quotes around instant keys.
      .replace(
        /^( *)"(0001-01-01|[12]\d{3}(-(0[1-9]|1[012])(-(0[1-9]|[12]\d|3[01]))?)?)":/gm,
        "$1$2:",
      )
      // Remove quotes around integer keys.
      .replace(/^( *)"(\d+)":/gm, "$1$2:")
  )
}
