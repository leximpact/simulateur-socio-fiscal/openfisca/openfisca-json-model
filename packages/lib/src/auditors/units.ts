import {
  auditChain,
  auditBoolean,
  auditCleanArray,
  auditFunction,
  auditKeyValueDictionary,
  auditOptions,
  auditRequire,
  auditString,
  auditSwitch,
  auditTrimString,
  type Audit,
} from "@auditors/core"

import { pluralizationCategories } from "../units"

import { auditDate } from "./periods"

function auditConstantUnit(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  auditConstantUnitContent(audit, data, errors, remainingKeys)

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditConstantUnitContent(
  audit: Audit,
  data: { [key: string]: unknown },
  errors: { [key: string]: unknown },
  remainingKeys: Set<string>,
): void {
  for (const key of ["label", "short_label"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditSwitch(
        auditString,
        auditKeyValueDictionary(auditOptions([...pluralizationCategories]), [
          auditString,
          auditRequire,
        ]),
      ),
    )
  }
  audit.attribute(
    data,
    "name",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditRequire,
  )
  audit.attribute(
    data,
    "parameter",
    true,
    errors,
    remainingKeys,
    auditTrimString,
  )
  audit.attribute(
    data,
    "ratio",
    true,
    errors,
    remainingKeys,
    auditBoolean,
    auditFunction((value) => (value ? true : null)),
  )
}

function auditUnit(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  if (data.units == undefined) {
    auditConstantUnitContent(audit, data, errors, remainingKeys)
  } else {
    for (const key of ["label", "short_label"]) {
      audit.attribute(
        data,
        key,
        true,
        errors,
        remainingKeys,
        auditSwitch(
          auditString,
          auditKeyValueDictionary(auditOptions([...pluralizationCategories]), [
            auditString,
            auditRequire,
          ]),
        ),
      )
    }
    audit.attribute(
      data,
      "name",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
    audit.attribute(
      data,
      "units",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditDate, [auditConstantUnit, auditRequire]),
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const auditUnits = auditChain(auditCleanArray(auditUnit), auditRequire)
