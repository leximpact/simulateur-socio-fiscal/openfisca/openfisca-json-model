import {
  auditArray,
  auditBoolean,
  auditChain,
  auditDateIso8601String,
  auditEmptyToNull,
  auditFunction,
  auditInteger,
  auditKeyValueDictionary,
  auditNumber,
  auditOptions,
  auditRequire,
  auditString,
  auditStringToNumber,
  auditSwitch,
  auditTest,
  auditTrimString,
  auditUnique,
  type Audit,
  type Auditor,
} from "@auditors/core"

import type {
  Entity,
  EntityByKey,
  GroupEntity,
  PersonEntity,
} from "../entities"
import type { Situation } from "../situations"
import { Variable, VariableByName } from "../variables"

function assertNever(type: string, value: never): never {
  throw new Error(`Unexpected ${type}: ${JSON.stringify(value)}`)
}

function auditEntityVariables(
  audit: Audit,
  data: { [key: string]: unknown },
  errors: { [key: string]: unknown },
  remainingKeys: Set<string>,
  currentSmic: number,
  currentYear: number,
  entity: Entity,
  variableByName: VariableByName,
) {
  for (const [variableName, variable] of Object.entries(variableByName).filter(
    ([, variable]) => variable.entity === entity.key,
  )) {
    audit.attribute(
      data,
      variableName,
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(
        [
          (_audit: Audit, value: string): [unknown, unknown] => {
            try {
              return [
                eval(
                  `let year = ${currentYear}; ${typeof value === "string" && !value.includes("year") ? JSON.stringify(value) : value}`,
                ),
                null,
              ]
            } catch (e) {
              return [value, (e as Error | string).toString()]
            }
          },
          auditSwitch(
            [auditString, auditStringToNumber, auditInteger],
            auditInteger,
          ),
          auditTest(
            (year) => year >= currentYear - 4 && year <= currentYear,
            `Year must be in range [${currentYear - 4}, ${currentYear}]`,
          ),
          auditFunction((year) => year.toString()),
        ],
        [auditVariableValue(currentSmic, variable), auditRequire],
      ),
    )
  }
}

function auditGroupWithoutId(
  currentSmic: number,
  currentYear: number,
  entity: GroupEntity,
  personsIds: string[],
  variableByName: VariableByName,
): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "name",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
    )
    for (const role of entity.roles) {
      const roleKey =
        (role.max ?? 99) > 1 ? (role.key_plural ?? role.key) : role.key
      audit.attribute(
        data,
        roleKey,
        true,
        errors,
        remainingKeys,
        auditArray(
          auditTrimString,
          personsIds.length === 0 ? [] : auditOptions(personsIds),
          auditRequire,
        ),
        auditUnique,
        auditEmptyToNull,
      )
    }

    auditEntityVariables(
      audit,
      data,
      errors,
      remainingKeys,
      currentSmic,
      currentYear,
      entity,
      variableByName,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

function auditLinkedVariable(testCasesIds: string[] | undefined): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "compare_to",
      true,
      errors,
      remainingKeys,
      testCasesIds === undefined ? auditString : auditOptions(testCasesIds),
    )
    audit.attribute(
      data,
      "description",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

function auditPersonWithoutId(
  currentSmic: number,
  currentYear: number,
  entity: PersonEntity,
  variableByName: VariableByName,
): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "name",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
    )

    auditEntityVariables(
      audit,
      data,
      errors,
      remainingKeys,
      currentSmic,
      currentYear,
      entity,
      variableByName,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditSituation(
  currentSmic: number,
  currentYear: number,
  entityByKey: EntityByKey,
  testCasesIds: string[] | undefined,
  variableByName: VariableByName,
  withId: boolean,
): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    let personsIds: string[] = []
    for (const entity of Object.values(entityByKey)) {
      if (entity.is_person) {
        const personEntityKey = entity.key_plural ?? entity.key
        audit.attribute(
          data,
          personEntityKey,
          true,
          errors,
          remainingKeys,
          auditKeyValueDictionary(
            [auditTrimString, auditEmptyToNull],
            [
              auditPersonWithoutId(
                currentSmic,
                currentYear,
                entity,
                variableByName,
              ),
              auditRequire,
            ],
          ),
          auditRequire,
        )
        if (errors[personEntityKey] === undefined) {
          personsIds = Object.keys(
            data[personEntityKey] as Record<string, PersonEntity>,
          )
        }
      }
    }

    for (const entity of Object.values(entityByKey)) {
      if (!entity.is_person) {
        audit.attribute(
          data,
          entity.key_plural ?? entity.key,
          true,
          errors,
          remainingKeys,
          auditKeyValueDictionary(
            [auditTrimString, auditEmptyToNull],
            [
              auditGroupWithoutId(
                currentSmic,
                currentYear,
                entity,
                personsIds,
                variableByName,
              ),
              auditRequire,
            ],
          ),
          auditRequire,
        )
      }
    }

    for (const key of ["description", "title"]) {
      audit.attribute(
        data,
        key,
        true,
        errors,
        remainingKeys,
        auditTrimString,
        auditEmptyToNull,
        auditRequire,
      )
    }
    audit.attribute(
      data,
      "dixieme",
      true,
      errors,
      remainingKeys,
      auditInteger,
      auditTest(
        (value) => 1 <= value && value <= 10,
        "Le dixième doit être compris entre 1 et 10",
      ),
    )
    if (withId) {
      audit.attribute(
        data,
        "id",
        true,
        errors,
        remainingKeys,
        auditTrimString,
        auditEmptyToNull,
        auditRequire,
      )
    }
    audit.attribute(
      data,
      "linked_variables",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditOptions(Object.keys(variableByName)), [
        auditArray(auditLinkedVariable(testCasesIds), auditRequire),
        auditRequire,
      ]),
      auditEmptyToNull,
    )
    audit.attribute(
      data,
      "note",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
    )
    audit.attribute(
      data,
      "sliders",
      true,
      errors,
      remainingKeys,
      auditArray(auditRequire),
    )

    if (
      Object.values(entityByKey).every(
        (entity) => errors[entity.key_plural ?? entity.key] === undefined,
      )
    ) {
      audit.attribute(
        data,
        "sliders",
        true,
        errors,
        remainingKeys,
        auditArray(auditSlider(entityByKey, data as Situation), auditRequire),
        auditEmptyToNull,
      )
    }

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

function auditSlider(entityByKey: EntityByKey, situation: Situation): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "entity",
      true,
      errors,
      remainingKeys,
      auditString,
      auditOptions(Object.keys(entityByKey)),
      auditRequire,
    )
    audit.attribute(
      data,
      "id",
      true,
      errors,
      remainingKeys,
      auditString,
      auditRequire,
    )
    for (const key of ["max", "min"]) {
      audit.attribute(
        data,
        key,
        true,
        errors,
        remainingKeys,
        auditNumber,
        auditRequire,
      )
    }
    audit.attribute(
      data,
      "name",
      true,
      errors,
      remainingKeys,
      auditString,
      auditRequire,
    )

    if (errors.entity === undefined) {
      const entity = entityByKey[data.entity as string]
      const populationById = situation[entity.key_plural ?? entity.key]
      audit.attribute(
        data,
        "id",
        true,
        errors,
        remainingKeys,
        auditOptions(Object.keys(populationById)),
      )

      if (errors.id === undefined) {
        const population = populationById[data.id as string]
        audit.attribute(
          data,
          "name",
          true,
          errors,
          remainingKeys,
          auditOptions(Object.keys(population)),
        )
      }
    }

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

function auditVariableValue(currentSmic: number, variable: Variable): Auditor {
  let valueAuditor: (_audit: Audit, value: string) => [unknown, unknown]
  switch (variable.value_type) {
    case "bool":
      valueAuditor = auditBoolean
      break
    case "date":
      valueAuditor = auditDateIso8601String
      break
    case "Enum":
      valueAuditor = auditOptions(Object.keys(variable.possible_values ?? []))
      break
    case "float":
      valueAuditor = auditNumber
      break
    case "int":
      valueAuditor = auditInteger
      break
    case "str":
      valueAuditor = auditString
      break
    default:
      assertNever("Variable.value_type", variable.value_type)
  }
  return auditChain((_audit: Audit, value: string): [unknown, unknown] => {
    try {
      return [
        eval(
          `let smic = ${currentSmic}; ${typeof value === "string" && !value.includes("smic") ? JSON.stringify(value) : value}`,
        ),
        null,
      ]
    } catch (e) {
      return [value, (e as Error | string).toString()]
    }
  }, valueAuditor)
}
