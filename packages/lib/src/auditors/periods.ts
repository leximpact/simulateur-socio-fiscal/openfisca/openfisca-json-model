import { auditChain, auditTest, auditTrimString } from "@auditors/core"

import { dateRegExp, instantRegExp } from "../periods"

export const auditDate = auditChain(
  auditTrimString,
  auditTest((data: string) => Boolean(dateRegExp.test(data)), "Invalid date"),
)

export const auditInstant = auditChain(
  auditTrimString,
  auditTest(
    (data: string) => Boolean(instantRegExp.test(data)),
    "Invalid instant",
  ),
)
