import {
  Audit,
  auditBoolean,
  auditCleanArray,
  auditFunction,
  auditRequire,
  auditTrimString,
} from "@auditors/core"

import { auditDecompositionReference } from "./decompositions"

export function auditWaterfall(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "advanced",
    true,
    errors,
    remainingKeys,
    auditBoolean,
    auditFunction((value) => value || undefined), // Delete false.
  )
  audit.attribute(data, "icon", true, errors, remainingKeys, auditTrimString)
  for (const key of ["label", "name", "root", "totalLabel"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "total",
    true,
    errors,
    remainingKeys,
    auditCleanArray(auditDecompositionReference),
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}
