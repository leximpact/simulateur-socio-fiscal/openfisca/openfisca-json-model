import type { Audit } from "@auditors/core"
import {
  auditArray,
  auditBoolean,
  auditChain,
  auditCleanArray,
  auditDateIso8601String,
  auditFunction,
  auditKeyValueDictionary,
  auditRequire,
  auditString,
  auditSwitch,
  auditTrimString,
  auditUnique,
} from "@auditors/core"

import { auditReferencesByDate } from "./references"

export function auditCustomization(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  auditDecompositionCoreContent(audit, data, errors, remainingKeys)

  audit.attribute(
    data,
    "input",
    true,
    errors,
    remainingKeys,
    auditBoolean,
    auditFunction((value) => value || undefined), // Delete false.
  )
  audit.attribute(
    data,
    "linked_added_variables",
    true,
    errors,
    remainingKeys,
    auditSwitch(auditBoolean, [auditCleanArray(auditTrimString), auditUnique]),
  )
  audit.attribute(
    data,
    "linked_other_variables",
    true,
    errors,
    remainingKeys,
    auditSwitch(auditBoolean, [auditCleanArray(auditTrimString), auditUnique]),
  )
  audit.attribute(
    data,
    "main_parameters",
    true,
    errors,
    remainingKeys,
    auditCleanArray(auditTrimString),
    auditUnique,
  )
  audit.attribute(
    data,
    "variables",
    true,
    errors,
    remainingKeys,
    auditCleanArray(auditTrimString),
    auditUnique,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const auditCustomizationByName = auditKeyValueDictionary(
  auditTrimString,
  [auditCustomization, auditRequire],
)

export function auditDecomposition(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  auditDecompositionCoreContent(audit, data, errors, remainingKeys)

  audit.attribute(data, "label", true, errors, remainingKeys, auditRequire)

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const auditDecompositionByName = auditKeyValueDictionary(
  auditTrimString,
  [auditDecomposition, auditRequire],
)

function auditDecompositionCoreContent(
  audit: Audit,
  data: { [key: string]: unknown },
  errors: { [key: string]: unknown },
  remainingKeys: Set<string>,
): void {
  audit.attribute(
    data,
    "children",
    false, // Don't delete nullish children (nullish means null after a JSON.parse).
    errors,
    remainingKeys,
    auditCleanArray(auditDecompositionReference),
  )
  audit.attribute(
    data,
    "description",
    true,
    errors,
    remainingKeys,
    auditReferencesByDate,
  )
  // Note: `"hidden": false` is not the same as `undefined`.
  audit.attribute(data, "hidden", true, errors, remainingKeys, auditBoolean)
  for (const key of ["label", "short_label"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  audit.attribute(
    data,
    "last_value_still_valid_on",
    true,
    errors,
    remainingKeys,
    auditDateIso8601String,
  )
  for (const key of ["obsolete", "virtual"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditBoolean,
      auditFunction((value) => value || undefined), // Delete false.
    )
  }
  audit.attribute(
    data,
    "options",
    true,
    errors,
    remainingKeys,
    auditSwitch(
      [auditArray(), auditCleanArray(auditDecompositionOptions)],
      [auditDecompositionOptions, auditFunction((options) => [options])],
    ),
  )
  audit.attribute(
    data,
    "reference",
    true,
    errors,
    remainingKeys,
    auditReferencesByDate,
  )
}

export function auditDecompositionOptions(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "waterfall",
    true,
    errors,
    remainingKeys,
    auditSwitch(
      [auditArray(), auditCleanArray(auditTrimString), auditUnique],
      [auditTrimString, auditFunction((name) => [name])],
    ),
    // TODO: Test existence of waterfall names.
  )

  if (errors.waterfall === undefined) {
    if (data.waterfall === undefined) {
      // Variables options

      for (const key of ["else", "then"]) {
        audit.attribute(
          data,
          key,
          true,
          errors,
          remainingKeys,
          auditVariablesCustomization,
        )
      }

      for (const variableName of remainingKeys) {
        const [validVariableName, variableNameError] = auditChain(
          auditTrimString,
          // TODO: Test existence of variable name.
          auditRequire,
        )(audit, variableName)
        if (variableNameError === null) {
          audit.attribute(
            data,
            variableName,
            true,
            errors,
            remainingKeys,
            auditSwitch(
              [
                auditArray(auditBoolean),
                auditCleanArray(auditBoolean),
                auditUnique,
              ],
              [
                auditArray(auditString),
                auditCleanArray(auditTrimString),
                auditUnique,
              ],
              [auditBoolean, auditFunction((value) => [value])],
              [auditTrimString, auditFunction((value) => [value])],
            ),
            // TODO: Test existence of variable value.
            auditRequire,
          )
          if (
            errors[variableName] === undefined &&
            validVariableName !== variableName
          ) {
            data[validVariableName] = data[variableName]
            delete data[variableName]
          }
        }
      }
    } else {
      // Waterfall options

      for (const key of ["else", "then"]) {
        audit.attribute(
          data,
          key,
          true,
          errors,
          remainingKeys,
          auditWaterfallCustomization,
        )
      }
    }
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditDecompositionReference(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "name",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditRequire,
  )
  audit.attribute(
    data,
    "negate",
    true,
    errors,
    remainingKeys,
    auditBoolean,
    auditFunction((value) => value || undefined), // Replace false with undefined.
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditVariablesCustomization(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  // Note: `"hidden": false` is not the same as `undefined`.
  audit.attribute(data, "hidden", true, errors, remainingKeys, auditBoolean)

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditWaterfallCustomization(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "children",
    false, // Don't delete nullish children (nullish means null after a JSON.parse).
    errors,
    remainingKeys,
    auditCleanArray(auditDecompositionReference),
  )
  // Note: `"hidden": false` is not the same as `undefined`.
  audit.attribute(data, "hidden", true, errors, remainingKeys, auditBoolean)

  return audit.reduceRemaining(data, errors, remainingKeys)
}
