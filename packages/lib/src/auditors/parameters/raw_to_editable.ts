import {
  Audit,
  auditArray,
  auditBoolean,
  auditChain,
  auditCleanArray,
  auditFunction,
  auditHttpUrl,
  auditKeyValueDictionary,
  auditNonEmpty,
  auditNoop,
  auditNullish,
  auditNumber,
  auditOptions,
  auditRequire,
  auditSetNullish,
  auditString,
  auditSwitch,
  auditTest,
  auditTrimString,
  auditTuple,
  auditUnique,
  type Auditor,
} from "@auditors/core"

import {
  // BracketBase,
  // MaybeNumberValue,
  mergeReferences,
  ParameterClass,
  ScaleType,
  ValueType,
  type AmountBracket,
  type BracketValueAtInstant,
  type RateBracket,
  type ValueAtInstant,
} from "../../parameters"
import { dateRegExp } from "../../periods"
import type { ReferencesByInstant } from "../../references"
import type { Unit } from "../../units"

import { auditDate } from "../periods"

export function auditRawBracketToEditablePhase1(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "amount",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(auditDate, [
      auditRawValueOrExpectedToEditable(auditNumber),
      auditRequire,
    ]),
  )
  audit.attribute(
    data,
    "average_rate",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(auditDate, [
      auditRawValueOrExpectedToEditable(auditNumber),
      auditRequire,
    ]),
  )
  audit.attribute(
    data,
    "base",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(auditDate, [
      auditRawValueOrExpectedToEditable(auditNumber),
      auditRequire,
    ]),
  )
  audit.attribute(
    data,
    "rate",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(auditDate, [
      auditRawValueOrExpectedToEditable(auditNumber),
      auditRequire,
    ]),
  )
  audit.attribute(
    data,
    "threshold",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(auditDate, [
      auditRawValueOrExpectedToEditable(auditNumber),
      auditRequire,
    ]),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRawBracketToEditablePhase2Amount(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(data, "amount", true, errors, remainingKeys, auditRequire)
  audit.attribute(
    data,
    "average_rate",
    true,
    errors,
    remainingKeys,
    auditNullish,
  )
  audit.attribute(data, "base", true, errors, remainingKeys, auditNullish)
  audit.attribute(data, "rate", true, errors, remainingKeys, auditNullish)
  audit.attribute(data, "threshold", true, errors, remainingKeys, auditRequire)

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRawBracketPhase2AverageRate(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(data, "amount", true, errors, remainingKeys, auditNullish)
  audit.attribute(
    data,
    "average_rate",
    true,
    errors,
    remainingKeys,
    auditRequire,
  )
  audit.attribute(data, "base", true, errors, remainingKeys)
  audit.attribute(data, "rate", true, errors, remainingKeys, auditNullish)
  audit.attribute(data, "threshold", true, errors, remainingKeys, auditRequire)

  if (errors.average_rate === undefined && errors.rate === undefined) {
    data.rate = data.average_rate
    delete data.average_rate
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRawBracketToEditablePhase2MarginalRate(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(data, "amount", true, errors, remainingKeys, auditNullish)
  audit.attribute(data, "base", true, errors, remainingKeys)
  audit.attribute(
    data,
    "average_rate",
    true,
    errors,
    remainingKeys,
    auditNullish,
  )
  audit.attribute(data, "rate", true, errors, remainingKeys, auditRequire)
  audit.attribute(data, "threshold", true, errors, remainingKeys, auditRequire)

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditRawMetadataBaseAttributesToEditable(
  audit: Audit,
  data: { [key: string]: unknown },
  errors: { [key: string]: unknown },
  remainingKeys: Set<string>,
): void {
  for (const key of [
    "description",
    "label_en",
    "documentation",
    "short_label",
    "short_label_en",
  ]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  audit.attribute(
    data,
    "documentation_start",
    true,
    errors,
    remainingKeys,
    auditBoolean,
    auditFunction((value) => (value ? true : null)),
  )
  audit.attribute(
    data,
    "last_value_still_valid_on",
    true,
    errors,
    remainingKeys,
    auditDate,
  )
  audit.attribute(
    data,
    "notes",
    true,
    errors,
    remainingKeys,
    auditRawNotesToEditable,
  )
  audit.attribute(
    data,
    "official_journal_date",
    true,
    errors,
    remainingKeys,
    auditKeyValueDictionary(auditDate, [
      auditSwitch(auditDate, [
        auditString,
        auditFunction((text) => text.split(";")),
        auditArray(auditDate),
        // TODO: Return something other than a string?
        auditFunction((instants) => instants.join("; ")),
      ]),
      auditRequire,
    ]),
  )
  audit.attribute(
    data,
    "reference",
    true,
    errors,
    remainingKeys,
    auditRawReferencesToEditable,
  )
  audit.attribute(
    data,
    "inflator",
    true,
    errors,
    remainingKeys,
    auditTrimString,
  )
  audit.attribute(
    data,
    "inflator_reference",
    true,
    errors,
    remainingKeys,
    auditRawReferencesToEditable,
  )
}

export function auditRawNodeParameterMetadataToEditable(
  parameterData: {
    [key: string]: unknown
  },
  units: Unit[],
  childrenId: string[] | undefined,
): Auditor {
  const children = parameterData.children
  const testOrderItems =
    childrenId !== undefined ||
    (children != null && typeof children === "object")
  if (childrenId === undefined) {
    childrenId = testOrderItems ? Object.keys(children!) : []
  }

  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    auditRawMetadataBaseAttributesToEditable(audit, data, errors, remainingKeys)
    // Used for compatibility with "barèmes IPP".
    audit.attribute(
      data,
      "order",
      true,
      errors,
      remainingKeys,
      auditCleanArray(
        auditString,
        testOrderItems ? auditOptions(childrenId!) : auditNoop,
      ),
      auditUnique,
    )
    audit.attribute(
      data,
      "unit",
      true,
      errors,
      remainingKeys,
      auditRawUnitNameToEditable(units),
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditRawParameterToEditable(
  units: Unit[],
  childrenId?: string[] | undefined,
): Auditor {
  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    // Repair data before validation.
    // Move some attributes from data to metadata.
    // See function `_set_backward_compatibility_metadata` of
    // https://github.com/openfisca/openfisca-core/blob/master/openfisca_core/parameters/helpers.py
    for (const key of ["reference", "unit"]) {
      if (data[key] !== undefined) {
        let metadata = data.metadata as { [key: string]: unknown } | undefined
        if (metadata === undefined) {
          metadata = data.metadata = {}
        }
        metadata[key] = data[key]
        delete data[key]
        remainingKeys.delete(key)
      }
    }

    for (const key of ["description", "documentation", "file_path"]) {
      audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
    }
    audit.attribute(
      data,
      "documentation_start",
      true,
      errors,
      remainingKeys,
      auditBoolean,
      auditFunction((value) => (value ? true : null)),
    )
    audit.attribute(
      data,
      "name",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      // auditTest((name) => {
      //   const ids = name.split(".")
      //   return ids[ids.length - 1].match(/^[\d]/) === null
      // }, "Last part of name must not start with a digit"),
    )

    if (data["brackets"] !== undefined) {
      // Data is a Scale.
      data.class = ParameterClass.Scale
      audit.attribute(
        data,
        "brackets",
        true,
        errors,
        remainingKeys,
        auditArray(
          auditRawBracketToEditablePhase1,
          auditNonEmpty,
          auditRequire,
        ),
        auditRequire,
      )
      audit.attribute(
        data,
        "metadata",
        true,
        errors,
        remainingKeys,
        auditRawScaleMetadataToEditable(units),
      )
    } else {
      // Repair data before validation.
      // Create `values` attribute if it is omitted.
      const childrenKeys = [...remainingKeys].filter(
        (key) => !["metadata", "values"].includes(key),
      )
      if (
        data.values === undefined &&
        childrenKeys.length > 0 &&
        childrenKeys.every((key) => dateRegExp.test(key))
      ) {
        const values: { [instant: string]: unknown } = (data.values = {})
        for (const instant of childrenKeys) {
          values[instant] = data[instant]
          delete data[instant]
          remainingKeys.delete(instant)
        }
      }

      if (data["values"] !== undefined) {
        // Data is a Parameter.
        data.class = ParameterClass.Value
        audit.attribute(
          data,
          "metadata",
          true,
          errors,
          remainingKeys,
          auditRawValueParameterMetadataToEditable(units),
        )
        audit.attribute(
          data,
          "values",
          true,
          errors,
          remainingKeys,
          auditSwitch(
            [
              auditKeyValueDictionary(auditDate, [
                auditRawValueOrValueOrExpectedToEditable(auditBoolean),
                auditRequire,
              ]),
              auditFunction((values) => {
                data.type = ValueType.Boolean
                return values
              }),
            ],
            [
              auditKeyValueDictionary(auditDate, [
                auditRawValueOrValueOrExpectedToEditable(auditNumber),
                auditRequire,
              ]),
              auditFunction((values) => {
                data.type = ValueType.Number
                return values
              }),
            ],
            [
              auditKeyValueDictionary(auditDate, [
                auditRawValueOrValueOrExpectedToEditable(
                  auditArray(auditString),
                ),
                auditRequire,
              ]),
              auditFunction((values) => {
                data.type = ValueType.StringArray
                return values
              }),
            ],
            [
              auditKeyValueDictionary(auditDate, [
                auditRawValueOrValueOrExpectedToEditable(
                  auditKeyValueDictionary(auditString, auditString),
                ),
                auditRequire,
              ]),
              auditFunction((values) => {
                data.type = ValueType.StringByString
                return values
              }),
            ],
          ),
          auditRequire,
        )
      } else {
        // Data is a NodeParameter.
        data.class = ParameterClass.Node

        // Repair data before validation.
        // Create `children` attribute from children keys.
        const children: { [key: string]: unknown } = {}
        for (const key of childrenKeys) {
          children[key] = data[key]
          delete data[key]
          remainingKeys.delete(key)
        }
        data.children = children

        audit.attribute(
          data,
          "children",
          true,
          errors,
          remainingKeys,
          auditKeyValueDictionary(auditString, [
            auditRawParameterToEditable(units),
            auditRequire,
          ]),
          // auditRequire, // A node may have no child (especially unprocessed nodes).
        )
        if (
          errors.children !== undefined &&
          typeof errors.children === "object"
        ) {
          Object.assign(errors, errors.children)
          delete errors.children
        }

        audit.attribute(
          data,
          "metadata",
          true,
          errors,
          remainingKeys,
          auditRawNodeParameterMetadataToEditable(data, units, childrenId),
        )
      }
    }

    if (errors.metadata === undefined && data.metadata != null) {
      // Merge metadata into parameter.
      const metadataErrors: { [key: string]: unknown } = {}
      for (const [key, metadataValue] of Object.entries(
        data.metadata as { [key: string]: unknown },
      )) {
        const dataValue = data[key]
        if (metadataValue !== dataValue) {
          if (dataValue === undefined) {
            data[key] = metadataValue
          } else {
            metadataErrors[key] =
              "Field is present both in parameter and its metadata, with different values"
          }
        }
      }
      if (Object.keys(metadataErrors).length > 0) {
        errors.metadata = metadataErrors
      } else {
        delete data.metadata
      }
    }

    if (data.class === ParameterClass.Scale && errors.brackets === undefined) {
      // For heuristic, see function `_get_at_instant` of
      // https://github.com/openfisca/openfisca-core/blob/master/openfisca_core/parameters/parameter_scale.py
      if (data.type === ScaleType.SingleAmount) {
        audit.attribute(
          data,
          "brackets",
          true,
          errors,
          remainingKeys,
          auditArray(auditRawBracketToEditablePhase2Amount, auditRequire),
          auditRequire,
        )

        // An amount scale can only contain an amount_unit or threshold_unit.
        audit.attribute(
          data,
          "rate_unit",
          true,
          errors,
          remainingKeys,
          auditNullish,
        )
      } else if (
        (data.brackets as { amount: unknown }[]).some(
          ({ amount }) => amount != null,
        )
      ) {
        audit.attribute(
          data,
          "brackets",
          true,
          errors,
          remainingKeys,
          auditArray(auditRawBracketToEditablePhase2Amount, auditRequire),
          auditRequire,
        )

        // An amount scale can only contain an amount_unit or threshold_unit.
        audit.attribute(
          data,
          "rate_unit",
          true,
          errors,
          remainingKeys,
          auditNullish,
        )

        audit.attribute(
          data,
          "type",
          true,
          errors,
          remainingKeys,
          auditOptions([ScaleType.MarginalAmount]),
          auditSetNullish(ScaleType.MarginalAmount),
        )
      } else if (
        (data.brackets as { average_rate: unknown }[]).some(
          ({ average_rate }) => average_rate != null,
        )
      ) {
        // A rate scale can only contain a rate_unit and a threshold_unit
        audit.attribute(
          data,
          "amount_unit",
          true,
          errors,
          remainingKeys,
          auditNullish,
        )

        audit.attribute(
          data,
          "brackets",
          true,
          errors,
          remainingKeys,
          auditArray(auditRawBracketPhase2AverageRate, auditRequire),
          auditRequire,
        )

        audit.attribute(
          data,
          "type",
          true,
          errors,
          remainingKeys,
          auditOptions([ScaleType.LinearAverageRate]),
          auditSetNullish(ScaleType.LinearAverageRate),
        )
      } else {
        // A rate scale can only contain a rate_unit and a threshold_unit
        audit.attribute(
          data,
          "amount_unit",
          true,
          errors,
          remainingKeys,
          auditNullish,
        )

        audit.attribute(
          data,
          "brackets",
          true,
          errors,
          remainingKeys,
          auditArray(auditRawBracketToEditablePhase2MarginalRate, auditRequire),
          auditRequire,
        )

        audit.attribute(
          data,
          "type",
          true,
          errors,
          remainingKeys,
          auditOptions([ScaleType.MarginalRate]),
          auditSetNullish(ScaleType.MarginalRate),
        )
      }

      // if (errors.brackets === undefined) {
      //   // Sort brackets by thresholds
      //   const brackets = data.brackets as BracketBase[]
      //   // Si pour toutes les dates de threshold1, les values de threshold2 (qui ne sont pas expected ou null) sont inférieures
      //   for (const [index, bracket2] of brackets.slice(1).entries()) {
      //     const threshold1 = brackets[index].threshold
      //     for (const [instant, value1] of Object.entries(threshold1)) {
      //       if (value1 === "expected" || value1.value === null) {
      //         continue
      //       }
      //       const value2 = bracket2.threshold[instant]
      //       if (
      //         value2 === undefined ||
      //         value2 === "expected" ||
      //         value2.value === null ||
      //         Object.values(bracket2).every(
      //           (valueAtInstant: {
      //             [instant: string]: MaybeNumberValue | "expected"
      //           }) => {
      //             const value = valueAtInstant[instant]
      //             return (
      //               value === undefined ||
      //               value === "expected" ||
      //               value.value === null ||
      //               value.value === 0
      //             )
      //           },
      //         )
      //       ) {
      //         continue
      //       }
      //       if (value1.value > value2.value) {
      //         errors.brackets = {
      //           [index]: `At ${instant}, value ${value1.value} of this bracket is greater than the value ${value2.value} of the next bracket`,
      //         }
      //         break
      //       }
      //     }
      //   }
      // }

      if (
        errors.brackets === undefined &&
        (errors.metadata as { [key: string]: unknown } | undefined)
          ?.ipp_csv_id === undefined
      ) {
        const ippCsvId = (
          data.metadata as { [key: string]: unknown } | undefined
        )?.ipp_csv_id as string | { [name: string]: string } | undefined
        if (ippCsvId !== undefined && typeof ippCsvId === "string") {
          const bracketsName = Object.keys(
            data.brackets as { [key: string]: unknown },
          )
          const ippCsvIdErrorByBracketName: { [key: string]: string } = {}
          for (const bracketName of Object.keys(ippCsvId)) {
            if (!bracketsName.includes(bracketName)) {
              ippCsvIdErrorByBracketName[bracketName] = `Expected ${bracketsName
                .map((bracketName) => `${JSON.stringify(bracketName)}`)
                .join(" or ")} as key, got: ${JSON.stringify(bracketName)}`
            }
          }
          if (Object.keys(ippCsvIdErrorByBracketName).length > 0) {
            if (errors.metadata === undefined) {
              errors.metadata = {}
            }
            ;(errors.metadata as { [key: string]: unknown }).ipp_csv_id =
              ippCsvIdErrorByBracketName
          }
        }
      }

      // => Convert `unit` to `threshold unit` and delete it.
      if (errors.unit === undefined && data.unit != null) {
        if (errors.threshold_unit === undefined) {
          audit.attribute(
            data,
            "threshold_unit",
            true,
            errors,
            remainingKeys,
            auditNullish,
          )
          if (errors.threshold_unit === undefined) {
            data.threshold_unit = data.unit
          }
        }
        delete data.unit
      }
    }

    // Merge the references of the parameter values (and brackets values) with
    // the references of the parameter.
    if (Object.keys(errors).length === 0) {
      const referencesByInstant = (data.reference as ReferencesByInstant) ?? {}
      switch (data.class) {
        case ParameterClass.Node: {
          break
        }
        case ParameterClass.Scale: {
          const brackets = data.brackets as AmountBracket[] | RateBracket[]
          for (const bracket of brackets) {
            for (const key of ["amount", "base", "rate", "threshold"]) {
              const values = (
                bracket as unknown as {
                  [key: string]: { [instant: string]: BracketValueAtInstant }
                }
              )[key]
              if (values === undefined) {
                continue
              }
              for (const [instant, valueAtInstant] of Object.entries(values)) {
                if (
                  valueAtInstant !== "expected" &&
                  valueAtInstant.reference != null
                ) {
                  const mergedReferences = mergeReferences(
                    referencesByInstant[instant],
                    valueAtInstant.reference,
                  )
                  if (mergedReferences !== undefined) {
                    referencesByInstant[instant] = mergedReferences
                  }
                  delete valueAtInstant.reference
                }
              }
            }
          }
          break
        }
        case ParameterClass.Value: {
          for (const [instant, valueAtInstant] of Object.entries(
            data.values as { [instant: string]: ValueAtInstant },
          )) {
            if (
              valueAtInstant !== "expected" &&
              valueAtInstant.reference != null
            ) {
              const mergedReferences = mergeReferences(
                referencesByInstant[instant],
                valueAtInstant.reference,
              )
              if (mergedReferences !== undefined) {
                referencesByInstant[instant] = mergedReferences
              }
              delete valueAtInstant.reference
            }
          }
          break
        }
      }
      if (Object.keys(referencesByInstant).length > 0) {
        data.reference = referencesByInstant
      }
    }

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}
export function auditRawReferenceObjectToEditable(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "href",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    // Remove jsessionid from Légifrance URLs.
    auditFunction((url) => url.replace(/;jsessionid=[^\/?]*/, "")),
  )
  audit.attribute(data, "note", true, errors, remainingKeys, auditTrimString)
  audit.attribute(data, "title", true, errors, remainingKeys, auditTrimString)

  if (
    errors.href === undefined &&
    errors.note === undefined &&
    errors.title === undefined &&
    data.href == null &&
    data.note == null &&
    data.title == null
  ) {
    errors.href =
      errors.note =
      errors.title =
        "A reference object must contain a href and/or a note and/or a title"
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRawScaleMetadataToEditable(units: Unit[]): Auditor {
  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    auditRawMetadataBaseAttributesToEditable(audit, data, errors, remainingKeys)
    // Used only for compatibility with "barèmes IPP".
    audit.attribute(
      data,
      "ipp_csv_id",
      true,
      errors,
      remainingKeys,
      auditSwitch(
        auditTrimString,
        auditKeyValueDictionary(
          auditOptions(["amount", "average_rate", "base", "rate", "threshold"]),
          [auditTrimString, auditRequire],
        ),
      ),
    )
    audit.attribute(
      data,
      "amount_unit",
      true,
      errors,
      remainingKeys,
      auditRawUnitNameToEditable(units),
    )
    audit.attribute(
      // UK
      data,
      "period",
      true,
      errors,
      remainingKeys,
      auditString,
      auditOptions([
        "hour", // UK
        "month", // UK
        "week", // UK
        "year", // UK
      ]),
    )
    audit.attribute(
      data,
      "rate_unit",
      true,
      errors,
      remainingKeys,
      auditRawUnitNameToEditable(units),
    )
    audit.attribute(
      data,
      "threshold_unit",
      true,
      errors,
      remainingKeys,
      auditRawUnitNameToEditable(units),
    )
    audit.attribute(
      data,
      "type",
      true,
      errors,
      remainingKeys,
      auditString,
      auditOptions(["marginal_rate", "single_amount"]),
    )
    audit.attribute(
      data,
      "unit",
      true,
      errors,
      remainingKeys,
      auditRawUnitNameToEditable(units),
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditRawUnitNameToEditable(units: Unit[]): Auditor {
  const unitsName = units.map(({ name }) => name)
  return auditChain(auditString, auditTrimString, auditOptions(unitsName))
}

export function auditRawValueParameterMetadataToEditable(
  units: Unit[],
): Auditor {
  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    auditRawMetadataBaseAttributesToEditable(audit, data, errors, remainingKeys)
    // Used only for compatibility "barèmes IPP".
    audit.attribute(
      data,
      "ipp_csv_id",
      true,
      errors,
      remainingKeys,
      auditTrimString,
    )
    audit.attribute(
      // UK
      data,
      "period",
      true,
      errors,
      remainingKeys,
      auditString,
      auditOptions([
        "hour", // UK
        "month", // UK
        "week", // UK
        "year", // UK
      ]),
    )
    audit.attribute(
      data,
      "unit",
      true,
      errors,
      remainingKeys,
      // Remove US "bool" unit.
      auditFunction((unit) => (unit === "bool" ? undefined : unit)),
      auditRawUnitNameToEditable(units),
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditRawValueToEditable(...auditors: Auditor[]): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "reference",
      true,
      errors,
      remainingKeys,
      auditSwitch(
        [
          auditRawReferenceToEditable,
          auditFunction((reference) => [reference]),
        ],
        auditCleanArray(auditRawReferenceToEditable),
      ),
    )
    audit.attribute(
      data,
      "value",
      false, // Keep null value.
      errors,
      remainingKeys,
      ...auditors,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export const auditRawReferenceToEditable = auditSwitch(
  [
    auditString,
    auditSwitch(
      [
        auditHttpUrl,
        // Remove jsessionid from Légifrance URLs.
        auditFunction((url) => url.replace(/;jsessionid=[^\/?]*/, "")),
        auditFunction((href) => ({ href })),
      ],
      auditChain(
        // When string has the form "title: url", replace it with a reference object.
        auditTest((text) => (text.match(/:/g) ?? []).length > 1),
        auditFunction((text) => {
          const textSplit = text.split(":")
          return [textSplit[0], textSplit.slice(1).join(":")]
        }),
        auditTuple(auditTrimString, auditHttpUrl),
        auditFunction(([title, href]) => ({ href, title })),
      ),
      auditFunction((title) => ({ title })),
    ),
  ],
  auditRawReferenceObjectToEditable,
)

export const auditRawReferencesToEditable = auditSwitch(
  [
    auditRawReferenceToEditable,
    auditFunction((reference) => ({ "0001-01-01": [reference] })),
  ],
  [
    auditArray(),
    auditCleanArray(auditRawReferenceToEditable),
    auditFunction((references) => ({ "0001-01-01": references })),
  ],
  auditKeyValueDictionary(
    auditDate,
    auditSwitch(
      [auditRawReferenceToEditable, auditFunction((reference) => [reference])],
      [auditArray(), auditCleanArray(auditRawReferenceToEditable)],
    ),
  ),
)

// export const auditRawNotesToEditable = auditSwitch(
//   [auditTrimString, auditFunction((note) => ({ "0001-01-01": note }))],
//   [
//     auditArray(),
//     auditCleanArray(auditTrimString),
//     auditFunction((notes: string[]) => ({
//       "0001-01-01": notes.map((note) => `- ${note}`).join("\n"),
//     })),
//   ],
//   auditKeyValueDictionary(auditDate, auditTrimString),
// )
export const auditRawNotesToEditable = auditRawReferencesToEditable

export const auditRawValueOrExpectedToEditable = (
  ...auditors: Auditor[]
): Auditor =>
  auditSwitch(
    auditTest((value) => value === "expected", 'Value is not "expected"'),
    auditRawValueToEditable(...auditors),
  )

export const auditRawValueOrValueOrExpectedToEditable = (
  ...auditors: Auditor[]
): Auditor =>
  auditSwitch(
    auditTest((value) => value === "expected", 'Value is not "expected"'),
    auditRawValueToEditable(...auditors),
    [...auditors, auditFunction((value) => ({ value }))],
  )
