import type { Parameter } from "../../parameters"
import { ParameterClass, ScaleType } from "../../parameters"

/// Note: This converter assumes that the input data is a valid editable parameter.
export function convertEditableParameterToRaw(editableParameter: Parameter): {
  [key: string]: unknown
} {
  const parameterKeys = new Set([
    "brackets",
    "class",
    "children",
    "description",
    "documentation",
    "file_path",
    "name",
    "values",
  ])
  const rawParameter: { [key: string]: unknown } = { ...editableParameter }

  // Move non-parameter attributes to metadata.
  const metadata: { [key: string]: unknown } = {}
  for (const [key, value] of Object.entries(editableParameter)) {
    if (parameterKeys.has(key)) {
      continue
    }
    metadata[key] = value
    delete rawParameter[key]
  }

  switch (editableParameter.class) {
    case ParameterClass.Node: {
      if (editableParameter.children !== undefined) {
        for (const [key, child] of Object.entries(editableParameter.children)) {
          rawParameter[key] = convertEditableParameterToRaw(child)
        }
        delete rawParameter.children
      }
      break
    }
    case ParameterClass.Scale: {
      switch (editableParameter.type) {
        case ScaleType.LinearAverageRate: {
          rawParameter.brackets = editableParameter.brackets.map(
            (editableBracket) => {
              const rawBracket: { [key: string]: unknown } = {
                ...editableBracket,
              }
              rawBracket.average_rate = editableBracket.rate
              delete rawBracket.rate
              return rawBracket
            },
          )
          delete metadata.type
          break
        }
        case ScaleType.MarginalAmount: {
          delete metadata.type
          break
        }
        case ScaleType.MarginalRate: {
          delete metadata.type
          break
        }
        case ScaleType.SingleAmount: {
          break
        }
      }
      break
    }
    case ParameterClass.Value: {
      delete metadata.type
      // if (editableParameter.values !== undefined) {
      //   for (const [instant, value] of Object.entries(
      //     editableParameter.values,
      //   )) {
      //     rawParameter[instant] = value
      //   }
      //   delete rawParameter.values
      // }
      break
    }
  }
  delete rawParameter.class

  const referencesByInstant = editableParameter.reference
  if (referencesByInstant !== undefined) {
    const newReferencesByInstant = Object.fromEntries(
      Object.entries(referencesByInstant).map(([instant, references]) => [
        instant,
        references.length === 1 ? references[0] : references,
      ]),
    )
    const instants = Object.keys(newReferencesByInstant)
    metadata.reference =
      instants.length === 1 && instants[0] === "0001-01-01"
        ? newReferencesByInstant["0001-01-01"]
        : newReferencesByInstant
  }

  if (Object.keys(metadata).length > 0) {
    rawParameter.metadata = metadata
  }

  return rawParameter
}
