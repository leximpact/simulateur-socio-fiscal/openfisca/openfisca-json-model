import {
  Audit,
  auditArray,
  auditBoolean,
  auditChain,
  auditCleanArray,
  auditFunction,
  auditKeyValueDictionary,
  auditNoop,
  auditNumber,
  auditOptions,
  auditRequire,
  auditString,
  auditSwitch,
  auditTest,
  auditTrimString,
  auditUnique,
  type Auditor,
} from "@auditors/core"

import { auditDate } from "../periods"
import { auditReference, auditReferencesByDate } from "../references"
import { ParameterClass, ScaleType, ValueType } from "../../parameters"
import type { Unit } from "../../units"

export function auditEditableAmountBracket(units: Unit[]): Auditor {
  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "amount",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditDate, [
        auditEditableValueOrExpected(units, auditNumber),
        auditRequire,
      ]),
      auditRequire,
    )
    audit.attribute(
      data,
      "threshold",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditDate, [
        auditEditableValueOrExpected(units, auditNumber),
        auditRequire,
      ]),
      auditRequire,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditEditableParameter(
  units: Unit[],
  childrenId?: string[] | undefined,
): Auditor {
  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "class",
      true,
      errors,
      remainingKeys,
      auditOptions(Object.values(ParameterClass)),
    )
    for (const key of [
      "description",
      "label_en",
      "documentation",
      "file_path",
      "short_label",
      "short_label_en",
    ]) {
      audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
    }
    audit.attribute(
      data,
      "documentation_start",
      true,
      errors,
      remainingKeys,
      auditBoolean,
      auditFunction((value) => (value ? true : null)),
    )
    audit.attribute(
      data,
      "inflator",
      true,
      errors,
      remainingKeys,
      auditTrimString,
    )
    audit.attribute(
      data,
      "inflator_reference",
      true,
      errors,
      remainingKeys,
      auditReferencesByDate,
    )
    audit.attribute(
      data,
      "last_value_still_valid_on",
      true,
      errors,
      remainingKeys,
      auditDate,
    )
    audit.attribute(
      data,
      "name",
      true,
      errors,
      remainingKeys,
      auditTrimString,
      // auditTest((name) => {
      //   const ids = name.split(".")
      //   return ids[ids.length - 1].match(/^[\d]/) === null
      // }, "Last part of name must not start with a digit"),
    )
    audit.attribute(
      data,
      "notes",
      true,
      errors,
      remainingKeys,
      auditReferencesByDate,
    )
    audit.attribute(
      data,
      "official_journal_date",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditDate, [
        auditSwitch(auditDate, [
          auditString,
          auditFunction((text) => text.split(";")),
          auditArray(auditDate),
          // TODO: Return something other than a string?
          auditFunction((instants) => instants.join("; ")),
        ]),
        auditRequire,
      ]),
    )
    audit.attribute(
      data,
      "reference",
      true,
      errors,
      remainingKeys,
      auditReferencesByDate,
    )

    if (errors.class === undefined) {
      switch (data.class as ParameterClass) {
        case ParameterClass.Node: {
          audit.attribute(
            data,
            "children",
            true,
            errors,
            remainingKeys,
            auditKeyValueDictionary(auditString, [
              auditEditableParameter(units),
              auditRequire,
            ]),
            // auditRequire, // A node may have no child (especially unprocessed nodes).
          )
          audit.attribute(
            data,
            "order",
            true,
            errors,
            remainingKeys,
            auditCleanArray(
              auditString,
              childrenId === undefined
                ? errors.children === undefined && data.children !== undefined
                  ? auditOptions(
                      Object.keys(data.children as { [id: string]: unknown }),
                    )
                  : auditNoop
                : auditOptions(childrenId),
            ),
            auditUnique,
          )
          audit.attribute(
            data,
            "unit",
            true,
            errors,
            remainingKeys,
            auditEditableUnitName(units),
          )

          break
        }
        case ParameterClass.Scale: {
          // Used only for compatibility with "barèmes IPP".
          audit.attribute(
            data,
            "ipp_csv_id",
            true,
            errors,
            remainingKeys,
            auditSwitch(
              auditTrimString,
              auditKeyValueDictionary(
                auditOptions([
                  "amount",
                  "average_rate",
                  "base",
                  "rate",
                  "threshold",
                ]),
                [auditTrimString, auditRequire],
              ),
            ),
          )
          audit.attribute(
            data,
            "type",
            true,
            errors,
            remainingKeys,
            auditOptions(Object.values(ScaleType)),
            auditRequire,
          )

          const type = data.type as ScaleType
          audit.attribute(
            data,
            "brackets",
            true,
            errors,
            remainingKeys,
            auditArray(
              errors.type === undefined
                ? [ScaleType.MarginalAmount, ScaleType.SingleAmount].includes(
                    type,
                  )
                  ? auditEditableAmountBracket(units)
                  : auditEditableRateBracket(units)
                : auditNoop,
              auditRequire,
            ),
            auditRequire,
          )
          if (errors.type === undefined) {
            if (
              [ScaleType.MarginalAmount, ScaleType.SingleAmount].includes(type)
            ) {
              audit.attribute(
                data,
                "amount_unit",
                true,
                errors,
                remainingKeys,
                auditEditableUnitName(units),
              )
            } else {
              audit.attribute(
                data,
                "rate_unit",
                true,
                errors,
                remainingKeys,
                auditEditableUnitName(units),
              )
            }
            audit.attribute(
              data,
              "threshold_unit",
              true,
              errors,
              remainingKeys,
              auditEditableUnitName(units),
            )
          } else {
            for (const key of ["rate_unit", "threshold_unit", "unit"]) {
              audit.attribute(data, key, true, errors, remainingKeys, auditNoop)
            }
          }

          audit.attribute(
            // UK
            data,
            "period",
            true,
            errors,
            remainingKeys,
            auditString,
            auditOptions([
              "hour", // UK
              "month", // UK
              "week", // UK
              "year", // UK
            ]),
          )

          break
        }
        case ParameterClass.Value: {
          // Used only for compatibility "barèmes IPP".
          audit.attribute(
            data,
            "ipp_csv_id",
            true,
            errors,
            remainingKeys,
            auditTrimString,
          )
          audit.attribute(
            data,
            "type",
            true,
            errors,
            remainingKeys,
            auditOptions(Object.values(ValueType)),
          )
          audit.attribute(
            // UK
            data,
            "period",
            true,
            errors,
            remainingKeys,
            auditString,
            auditOptions([
              "hour", // UK
              "month", // UK
              "week", // UK
              "year", // UK
            ]),
          )
          audit.attribute(
            data,
            "unit",
            true,
            errors,
            remainingKeys,
            auditEditableUnitName(units),
          )
          const type = data.type as ValueType
          audit.attribute(
            data,
            "values",
            true,
            errors,
            remainingKeys,
            auditKeyValueDictionary(auditDate, [
              auditEditableValueOrExpected(
                units,
                errors.type === undefined
                  ? type === ValueType.Boolean
                    ? auditBoolean
                    : type === ValueType.Number
                      ? auditNumber
                      : type === ValueType.StringArray
                        ? auditArray(auditString)
                        : // type === ValueType.StringByString
                          auditKeyValueDictionary(auditString, auditString)
                  : auditNoop,
              ),
              auditRequire,
            ]),
            auditRequire,
          )

          // if (errors.values === undefined) {
          //   const validDates = new Set(
          //     Object.keys(data.values as { [date: string]: unknown }),
          //   )
          //   validDates.add("0001-01-01")

          //   if (errors.notes === undefined) {
          //     audit.attribute(
          //       data,
          //       "notes",
          //       true,
          //       errors,
          //       remainingKeys,
          //       auditKeyValueDictionary(
          //         auditTest(
          //           (date) => validDates.has(date),
          //           (date) => `Parameter has no value for date ${date}`,
          //         ),
          //         auditNoop,
          //       ),
          //     )
          //   }

          //   if (errors.official_journal_date === undefined) {
          //     audit.attribute(
          //       data,
          //       "official_journal_date",
          //       true,
          //       errors,
          //       remainingKeys,
          //       auditKeyValueDictionary(
          //         auditTest(
          //           (date) => validDates.has(date),
          //           (date) => `Parameter has no value for date ${date}`,
          //         ),
          //         auditNoop,
          //       ),
          //     )
          //   }

          //   if (errors.reference === undefined) {
          //     audit.attribute(
          //       data,
          //       "reference",
          //       true,
          //       errors,
          //       remainingKeys,
          //       auditKeyValueDictionary(
          //         auditTest(
          //           (date) => validDates.has(date),
          //           (date) => `Parameter has no value for date ${date}`,
          //         ),
          //         auditNoop,
          //       ),
          //     )
          //   }
          // }

          break
        }
      }
    }

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditEditableRateBracket(units: Unit[]): Auditor {
  return function (audit: Audit, dataUnknown: unknown): [unknown, unknown] {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "base",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditDate, [
        auditEditableValueOrExpected(units, auditNumber),
        auditRequire,
      ]),
    )
    audit.attribute(
      data,
      "rate",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(
        auditDate,
        auditEditableValueOrExpected(units, auditNumber),
      ),
      auditRequire,
    )
    audit.attribute(
      data,
      "threshold",
      true,
      errors,
      remainingKeys,
      auditKeyValueDictionary(auditDate, [
        auditEditableValueOrExpected(units, auditNumber),
        auditRequire,
      ]),
      auditRequire,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export function auditEditableUnitName(units: Unit[]): Auditor {
  const unitsName = units.map(({ name }) => name)
  return auditChain(auditString, auditOptions(unitsName))
}

export function auditEditableValue(
  units: Unit[],
  ...auditors: Auditor[]
): Auditor {
  return (audit: Audit, dataUnknown: unknown): [unknown, unknown] => {
    if (dataUnknown == null) {
      return [dataUnknown, null]
    }
    if (typeof dataUnknown !== "object") {
      return audit.unexpectedType(dataUnknown, "object")
    }

    const data: { [key: string]: unknown } = { ...dataUnknown }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "reference",
      true,
      errors,
      remainingKeys,
      auditCleanArray(auditReference),
    )
    audit.attribute(
      data,
      "unit",
      true,
      errors,
      remainingKeys,
      auditEditableUnitName(units),
    )
    audit.attribute(
      data,
      "value",
      false, // Keep null value.
      errors,
      remainingKeys,
      ...auditors,
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export const auditEditableValueOrExpected = (
  units: Unit[],
  ...auditors: Auditor[]
): Auditor =>
  auditSwitch(
    auditTest((value) => value === "expected"),
    auditEditableValue(units, ...auditors),
  )
