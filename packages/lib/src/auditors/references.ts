import {
  Audit,
  auditCleanArray,
  auditFunction,
  auditHttpUrl,
  auditKeyValueDictionary,
  auditTrimString,
} from "@auditors/core"

import { auditDate } from "./periods"

export function auditReference(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data: { [key: string]: unknown } = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "href",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    // Remove jsessionid from Légifrance URLs.
    auditFunction((url) => url.replace(/;jsessionid=[^\/?]*/, "")),
  )
  audit.attribute(data, "note", true, errors, remainingKeys, auditTrimString)
  audit.attribute(data, "title", true, errors, remainingKeys, auditTrimString)

  if (
    errors.href === undefined &&
    errors.note === undefined &&
    errors.title === undefined &&
    data.href == null &&
    data.note == null &&
    data.title == null
  ) {
    errors.href =
      errors.note =
      errors.title =
        "A reference object must contain a href and/or a note and/or a title"
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export const auditReferencesByDate = auditKeyValueDictionary(
  auditDate,
  auditCleanArray(auditReference),
)
