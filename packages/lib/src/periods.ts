export class Instant {
  constructor(
    public readonly year: number,
    public readonly month: number = 1,
    public readonly day: number = 1,
  ) {}

  get date() {
    return new Date(this.toString())
  }

  static fromString(value: string): Instant {
    const [year, month, day] = value.split("-")
    return new Instant(
      parseInt(year),
      month === undefined ? 1 : parseInt(month),
      day === undefined ? 1 : parseInt(day),
    )
  }

  /// Shift the given instant from `offset` units.
  offset(offset: "first-of" | "last-of" | number, unit: PeriodUnit): Instant {
    let { year, month, day } = this
    if (offset === "first-of") {
      if (unit === "month") {
        day = 1
      } else if (unit === "year") {
        month = 1
        day = 1
      }
    } else if (offset === "last-of") {
      if (unit === "month") {
        day = getMonthLastDay(year, month)
      } else if (unit === "year") {
        month = 12
        day = 31
      }
    } else if (unit === "day") {
      day += offset
      if (offset < 0) {
        while (day < 1) {
          month -= 1
          if (month === 0) {
            year -= 1
            month = 12
            day += getMonthLastDay(year, month)
          }
        }
      } else if (offset > 0) {
        let monthLastDay = getMonthLastDay(year, month)
        while (day > monthLastDay) {
          month += 1
          if (month === 13) {
            year += 1
            month = 1
          }
          day -= monthLastDay
          monthLastDay = getMonthLastDay(year, month)
        }
      }
    } else if (unit === "month") {
      month += offset
      if (offset < 0) {
        while (month < 1) {
          year -= 1
          month += 12
        }
      } else if (offset > 0) {
        while (month > 12) {
          year += 1
          month -= 12
        }
      }
      const monthLastDay = getMonthLastDay(year, month)
      if (day > monthLastDay) {
        day = monthLastDay
      }
    } else if (unit === "year") {
      year += offset
      // # Handle february month of leap year.
      const monthLastDay = getMonthLastDay(year, month)
      if (day > monthLastDay) {
        day = monthLastDay
      }
    }
    return new Instant(year, month, day)
  }

  /// Create a new period starting at instant.
  period(unit: PeriodUnit, size?: number): Period {
    return new Period(unit, this, size)
  }

  toString(): string {
    return `${this.year.toString()}-${this.month
      .toString()
      .padStart(2, "0")}-${this.day.toString().padStart(2, "0")}`
  }
}

export class Period {
  constructor(
    public readonly unit: PeriodUnit,
    public readonly start: Instant,
    public readonly size: number = 1,
  ) {}

  get days(): number {
    return (
      Math.round(
        (this.stop.date.getTime() - this.start.date.getTime()) /
          (1000 * 60 * 60 * 24),
      ) + 1
    )
  }

  get first_day(): Period {
    return this.start.period("day")
  }

  get first_month(): Period {
    return this.start.offset("first-of", "month").period("month")
  }

  static fromString(value: string): Period {
    const match = value.match(periodRegExp)!
    const [, , unit, start, monthAndDaySuffix, daySuffix, , size] = match
    return new Period(
      unit === undefined
        ? monthAndDaySuffix === undefined
          ? "year"
          : daySuffix === undefined
            ? "month"
            : "day"
        : (unit as PeriodUnit),
      Instant.fromString(start),
      size === undefined ? 1 : parseInt(size),
    )
  }

  get last_3_months(): Period {
    return this.first_month.start.period("month", 3).offset(-3)
  }

  get last_month(): Period {
    return this.first_month.offset(-1)
  }

  get last_year(): Period {
    return this.start.offset("first-of", "year").period("year").offset(-1)
  }

  get n_2(): Period {
    return this.start.offset("first-of", "year").period("year").offset(-2)
  }

  /// Shift the given period from `offset` units.
  offset(offset: "first-of" | "last-of" | number, unit?: PeriodUnit): Period {
    return new Period(
      this.unit,
      this.start.offset(offset, unit === undefined ? this.unit : unit),
      this.size,
    )
  }

  get stop(): Instant {
    const { unit, start, size } = this
    let { year, month, day } = start

    if (unit === "eternity") {
      return new Instant(9999, 12, 31)
    }
    if (unit === "day") {
      if (size > 1) {
        day += size - 1
        let monthLastDay = getMonthLastDay(year, month)
        while (day > monthLastDay) {
          month += 1
          if (month === 13) {
            year += 1
            month = 1
          }
          day -= monthLastDay
          monthLastDay = getMonthLastDay(year, month)
        }
      }
    } else {
      if (unit === "month") {
        month += size
        while (month > 12) {
          year += 1
          month -= 12
        }
      } else {
        console.assert(unit === "year", `Invalid unit: "${unit}"`)
        year += size
      }
      day -= 1
      if (day < 1) {
        month -= 1
        if (month === 0) {
          year -= 1
          month = 12
        }
        day += getMonthLastDay(year, month)
      } else {
        const monthLastDay = getMonthLastDay(year, month)
        if (day > monthLastDay) {
          month += 1
          if (month === 13) {
            year += 1
            month = 1
          }
          day -= monthLastDay
        }
      }
    }
    return new Instant(year, month, day)
  }

  get this_year(): Period {
    return this.start.offset("first-of", "year").period("year")
  }

  toString(): string {
    const { unit, start, size } = this
    if (unit === "eternity") {
      return "ETERNITY"
    }
    const { year, month, day } = start

    // 1 year long period
    if ((unit === "month" && size === 12) || (unit === "year" && size === 1)) {
      return month === 1
        ? //  civil year starting from january
          year.toString()
        : // rolling year
          `year:${year.toString()}-${month.toString().padStart(2, "0")}`
    }

    // simple month
    if (unit === "month" && size === 1) {
      return `${year.toString()}-${month.toString().padStart(2, "0")}`
    }

    // several civil years
    if (unit === "year" && month === 1) {
      return `${unit}:${year.toString()}:${size}`
    }

    if (unit === "day") {
      return size === 1
        ? `${year.toString()}-${month.toString().padStart(2, "0")}-${day
            .toString()
            .padStart(2, "0")}`
        : `${unit}:${year.toString()}-${month.toString().padStart(2, "0")}-${day
            .toString()
            .padStart(2, "0")}:${size}`
    }

    // complex monthly period
    return `${unit}:${year.toString()}-${month
      .toString()
      .padStart(2, "0")}:${size}`
  }
}

export type PeriodUnit = "day" | "eternity" | "month" | "year"

export const dateRegExp =
  /^(0001-01-01|[12]\d{3}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01]))$/
export const instantRegExp =
  /^(0001-01-01|[12]\d{3}(-(0[1-9]|1[012])(-(0[1-9]|[12]\d|3[01]))?)?)$/
const periodRegExp =
  /^((day|month|year):)?(\d{4}(-\d{1,2}(-\d{1,2})?)?)(:(\d+))?$/

function getMonthLastDay(year: number, month: number): number {
  return new Date(year, month /* - 1 + 1 */, 0).getDate()
}

export function sortValueByInstantDescending<T>(valueByInstant: {
  [instant: string]: T
}): [string, T][] {
  return Object.entries(valueByInstant).sort(([instant1], [instant2]) =>
    instant2.localeCompare(instant1),
  )
}
