export interface ChangingUnit extends UnitBase {
  units: { [date: string]: ConstantUnit }
}

export interface ConstantUnit extends UnitBase {
  /// Name of parameter containing the values of the unit
  parameter?: string
  /// Can this unit be expressed as a percentage?
  ratio?: boolean
  units?: undefined
}

// See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/PluralRules/select#return_value
export const pluralizationCategories = [
  "zero",
  "one",
  "two",
  "few",
  "many",
  "other",
] as const
type PluralizationCategoriesTuple = typeof pluralizationCategories
export type PluralizationCategory = PluralizationCategoriesTuple[number]

export type StringByPluralizationCategory = {
  [Property in PluralizationCategory]?: string
}

export type Unit = ChangingUnit | ConstantUnit

export interface UnitBase {
  label?: StringByPluralizationCategory | string
  name: string
  short_label?: StringByPluralizationCategory | string
  units?: { [date: string]: ConstantUnit } | undefined
}

export function getUnitAtDate(
  unitByName: { [name: string]: Unit },
  name: string | undefined | null,
  date: string | undefined | null,
): Unit | undefined {
  if (name == null) {
    return undefined
  }
  let unit: Unit | undefined = unitByName[name]
  if (unit === undefined) {
    return undefined
  }
  if (date == null) {
    return unit
  }
  if (unit.units !== undefined) {
    unit = Object.entries(unit.units)
      .sort(([startDate1], [startDate2]) =>
        startDate2.localeCompare(startDate1),
      )
      .find(([startDate]) => startDate <= date)?.[1]
    if (unit === undefined) {
      return undefined
    }
  }
  return unit
}

export function getUnitLabel(
  unitByName: { [name: string]: Unit },
  name: string | undefined | null,
  date: string | undefined | null,
  pluralizationCategory: PluralizationCategory,
): string {
  const unit = getUnitAtDate(unitByName, name, date)
  if (unit === undefined) {
    return name ?? ""
  }
  return labelFromUnit(unit, pluralizationCategory)
}

export function getUnitShortLabel(
  unitByName: { [name: string]: Unit },
  name: string | undefined | null,
  date: string | undefined | null,
  pluralizationCategory: PluralizationCategory,
): string {
  const unit = getUnitAtDate(unitByName, name, date)
  if (unit === undefined) {
    return name ?? ""
  }
  return shortLabelFromUnit(unit, pluralizationCategory)
}

export function labelFromUnit(
  unit: Unit,
  pluralizationCategory: PluralizationCategory,
): string {
  const label = unit.label
  return label === undefined
    ? unit.name
    : typeof label === "string"
      ? label
      : (label[pluralizationCategory] ?? unit.name)
}

export function shortLabelFromUnit(
  unit: Unit,
  pluralizationCategory: PluralizationCategory,
): string {
  const shortLabel = unit.short_label
  return shortLabel === undefined
    ? labelFromUnit(unit, pluralizationCategory)
    : typeof shortLabel === "string"
      ? shortLabel
      : (shortLabel[pluralizationCategory] ??
        labelFromUnit(unit, pluralizationCategory))
}
