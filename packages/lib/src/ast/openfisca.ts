import type { EntityByKey } from "../entities"
import { accessParameterFromIds, type NodeParameter } from "../parameters"
import type { Variable } from "../variables"

import {
  OpenfiscaAstCogCall,
  OpenfiscaAstCogCallOperation,
  OpenfiscaAstInstantOrPeriodExpression,
  OpenfiscaAstPeriod,
  PythonAstConstant,
  PythonAstKeyword,
  PythonAstNode,
  PythonAstSubscript,
  type OpenfiscaAstCog,
  type OpenfiscaAstFormula,
  type OpenfiscaAstNode,
  type PythonAstAssign,
  type PythonAstAttribute,
  type PythonAstAugAssign,
  type PythonAstBinaryOperator,
  type PythonAstCall,
  type PythonAstFunctionDef,
  type PythonAstName,
  type PythonAstReturn,
} from "./nodes"
import { AstTransformer } from "./visitors"

/// Generate an OpenFisca abstract tree from a Python Abstract syntax tree.
export class OpenfiscaAstFormulaBuilder extends AstTransformer<
  PythonAstNode,
  OpenfiscaAstNode
> {
  public readonly cogByName: { [name: string]: OpenfiscaAstCog } = {}
  public readonly globalVariableValueByName: {
    [name: string]: {
      input: PythonAstNode
      output: OpenfiscaAstNode
      error?: unknown
    }
  } = {
    ADD: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "openfisca_france.ADD",
      },
      output: { ast_class: "built_in", name: "ADD" },
    },
    Famille: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "openfisca_france.Famille",
      },
      output: { ast_class: "built_in", name: "Famille" },
    },
    FoyerFiscal: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "openfisca_france.FoyerFiscal",
      },
      output: { ast_class: "built_in", name: "FoyerFiscal" },
    },
    max_: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "numpy.maximum",
      },
      output: { ast_class: "built_in", name: "max" },
    },
    Menage: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "openfisca_france.Menage",
      },
      output: { ast_class: "built_in", name: "Menage" },
    },
    min_: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "numpy.minimum",
      },
      output: { ast_class: "built_in", name: "min" },
    },
    not_: {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "numpy.logical_not",
      },
      output: { ast_class: "built_in", name: "not" },
    },
  }
  public readonly personEntityKey: string
  public returnCount = 0
  /// Value of the latest return statement found in formula
  public returnValue:
    | {
        input: PythonAstNode
        output: OpenfiscaAstNode
        error?: unknown
      }
    | undefined = undefined
  public readonly variableValueByName: {
    [name: string]: {
      input: PythonAstNode
      output: OpenfiscaAstNode
      error?: unknown
    }
  } = {}

  constructor(
    public readonly variable: Variable,
    functionDef: PythonAstFunctionDef,
    public readonly entityByKey: EntityByKey,
    public readonly rootParameter: NodeParameter,
    public readonly variableSummaryByName: { [name: string]: Variable },
  ) {
    super()

    const entityKey = variable.entity
    this.personEntityKey = Object.entries(entityByKey)
      .filter(([, entity]) => entity.is_person)
      .map(([key]) => key)[0]

    const { args } = functionDef.args
    console.assert(
      args.length >= 2 && args.length <= 3,
      "Unexpected number of parameters in formula",
    )

    console.assert(
      args[0].arg === entityKey,
      `Invalid name "${args[0].arg}" for formula entity parameter`,
    )
    console.assert(
      entityByKey[entityKey] !== undefined,
      `Invalid name "${entityKey}" for formula entity parameter`,
    )
    this.variableValueByName[args[0].arg] = {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: entityKey,
      },
      output: {
        ast_class: "entity",
        key: entityKey,
      },
    }

    console.assert(
      args[1].arg === "period",
      `Invalid name "${args[1].arg}" for formula period parameter`,
    )
    this.variableValueByName[args[1].arg] = {
      input: {
        // Dummy input
        ast_class: "Name",
        ctx: { ast_class: "Load" },
        id: "period",
      },
      output: { ast_class: "period" },
    }

    if (args.length >= 3) {
      console.assert(
        args[2].arg === "parameters",
        `Invalid name for formula parameter argunemnt: ${args[1].arg}`,
      )
      this.variableValueByName[args[2].arg] = {
        input: {
          // Dummy input
          ast_class: "Name",
          ctx: { ast_class: "Load" },
          id: "parameters",
        },
        output: {
          ast_class: "parameter",
          ids: [],
        },
      }
    }
  }

  run(functionDef: PythonAstFunctionDef): OpenfiscaAstFormula {
    for (const instruction of functionDef.body) {
      this.transform(instruction)
    }
    const formula: OpenfiscaAstFormula = {
      ast_class: "formula",
      returnValue:
        this.returnCount === 1
          ? this.returnValue!
          : {
              input: { ast_class: "Constant", value: "ERROR" }, // TODO: Replace dummy value.
              output: { ast_class: "Constant", value: "ERROR" }, // TODO: Replace dummy value.
              error: "Formula doesn't return a value or return several values",
            },
      // variableValueByName: this.variableValueByName,
    }

    // const variableValueByNameError:{[name: string]: unknown} = {}
    // const variableValueByName: {[name: string]: OpenfiscaAstNode} = {}
    // for (const [name, variableValue] of Object.entries(this.variableValueByName)) {
    //   variableValueByName[name] = variableValue.output
    //   if (variableValue.error !== undefined) {
    //     variableValueByNameError[name] = variableValue.error
    //   }
    // }
    // if (Object.keys(variableValueByNameError).length !== 0) {
    //   error.variableValueByName = variableValueByNameError
    // }

    return formula
  }

  transform_Assign(assign: PythonAstAssign): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    const targets = this.reduceMapErrors(
      assign.targets.map((target) => this.transform(target as PythonAstNode)),
    )
    const value = this.transform(assign.value as PythonAstNode)
    for (const target of targets.output) {
      if (target.ast_class === "Name" && target.ctx.ast_class === "Store") {
        this.variableValueByName[target.id] = value
      }
    }
    return this.reduceErrorByKey(
      assign,
      { ...assign, targets: targets.output, value: value.output },
      {
        targets: targets.error,
        value: value.error,
      },
    )
  }

  transform_Attribute(attribute: PythonAstAttribute): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    const value = this.transform(attribute.value as PythonAstNode)
    const { output: valueOutput } = value
    switch (valueOutput.ast_class) {
      case "entity": {
        const entity = this.entityByKey[valueOutput.key]
        if (!entity.is_person && attribute.attr === "members") {
          return this.reduceErrorByKey(
            attribute,
            {
              ast_class: "entity",
              key: this.personEntityKey,
            },
            {
              value: value.error,
            },
          )
        }
        break
      }
      case "instant_or_period_expression": {
        return this.reduceErrorByKey(
          attribute,
          {
            ast_class: "instant_or_period_expression",
            expression: {
              ...attribute,
              value: valueOutput.expression,
            },
          },
          {
            value: value.error,
          },
        )
      }
      case "parameter_at_instant": {
        const ids = [...valueOutput.ids, attribute.attr]
        const [, attrError] = accessParameterFromIds(this.rootParameter, ids)
        const parameterAtInstant = {
          ...valueOutput,
          ids,
        }
        return this.reduceErrorByKey(attribute, parameterAtInstant, {
          attr: attrError,
          value: value.error,
        })
      }
      case "period": {
        return this.reduceErrorByKey(
          attribute,
          {
            ast_class: "instant_or_period_expression",
            expression: {
              ...attribute,
              value: valueOutput,
            },
          },
          {
            value: value.error,
          },
        )
      }
      default:
    }

    return this.reduceErrorByKey(
      attribute,
      { ...attribute, value: valueOutput },
      {
        value: value.error,
      },
    )
  }

  transform_AugAssign(augAssign: PythonAstAugAssign): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    // Replace augmented assign (+=, /=, etc) with a normal assign.
    return this.transform({
      ast_class: "Assign",
      targets: [augAssign.target],
      value: {
        ast_class: "BinOp",
        left: {
          ...augAssign.target,
          ctx: { ast_class: "Load" },
        },
        op: augAssign.op as PythonAstBinaryOperator,
        right: augAssign.value,
      },
    })
  }

  transform_Call(call: PythonAstCall): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    const args = this.reduceMapErrors(
      call.args.map((arg) => this.transform(arg as PythonAstNode)),
    )
    const func = this.transform(call.func as PythonAstNode)
    const { output: funcOutput } = func
    const keywords = this.reduceMapErrors(
      call.keywords.map((keyword) => this.transform(keyword as PythonAstNode)),
    ) as { input: PythonAstNode[]; output: PythonAstKeyword[]; error?: unknown }

    switch (funcOutput.ast_class) {
      case "entity": {
        const [variableNameOutput, periodOutput, operationOutput] = args.output
        if (variableNameOutput.ast_class === "Constant") {
          const variableName = variableNameOutput.value as string
          let cog = this.cogByName[variableName]
          if (cog === undefined) {
            const variableSummary = this.variableSummaryByName[variableName]
            cog = this.cogByName[variableName] = {
              ast_class: "cog",
              entity: this.entityByKey[variableSummary.entity],
              name: variableName,
              periodUnit: variableSummary.definition_period,
            }
          }
          const cogCall: OpenfiscaAstCogCall = {
            ast_class: "cog_call",
            cog,
            period: periodOutput as
              | OpenfiscaAstInstantOrPeriodExpression
              | OpenfiscaAstPeriod,
          }
          if (operationOutput === undefined) {
            if (cog.periodUnit !== this.variable.definition_period) {
              if (
                (
                  args.error as { [index: number]: unknown } | undefined
                )?.[2] === undefined
              ) {
                // Add an error if one doesn't exist yet.
                if (args.error === undefined) {
                  args.error = {}
                }
                ;(args.error as { [index: number]: unknown })[2] =
                  `Missing operation in cog call to convert result from ${cog.periodUnit} to ${this.variable.definition_period}`
              }
            }
          } else if (
            operationOutput.ast_class === "Constant" &&
            ["ADD", "DIVIDE"].includes(operationOutput.value as string)
          ) {
            cogCall.operation =
              operationOutput.value as OpenfiscaAstCogCallOperation
          } else if (
            (args.error as { [index: number]: unknown } | undefined)?.[2] ===
            undefined
          ) {
            // Unexpected operation for cog call
            // => Add an error if one doesn't exist yet.
            if (args.error === undefined) {
              args.error = {}
            }
            ;(args.error as { [index: number]: unknown })[2] =
              "Unexpected operation for cog call"
          }
          return this.reduceErrorByKey(call, cogCall, {
            args: args.error,
            func: func.error,
            keywords: keywords.error,
          })
        }
        break
      }
      case "instant_or_period_expression": {
        return this.reduceErrorByKey(
          call,
          {
            ast_class: "instant_or_period_expression",
            expression: {
              ...call,
              func: funcOutput.expression,
            },
          },
          {
            args: args.error,
            func: func.error,
            keywords: keywords.error,
          },
        )
      }
      case "parameter": {
        const [instantOutput] = args.output
        return this.reduceErrorByKey(
          call,
          {
            ast_class: "parameter_at_instant",
            ids: funcOutput.ids,
            instant: instantOutput,
          },
          {
            args: args.error,
            func: func.error,
            keywords: keywords.error,
          },
        )
      }
      default:
    }

    return this.reduceErrorByKey(
      call,
      {
        ...call,
        args: args.output,
        func: func.output,
        keywords: keywords.output,
      },
      {
        args: args.error,
        func: func.error,
        keywords: keywords.error,
      },
    )
  }

  transform_Name(name: PythonAstName): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    if (name.ctx.ast_class === "Load") {
      const variableValue =
        this.variableValueByName[name.id] ??
        this.globalVariableValueByName[name.id]
      return variableValue === undefined
        ? {
            input: name,
            output: name,
            error: { id: `Undefined variable: ${name.id}` },
          }
        : variableValue
    }
    return { input: name, output: name }
  }

  transform_Return(returnNode: PythonAstReturn): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    const value =
      returnNode.value === undefined
        ? {
            input: {
              // Dummy input
              ast_class: "Constant",
              // value: undefined,
            } as PythonAstConstant,
            output: {
              ast_class: "Constant",
              // value: undefined
            } as PythonAstConstant,
            error: "Missing return value",
          }
        : this.transform(returnNode.value as PythonAstNode)
    this.returnCount++
    this.returnValue = value
    return this.reduceErrorByKey(
      returnNode,
      { ...returnNode, value: value.output },
      { value: value.error },
    )
  }

  transform_Subscript(subscript: PythonAstSubscript): {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  } {
    const slice = this.transform(subscript.slice as PythonAstNode)
    const value = this.transform(subscript.value as PythonAstNode)
    return this.reduceErrorByKey(
      subscript,
      { ...subscript, slice: slice.output, value: value.output },
      {
        slice: slice.error,
        value: value.error,
      },
    )
  }
}

export function openfiscaAstFromFormulaPythonAst(
  variable: Variable,
  functionDef: PythonAstFunctionDef,
  entityByKey: EntityByKey,
  rootParameter: NodeParameter,
  variableSummaryByName: { [name: string]: Variable },
): OpenfiscaAstFormula {
  const openfiscaAstFormulaBuilder = new OpenfiscaAstFormulaBuilder(
    variable,
    functionDef,
    entityByKey,
    rootParameter,
    variableSummaryByName,
  )
  return openfiscaAstFormulaBuilder.run(functionDef)
}
