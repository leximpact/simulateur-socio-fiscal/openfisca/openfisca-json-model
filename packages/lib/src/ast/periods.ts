import { Instant, Period, PeriodUnit } from "../periods"

import type {
  OpenfiscaAstInstantOrPeriodExpression,
  OpenfiscaAstNode,
  OpenfiscaAstPeriod,
  PythonAstAttribute,
  PythonAstCall,
  PythonAstConstant,
  PythonAstKeyword,
  PythonAstUnaryOp,
} from "./nodes"
import { AstTransformer } from "./visitors"

class InstantOrPeriodMethod {
  constructor(
    public readonly instantOrPeriod: Instant | Period,
    public readonly methodName: string,
  ) {}
}

class InstantOrPeriodEvaluator extends AstTransformer<
  OpenfiscaAstNode,
  unknown
> {
  constructor(public readonly period: Period) {
    super()
  }

  transform_Attribute(attribute: PythonAstAttribute): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const value = this.transform(attribute.value)
    const { output: valueOutput } = value

    if (value.error === undefined) {
      if (valueOutput instanceof Instant) {
        switch (attribute.attr) {
          case "offset":
          case "period":
            // Method
            return {
              input: attribute,
              output: new InstantOrPeriodMethod(valueOutput, attribute.attr),
            }
          default: {
            return {
              input: attribute,
              output: valueOutput,
              error: `Unknown instant attribute: ${attribute.attr}`,
            }
          }
        }
      }
      if (valueOutput instanceof Period) {
        switch (attribute.attr) {
          case "days":
          case "first_month":
          case "last_3_months":
          case "last_month":
          case "last_year":
          case "n_2":
          case "start":
          case "this_year":
            // Simple attribute
            return {
              input: attribute,
              output: (valueOutput as unknown as { [attr: string]: unknown })[
                attribute.attr
              ],
            }
          case "offset":
            // Method
            return {
              input: attribute,
              output: new InstantOrPeriodMethod(valueOutput, attribute.attr),
            }
          default: {
            return {
              input: attribute,
              output: valueOutput,
              error: `Unknown period attribute: ${attribute.attr}`,
            }
          }
        }
      }
    }

    const result = this.reduceErrorByKey(
      attribute,
      {
        ...attribute,
        value: valueOutput,
      },
      {
        value: value.error,
      },
    )
    return result.error === undefined
      ? {
          ...result,
          error: "Unhandled Attribute in evaluation of instant or period",
        }
      : result
  }

  transform_Call(call: PythonAstCall): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const args = this.reduceMapErrors(
      call.args.map((arg) => this.transform(arg)),
    )
    const func = this.transform(call.func)
    const { output: funcOutput } = func
    const keywords = this.reduceMapErrors(
      call.keywords.map((keyword) => this.transform(keyword)),
    ) as {
      input: OpenfiscaAstNode[]
      output: PythonAstKeyword[]
      error?: unknown
    }

    if (
      args.error === undefined &&
      func.error === undefined &&
      keywords.error === undefined
    ) {
      if (funcOutput instanceof InstantOrPeriodMethod) {
        const { instantOrPeriod, methodName } = funcOutput
        if (instantOrPeriod instanceof Instant) {
          switch (methodName) {
            case "offset": {
              console.assert(
                keywords.output.length === 0,
                "Unexpected keyword argument for Instant.offset() method",
              )
              return {
                input: call,
                output: instantOrPeriod.offset(
                  ...(args.output as [
                    "first-of" | "last-of" | number,
                    PeriodUnit,
                  ]),
                ),
              }
            }
            case "period": {
              console.assert(
                keywords.output.length === 0,
                "Unexpected keyword argument for Instant.period() method",
              )
              return {
                input: call,
                output: instantOrPeriod.period(
                  ...(args.output as [PeriodUnit, number]),
                ),
              }
            }
            default: {
              return {
                input: call,
                output: instantOrPeriod,
                error: `Unknown instant method: ${methodName}`,
              }
            }
          }
        }
        if (instantOrPeriod instanceof Period) {
          switch (methodName) {
            case "offset": {
              console.assert(
                keywords.output.length === 0,
                "Unexpected keyword argument for Period.offset() method",
              )
              return {
                input: call,
                output: instantOrPeriod.offset(
                  ...(args.output as [
                    "first-of" | "last-of" | number,
                    PeriodUnit | undefined,
                  ]),
                ),
              }
            }
            default: {
              return {
                input: call,
                output: instantOrPeriod,
                error: `Unknown period method: ${methodName}`,
              }
            }
          }
        }
      }
    }

    const result = this.reduceErrorByKey(
      call,
      {
        ...call,
        args: args.output,
        func: funcOutput,
        keywords: keywords.output,
      },
      {
        args: args.error,
        func: func.error,
        keywords: keywords.error,
      },
    )
    return result.error === undefined
      ? {
          ...result,
          error: "Unhandled Call in evaluation of instant or period",
        }
      : result
  }

  transform_Constant(constant: PythonAstConstant): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return { input: constant, output: constant.value }
  }

  transform_instant_or_period_expression(
    instantOrPeriodExpression: OpenfiscaAstInstantOrPeriodExpression,
  ): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const expression = this.transform(instantOrPeriodExpression.expression)
    return this.reduceErrorByKey(instantOrPeriodExpression, expression.output, {
      expression: expression.error,
    })
  }

  transform_period(period: OpenfiscaAstPeriod): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return { input: period, output: this.period }
  }

  transform_UnaryOp(unaryOp: PythonAstUnaryOp): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const operand = this.transform(unaryOp.operand)
    if (operand.error === undefined) {
      switch (unaryOp.op.ast_class) {
        case "Invert":
        case "Not":
          break
        case "UAdd":
          return { input: unaryOp, output: operand.output }
        case "USub":
          return { input: unaryOp, output: -(operand.output as number) }
        default:
          assertNever("unary operator", unaryOp.op)
      }
    }

    const result = this.reduceErrorByKey(
      unaryOp,
      {
        ...unaryOp,
        operand: operand.output,
      },
      {
        operand: operand.error,
      },
    )
    return result.error === undefined
      ? {
          ...result,
          error: "Unhandled UnaryOp in evaluation of instant or period",
        }
      : result
  }

  transformGenericOutputByKey(
    input: OpenfiscaAstNode,
    _outputByKey: {
      [key: string]: unknown
    },
    _errorByKey: {
      [key: string]: unknown
    },
  ): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return {
      input,
      output: new Instant(9999, 12, 31),
      error: `Missing instant or period transformer for ${input.ast_class}`,
    }
  }
}

function assertNever(type: string, value: never): never {
  throw new Error(`Unexpected ${type}: ${JSON.stringify(value)}`)
}

export function evaluateOpenfiscaAstInstantOrPeriod(
  instantOrPeriod: OpenfiscaAstNode,
  period: Period,
): {
  input: OpenfiscaAstNode
  output: Instant | Period | number
  error?: unknown
} {
  const instantOrPeriodEvaluator = new InstantOrPeriodEvaluator(period)
  const instantOrPeriodResult =
    instantOrPeriodEvaluator.transform(instantOrPeriod)
  if (instantOrPeriodResult.output instanceof InstantOrPeriodMethod) {
    // Error: Result output is a method instead of a instant or a period.
    return {
      input: instantOrPeriodResult.input,
      output: instantOrPeriodResult.output.instantOrPeriod,
      error:
        instantOrPeriodResult.error ??
        "Evaluation of instant or period expression is a method instead of a value",
    }
  }
  if (
    !(instantOrPeriodResult.output instanceof Instant) &&
    !(instantOrPeriodResult.output instanceof Period) &&
    !Number.isInteger(instantOrPeriodResult.output)
  ) {
    // Error: Unexpected result output.
    return {
      input: instantOrPeriodResult.input,
      output: new Instant(9999, 12, 31),
      error:
        instantOrPeriodResult.error ??
        "Unexpected result for evaluation of instant or period",
    }
  }
  return instantOrPeriodResult as {
    input: OpenfiscaAstNode
    output: Instant | Period
    error?: unknown
  }
}
