import type { DecompositionReference } from "../decompositions"
import type { EntityByKey } from "../entities"

import {
  type PythonAstAssign,
  type PythonAstNode,
  type PythonAstAttribute,
  type PythonAstAugAssign,
  type PythonAstBinaryOperator,
  type PythonAstCall,
  type PythonAstConstant,
  type PythonAstDelete,
  type PythonAstFor,
  type PythonAstFunctionDef,
  type PythonAstGeneratorExp,
  type PythonAstListComp,
  type PythonAstName,
  type PythonAstReturn,
  type PythonAstSetComp,
  type PythonAstSubscript,
} from "./nodes"
import { AstVisitor } from "./visitors"

type VariableDescription =
  | VariableDescriptionBoolean
  | VariableDescriptionDecomposition
  | VariableDescriptionOpenFiscaParameter
  | VariableDescriptionOpenFiscaVariable
  | VariableDescriptionOpenFiscaVariableList
  | VariableDescriptionString
  | VariableDescriptionStringListOrTuple

interface VariableDescriptionBase {
  type: VariableType
}

/// Description stating that the variable is either a boolean or a vector of booleans.
interface VariableDescriptionBoolean extends VariableDescriptionBase {
  type: VariableType.Boolean
}

interface VariableDescriptionDecomposition extends VariableDescriptionBase {
  /// A list of names of OpenFisca variables that are added to generate the decomposition.
  /// Note: There is no such thing as a decomposition with only one variable name, because
  /// it is an OpenFisca variable, not a decomposition.
  decomposition: DecompositionReference[]
  type: VariableType.Decomposition
}

interface VariableDescriptionOpenFiscaParameter
  extends VariableDescriptionBase {
  /// PythonAstName of OpenFisca parameter
  name: string
  type: VariableType.OpenFiscaParameter
}

interface VariableDescriptionOpenFiscaVariable extends VariableDescriptionBase {
  /// PythonAstName of OpenFisca variable
  name: string
  type: VariableType.OpenFiscaVariable
}

interface VariableDescriptionOpenFiscaVariableList
  extends VariableDescriptionBase {
  /// Names of OpenFisca variables
  names: string[]
  type: VariableType.OpenFiscaVariableList
}

interface VariableDescriptionString extends VariableDescriptionBase {
  value: string
  type: VariableType.String
}

interface VariableDescriptionStringListOrTuple extends VariableDescriptionBase {
  items: string[]
  type: VariableType.StringListOrTuple
}

enum VariableType {
  Boolean = "Boolean",
  Decomposition = "Decomposition",
  OpenFiscaParameter = "OpenFiscaParameter",
  OpenFiscaVariable = "OpenFiscaVariable",
  OpenFiscaVariableList = "OpenFiscaVariableList",
  String = "String",
  StringListOrTuple = "StringListOrTuple",
}

class FormulaExtractor extends AstVisitor<PythonAstNode> {
  /// PythonAstList of names of OpenFisca variables that are added to generate the result of the formula.
  decomposition: DecompositionReference[] | undefined = undefined
  /// Names of parameters used by formula
  openFiscaParametersName: Set<string> = new Set()
  /// Names of variables used by formula
  openFiscaVariablesName: Set<string> = new Set()
  singlePersonRolesAndSubroles: Set<string>
  /// Descriptions of local variables created by formula
  variableDescriptionById: { [id: string]: VariableDescription | null } = {} // `null` means unknown.

  constructor(
    public readonly entityByKey: EntityByKey,
    public readonly leafParametersName: Set<string>,
  ) {
    super()

    const singlePersonRolesAndSubroles = new Set<string>()
    for (const entity of Object.values(entityByKey)) {
      if (entity.is_person) {
        continue
      }
      for (const role of entity.roles) {
        if (role.max === 1) {
          singlePersonRolesAndSubroles.add(role.key)
        }
        for (const subrole of role.subroles ?? []) {
          if (subrole.max === 1) {
            singlePersonRolesAndSubroles.add(subrole.key)
          }
        }
      }
    }
    this.singlePersonRolesAndSubroles = singlePersonRolesAndSubroles
  }

  extractDecomposition(node: PythonAstNode): DecompositionReference[] | null {
    switchNodeAstClass: switch (node.ast_class) {
      case "BinOp": {
        switch (node.op.ast_class) {
          case "Add": {
            const leftDecomposition = this.extractDecomposition(node.left)
            if (leftDecomposition !== null) {
              const rightDecomposition = this.extractDecomposition(node.right)
              if (rightDecomposition !== null) {
                return [...leftDecomposition, ...rightDecomposition]
              }
            }
            break switchNodeAstClass
          }
          case "Sub": {
            const leftDecomposition = this.extractDecomposition(node.left)
            if (leftDecomposition !== null) {
              const rightDecomposition = this.extractDecomposition(node.right)
              if (rightDecomposition !== null) {
                return [
                  ...leftDecomposition,
                  ...rightDecomposition.map((decompositionReference) => {
                    decompositionReference = { ...decompositionReference }
                    if (decompositionReference.negate) {
                      delete decompositionReference.negate
                    } else {
                      decompositionReference.negate = true
                    }
                    return decompositionReference
                  }),
                ]
              }
            }
            break switchNodeAstClass
          }
          case "Mult": {
            const leftDecomposition = this.extractDecomposition(node.left)
            if (leftDecomposition !== null && this.isBoolean(node.right)) {
              return leftDecomposition
            }
            const rightDecomposition = this.extractDecomposition(node.right)
            if (rightDecomposition !== null && this.isBoolean(node.left)) {
              return rightDecomposition
            }
            break switchNodeAstClass
          }
          default:
            break switchNodeAstClass
        }
      }
      case "Call": {
        const func = node.func
        switch (func.ast_class) {
          case "Name": {
            // US
            if (func.id === "add") {
              // Example: add(tax_unit, period, "e00900p", "e02100p", "k1bx14p")
              console.assert(func.ctx.ast_class === "Load")
              const variablesName = node.args
                .slice(2)
                .map((arg) => this.extractString(arg as PythonAstNode))
              if (
                variablesName.every((variableName) => variableName !== null)
              ) {
                return (variablesName as string[]).map((name) => ({ name }))
              }
              // Don't break, to test for UK form of "add" below.
              // break switchNodeAstClass
            }
            // UK
            if (["add", "aggr"].includes(func.id)) {
              // Example: add(person, period, ["savings_allowance", "savings_starter_rate_income"])
              console.assert(func.ctx.ast_class === "Load")
              const variablesName = this.extractStringList(
                node.args[2] as PythonAstNode,
              )
              if (variablesName !== null) {
                return variablesName.map((name) => ({ name }))
              }
              console.log("TODO: Unrecognized variable call in formula:", node)
              console.log("node.args", node.args)
              break switchNodeAstClass
            } else if (["around", "round", "round_"].includes(func.id)) {
              // Example: around(ir_plaf_qf - decote_gain_fiscal)
              console.assert(func.ctx.ast_class === "Load")
              const decomposition = this.extractDecomposition(
                node.args[0] as PythonAstNode,
              )
              if (decomposition !== null) {
                return decomposition
              }
              break switchNodeAstClass
            }
            break switchNodeAstClass
          }
          default:
            break switchNodeAstClass
        }
      }
      case "Name": {
        if (node.ctx.ast_class === "Load") {
          const variableDescription = this.variableDescriptionById[node.id]
          if (variableDescription?.type === VariableType.Decomposition) {
            return variableDescription.decomposition
          }
        }
        break switchNodeAstClass
      }
      case "UnaryOp": {
        switch (node.op.ast_class) {
          case "USub": {
            const decomposition = this.extractDecomposition(node.operand)
            if (decomposition !== null) {
              return decomposition.map((decompositionReference) => {
                decompositionReference = { ...decompositionReference }
                if (decompositionReference.negate) {
                  delete decompositionReference.negate
                } else {
                  decompositionReference.negate = true
                }
                return decompositionReference
              })
            }
            break switchNodeAstClass
          }
          default:
            break switchNodeAstClass
        }
      }
    }

    const openFiscaVariablesName = this.extractOpenFiscaVariablesName(node)
    if (
      openFiscaVariablesName !== null &&
      openFiscaVariablesName.length === 1
    ) {
      return openFiscaVariablesName.map((name) => ({ name }))
    }

    return null
  }

  extractOpenFiscaParameterName(node: PythonAstNode): string | null {
    switch (node.ast_class) {
      case "Attribute":
        return this.extractOpenFiscaParameterNameFromAttribute(node)
      case "Call":
        return this.extractOpenFiscaParameterNameFromCall(node)
      case "Name": {
        if (node.ctx.ast_class === "Load") {
          const variableDescription = this.variableDescriptionById[node.id]
          if (variableDescription?.type === VariableType.OpenFiscaParameter) {
            return variableDescription.name
          }
        }
        return null
      }
      case "Subscript":
        return this.extractOpenFiscaParameterNameFromSubscript(node)
      default:
        return null
    }
  }

  extractOpenFiscaParameterNameFromAttribute(
    node: PythonAstAttribute,
  ): string | null {
    const parameterName = this.extractOpenFiscaParameterName(
      node.value as PythonAstNode,
    )
    if (parameterName === null) {
      return null
    }
    if (this.leafParametersName.has(parameterName)) {
      // PythonAstAttribute `node.attr` is a property or method of parameter, not a child.
      return parameterName
    }
    if (node.attr === "_children") {
      // Parameter is not a leaf, but its children nodes are accessed using an
      // alternate syntax.
      return parameterName
    }
    return [...parameterName.split(".").filter(Boolean), node.attr].join(".")
  }

  extractOpenFiscaParameterNameFromCall(node: PythonAstCall): string | null {
    const func = node.func
    if (func.ast_class === "Name" && func.id === "parameters") {
      return ""
    }
    return null
  }

  extractOpenFiscaParameterNameFromSubscript(
    node: PythonAstSubscript,
  ): string | null {
    const parameterName = this.extractOpenFiscaParameterName(
      node.value as PythonAstNode,
    )
    if (parameterName === null) {
      return null
    }
    if (
      !this.leafParametersName.has(parameterName) &&
      node.ctx.ast_class === "Load" &&
      node.slice.ast_class === "Constant" &&
      typeof node.slice.value === "string"
    ) {
      return [
        ...parameterName.split(".").filter(Boolean),
        node.slice.value,
      ].join(".")
    }
    // PythonAstSubscript is not a child.
    return parameterName
  }

  extractOpenFiscaVariablesName(node: PythonAstNode): string[] | null {
    switch (node.ast_class) {
      case "Call": {
        return this.extractOpenFiscaVariablesNameFromCall(node)
      }
      case "Name": {
        if (node.ctx.ast_class === "Load") {
          const variableDescription = this.variableDescriptionById[node.id]
          if (variableDescription?.type === VariableType.OpenFiscaVariable) {
            return [variableDescription.name]
          }
          if (
            variableDescription?.type === VariableType.OpenFiscaVariableList
          ) {
            return variableDescription.names
          }
        }
        return null
      }
      default:
        return null
    }
  }

  extractOpenFiscaVariablesNameFromCall(node: PythonAstCall): string[] | null {
    const func = node.func
    switch (func.ast_class) {
      case "Attribute": {
        const value = func.value
        const attr = func.attr
        switch (value.ast_class) {
          case "Attribute": {
            const value2 = value.value
            const attr2 = value.attr
            switch (value2.ast_class) {
              case "Name": {
                const value2Entity = this.entityByKey[value2.id]
                const attrEntity = this.entityByKey[attr]
                if (
                  value2Entity !== undefined &&
                  !value2Entity.is_person &&
                  (attr2 === "members" ||
                    this.singlePersonRolesAndSubroles.has(attr2)) &&
                  attrEntity !== undefined &&
                  !attrEntity.is_person
                ) {
                  // Example:
                  // * menage.members.famille('prestations_sociales', period)
                  // * famille.demandeur.menage('logement_conventionne', period)
                  console.assert(value2.ctx.ast_class === "Load")
                  console.assert(value.ctx.ast_class === "Load")
                  const variableName = this.extractString(
                    node.args[0] as PythonAstNode,
                  )
                  if (variableName !== null) {
                    return [variableName]
                  }
                  console.log(
                    "TODO: Unrecognized variable call in formula:",
                    node,
                  )
                  return null
                }

                return null
              }
              default:
                return null
            }

            return null
          }
          case "Name": {
            const valueEntity = this.entityByKey[value.id]
            const attrEntity = this.entityByKey[attr]
            if (
              valueEntity !== undefined &&
              valueEntity.is_person &&
              attrEntity !== undefined &&
              !attrEntity.is_person
            ) {
              // Example: individu.foyer_fiscal('csg_revenus_capital', period)
              console.assert(value.ctx.ast_class === "Load")
              const variableName = this.extractString(
                node.args[0] as PythonAstNode,
              )
              if (variableName !== null) {
                return [variableName]
              }
              console.log("TODO: Unrecognized variable call in formula:", node)
              return null
            }

            if (valueEntity !== undefined && !valueEntity.is_person) {
              if (
                attr === "members" ||
                this.singlePersonRolesAndSubroles.has(attr)
              ) {
                // Example:
                // * menage.members('pensions_nettes', period)
                // * famille.demandeur('age', period)
                console.assert(value.ctx.ast_class === "Load")
                const variableName = this.extractString(
                  node.args[0] as PythonAstNode,
                )
                if (variableName !== null) {
                  return [variableName]
                }
                console.log(
                  "TODO: Unrecognized variable call in formula:",
                  node,
                )
                return null
              }
              if (attr === "sum") {
                console.assert(value.ctx.ast_class === "Load")
                const openFiscaVariablesName =
                  this.extractOpenFiscaVariablesName(
                    node.args[0] as PythonAstNode,
                  )
                if (openFiscaVariablesName !== null) {
                  // Example: menage.sum(menage.members('pensions_nettes', period))
                  console.assert(openFiscaVariablesName.length === 1)
                  return openFiscaVariablesName
                }
                const decomposition = this.extractDecomposition(
                  node.args[0] as PythonAstNode,
                )
                if (decomposition !== null) {
                  // Example:
                  //   prestations_sociales_i = menage.members.famille(
                  //       "prestations_sociales", period
                  //   ) * menage.members.has_role(Famille.DEMANDEUR)
                  //   prestations_sociales = menage.sum(prestations_sociales_i)
                  // because prestations_sociales_i is a decomposition.
                  console.assert(decomposition.length === 1)
                  return [decomposition[0].name]
                }
                return null
              }
            }

            return null
          }
          default:
            return null
        }
      }
      case "Name": {
        // US
        if (func.id === "add") {
          // Example: add(tax_unit, period, "e00900p", "e02100p", "k1bx14p")
          console.assert(func.ctx.ast_class === "Load")
          const variablesName = node.args
            .slice(2)
            .map((arg) => this.extractString(arg as PythonAstNode))
          if (variablesName.every((variableName) => variableName !== null)) {
            return variablesName as string[]
          }
          // Don't break, to test for UK form of "add" below.
          // return null
        }
        // UK
        if (["add", "aggr"].includes(func.id)) {
          // Example: add(person, period, ["savings_allowance", "savings_starter_rate_income"])
          console.assert(func.ctx.ast_class === "Load")
          const variablesName = this.extractStringList(
            node.args[2] as PythonAstNode,
          )
          if (variablesName !== null) {
            return variablesName
          }
          console.log("TODO: Unrecognized variable call in formula:", node)
          console.log("node.args", node.args)
          return null
        }
        if (this.entityByKey[func.id] !== undefined) {
          console.assert(func.ctx.ast_class === "Load")
          const variableName = this.extractString(node.args[0] as PythonAstNode)
          if (variableName !== null) {
            return [variableName]
          }
          console.log("TODO: Unrecognized variable call in formula:", node)
          return null
        }
        return null
      }
      default:
        return null
    }
  }

  extractString(node: PythonAstNode): string | null {
    switch (node.ast_class) {
      case "Constant":
        return typeof node.value === "string" ? node.value : null
      case "Name": {
        if (node.ctx.ast_class === "Load") {
          const variableDescription = this.variableDescriptionById[node.id]
          if (variableDescription?.type === VariableType.String) {
            return variableDescription.value
          }
        }
        return null
      }
      default:
        return null
    }
  }

  extractStringList(node: PythonAstNode): string[] | null {
    switch (node.ast_class) {
      case "List":
      case "Tuple":
        return node.ctx.ast_class === "Load" &&
          node.elts.every(
            (element) =>
              element.ast_class === "Constant" &&
              typeof element.value === "string",
          )
          ? (node.elts.map(
              (element) => (element as PythonAstConstant).value,
            ) as string[])
          : null
      case "Name": {
        if (node.ctx.ast_class === "Load") {
          const variableDescription = this.variableDescriptionById[node.id]
          if (variableDescription?.type === VariableType.StringListOrTuple) {
            return variableDescription.items
          }
        }
        return null
      }
      default:
        return null
    }
  }

  /// Detect if node is a boolean or a boolean vector.
  isBoolean(node: PythonAstNode): boolean {
    switch (node.ast_class) {
      case "Call": {
        const func = node.func
        switch (func.ast_class) {
          case "Attribute": {
            const value = func.value
            const attr = func.attr
            switch (value.ast_class) {
              case "Attribute": {
                const value2 = value.value
                const attr2 = value.attr
                switch (value2.ast_class) {
                  case "Name": {
                    const value2Entity = this.entityByKey[value2.id]
                    if (
                      value2Entity !== undefined &&
                      !value2Entity.is_person &&
                      attr2 === "members" &&
                      attr === "has_role"
                    ) {
                      // Example: menage.members.has_role(Famille.ENFANT)
                      console.assert(value2.ctx.ast_class === "Load")
                      return true
                    }

                    return false
                  }
                  default:
                    return false
                }

                return false
              }
              case "Name": {
                const valueEntity = this.entityByKey[value.id]
                if (
                  valueEntity !== undefined &&
                  valueEntity.is_person &&
                  func.attr === "has_role"
                ) {
                  // Example: individu.has_role(FoyerFiscal.DECLARANT_PRINCIPAL)
                  console.assert(value.ctx.ast_class === "Load")
                  return true
                }

                return false
              }
              default:
                return false
            }
          }
          case "Name": {
            const entity = this.entityByKey[func.id]
            if (
              entity !== undefined &&
              entity.is_person &&
              node.args.length === 2 &&
              node.args[0].ast_class === "Constant" &&
              typeof node.args[0].value === "string" &&
              // UK
              ["is_benunit_head", "is_household_head"].includes(
                node.args[0].value,
              )
            ) {
              // Example: person("is_benunit_head", period)
              return true
            }
            return false
          }
          default:
            return false
        }
      }
      case "Name": {
        if (node.ctx.ast_class === "Load") {
          return (
            this.variableDescriptionById[node.id]?.type === VariableType.Boolean
          )
        }
        return false
      }
      default:
        return false
    }
  }

  visit_Assign(node: PythonAstAssign): void {
    const openFiscaParametersName = new Set(this.openFiscaParametersName)
    this.visitGeneric(node)

    const value = node.value
    let variableDescription: VariableDescription | null = null // `null` means unknown variable description

    if (variableDescription === null) {
      const openFiscaParameterName = this.extractOpenFiscaParameterName(
        value as PythonAstNode,
      )
      if (openFiscaParameterName !== null) {
        // Ignore OpenFisca parameters added to this.openFiscaParametersName by above call to
        // `this.visitGeneric(node)`, because they are parent of `openFiscaParameterName` and not really
        // used.
        this.openFiscaParametersName = openFiscaParametersName

        variableDescription = {
          name: openFiscaParameterName,
          type: VariableType.OpenFiscaParameter,
        }
      }
    }

    if (variableDescription === null) {
      const openFiscaVariablesName = this.extractOpenFiscaVariablesName(
        value as PythonAstNode,
      )
      if (openFiscaVariablesName !== null) {
        if (openFiscaVariablesName.length === 1) {
          variableDescription = {
            name: openFiscaVariablesName[0],
            type: VariableType.OpenFiscaVariable,
          }
        } else {
          variableDescription = {
            names: openFiscaVariablesName,
            type: VariableType.OpenFiscaVariableList,
          }
        }
      } else {
        // Note: extractDecomposition must be executed after extractOpenFiscaVariablesName.
        const decomposition = this.extractDecomposition(value as PythonAstNode)
        if (decomposition !== null) {
          variableDescription = {
            decomposition,
            type: VariableType.Decomposition,
          }
        } else if (this.isBoolean(value as PythonAstNode)) {
          variableDescription = {
            type: VariableType.Boolean,
          }
        }
      }
    }

    if (variableDescription === null) {
      const stringListOrTuple = this.extractStringList(value as PythonAstNode)
      if (stringListOrTuple !== null) {
        variableDescription = {
          items: stringListOrTuple,
          type: VariableType.StringListOrTuple,
        }
      }
    }

    for (const target of node.targets) {
      if (target.ast_class === "Name" && target.ctx.ast_class === "Store") {
        this.variableDescriptionById[target.id] = variableDescription
      }
    }
  }

  visit_Attribute(node: PythonAstAttribute): void {
    const openFiscaParametersName = new Set(this.openFiscaParametersName)
    this.visitGeneric(node)

    const openFiscaParameterName =
      this.extractOpenFiscaParameterNameFromAttribute(node)
    if (openFiscaParameterName !== null) {
      // Ignore OpenFisca parameters added to this.openFiscaParametersName by above call to
      // `this.visitGeneric(node)`, because they are parent of `openFiscaParameterName` and not really
      // used.
      openFiscaParametersName.add(openFiscaParameterName)
      this.openFiscaParametersName = openFiscaParametersName
    }
  }

  visit_AugAssign(node: PythonAstAugAssign): void {
    this.visit({
      ast_class: "Assign",
      targets: [node.target],
      value: {
        ast_class: "BinOp",
        left: {
          ...node.target,
          ctx: { ast_class: "Load" },
        },
        op: node.op as PythonAstBinaryOperator,
        right: node.value,
      },
    })
  }

  visit_Call(node: PythonAstCall): void {
    // Note: this.visitGeneric(node) is called below.

    const { func } = node
    if (
      func.ast_class === "Name" &&
      [
        "apply_bareme",
        "compute_cotisation",
        "compute_cotisation_annuelle",
        "compute_cotisation_anticipee",
      ].includes(func.id)
    ) {
      const keywordByArg = Object.fromEntries(
        node.keywords.map((keyword) => [keyword.arg, keyword]),
      )
      const baremeName = keywordByArg["bareme_name"]?.value
      const cotisationType = keywordByArg["cotisation_type"]?.value
      if (
        baremeName?.ast_class === "Constant" &&
        cotisationType !== undefined
      ) {
        const baremePrefixes =
          cotisationType.ast_class === "Constant"
            ? cotisationType.value === "employeur"
              ? ["cotsoc.cotisations_employeur"]
              : cotisationType.value === "salarie"
                ? ["cotsoc.cotisations_salarie"]
                : []
            : // Assume that cotisationType is a variable that can have both values "employeur" & "salarie".
              ["cotsoc.cotisations_employeur", "cotsoc.cotisations_salarie"]
        for (const baremePrefix of baremePrefixes) {
          for (const categorieSalarie of [
            "prive_non_cadre",
            "prive_cadre",
            "public_titulaire_etat",
            "public_titulaire_militaire",
            "public_titulaire_territoriale",
            "public_titulaire_hospitaliere",
            "public_non_titulaire",
            // "non_pertinent",
          ]) {
            const baremeParameterName = `${baremePrefix}.${categorieSalarie}.${baremeName.value}`
            if (this.leafParametersName.has(baremeParameterName)) {
              this.openFiscaParametersName.add(baremeParameterName)
            }
          }
        }
        // Visit all other items of node.
        for (const arg of node.args) {
          this.visit(arg as PythonAstNode)
        }
        for (const keyword of node.keywords) {
          if (
            keyword.arg === undefined ||
            !["bareme_name", "cotisation_type"].includes(keyword.arg)
          ) {
            this.visit(keyword)
          }
        }
        return
      }
    } else if (
      func.ast_class === "Name" &&
      func.id === "apply_bareme_for_relevant_type_sal"
    ) {
      const keywordByArg = Object.fromEntries(
        node.keywords.map((keyword) => [keyword.arg, keyword]),
      )
      const baremeByTypeSalName = keywordByArg["bareme_by_type_sal_name"]?.value
      const baremeName = keywordByArg["bareme_name"]?.value
      if (
        baremeByTypeSalName !== undefined &&
        baremeName?.ast_class === "Constant"
      ) {
        const baremePrefix =
          this.extractOpenFiscaParameterName(baremeByTypeSalName)
        if (baremePrefix !== null) {
          for (const categorieSalarie of [
            "prive_non_cadre",
            "prive_cadre",
            "public_titulaire_etat",
            "public_titulaire_militaire",
            "public_titulaire_territoriale",
            "public_titulaire_hospitaliere",
            "public_non_titulaire",
            // "non_pertinent",
          ]) {
            const baremeParameterName = `${baremePrefix}.${categorieSalarie}.${baremeName.value}`
            if (this.leafParametersName.has(baremeParameterName)) {
              this.openFiscaParametersName.add(baremeParameterName)
            }
          }
          // Visit all other items of node.
          for (const arg of node.args) {
            this.visit(arg as PythonAstNode)
          }
          for (const keyword of node.keywords) {
            if (
              keyword.arg === undefined ||
              !["bareme_by_type_sal_name", "bareme_name"].includes(keyword.arg)
            ) {
              this.visit(keyword)
            }
          }
          return
        }
      }
    }

    this.visitGeneric(node)

    const openFiscaParameterName =
      this.extractOpenFiscaParameterNameFromCall(node)
    if (openFiscaParameterName !== null) {
      this.openFiscaParametersName.add(openFiscaParameterName)
      return
    }

    const openFiscaVariablesName =
      this.extractOpenFiscaVariablesNameFromCall(node)
    if (openFiscaVariablesName !== null) {
      for (const openFiscaVariableName of openFiscaVariablesName) {
        this.openFiscaVariablesName.add(openFiscaVariableName)
      }
    }
  }

  visit_Delete(node: PythonAstDelete): void {
    this.visitGeneric(node)

    for (const target of node.targets) {
      if (target.ast_class === "Name" && target.ctx.ast_class === "Del") {
        delete this.variableDescriptionById[target.id]
        continue
      }
    }
  }

  visit_For(node: PythonAstFor): void {
    const { body, iter, orelse, target } = node
    const stringList = this.extractStringList(iter)
    if (
      stringList !== null &&
      target.ast_class === "Name" &&
      target.ctx.ast_class === "Store"
    ) {
      const variableDescriptionById = this.variableDescriptionById
      for (const value of stringList) {
        this.variableDescriptionById = {
          ...variableDescriptionById,
          [target.id]: { value, type: VariableType.String },
        }
        for (const bodyItem of body) {
          this.visit(bodyItem)
        }
        for (const orelseItem of orelse) {
          this.visit(orelseItem)
        }
      }
      this.variableDescriptionById = variableDescriptionById
      return
    }

    this.visitGeneric(node)
  }

  visit_GeneratorExp(node: PythonAstGeneratorExp): void {
    this.visitSequenceGenerator(node)
  }

  visit_ListComp(node: PythonAstListComp): void {
    this.visitSequenceGenerator(node)
  }

  visit_Name(node: PythonAstName): void {
    this.visitGeneric(node)

    if (node.ctx.ast_class === "Load") {
      const variableDescription = this.variableDescriptionById[node.id]
      if (variableDescription?.type === VariableType.OpenFiscaParameter) {
        this.openFiscaParametersName.add(variableDescription.name)
      }
    }
  }

  visit_Return(node: PythonAstReturn): void {
    this.visitGeneric(node)

    if (node.value !== undefined) {
      const decomposition = this.extractDecomposition(
        node.value as PythonAstNode,
      )
      if (decomposition !== null) {
        this.decomposition = decomposition
      }
    }
  }

  visit_SetComp(node: PythonAstSetComp): void {
    this.visitSequenceGenerator(node)
  }

  visit_Subscript(node: PythonAstSubscript): void {
    const openFiscaParametersName = new Set(this.openFiscaParametersName)
    this.visitGeneric(node)

    const openFiscaParameterName =
      this.extractOpenFiscaParameterNameFromSubscript(node)
    if (openFiscaParameterName !== null) {
      // Ignore OpenFisca parameters added to this.openFiscaParametersName by above call to
      // `this.visitGeneric(node)`, because they are parent of `openFiscaParameterName` and not really
      // used.
      openFiscaParametersName.add(openFiscaParameterName)
      this.openFiscaParametersName = openFiscaParametersName
    }
  }

  visitSequenceGenerator(
    node: PythonAstGeneratorExp | PythonAstListComp | PythonAstSetComp,
  ): void {
    if (node.generators.length === 1) {
      const { ifs, iter, target } = node.generators[0]
      const stringList = this.extractStringList(iter)
      if (
        stringList !== null &&
        target.ast_class === "Name" &&
        target.ctx.ast_class === "Store"
      ) {
        const variableDescriptionById = this.variableDescriptionById
        for (const value of stringList) {
          this.variableDescriptionById = {
            ...variableDescriptionById,
            [target.id]: { value, type: VariableType.String },
          }
          for (const ifNode of ifs) {
            this.visit(ifNode)
          }
          this.visit(node.elt)
        }
        this.variableDescriptionById = variableDescriptionById
        return
      }
    }

    this.visitGeneric(node)
  }
}

export function extractFromFormulaAst(
  formula: PythonAstFunctionDef,
  entityByKey: EntityByKey,
  leafParametersName: Set<string>,
): FormulaExtractor {
  const extractor = new FormulaExtractor(entityByKey, leafParametersName)
  extractor.visit(formula)
  return extractor
}
