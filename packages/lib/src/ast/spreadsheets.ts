import {
  accessParameterFromIds,
  ParameterClass,
  type NodeParameter,
} from "../parameters"
import { Period } from "../periods"
import type { Variable } from "../variables"

import type {
  OpenfiscaAstBuiltIn,
  OpenfiscaAstCogCall,
  OpenfiscaAstEntity,
  OpenfiscaAstInstantOrPeriodExpression,
  OpenfiscaAstNode,
  OpenfiscaAstParameterAtInstant,
  OpenfiscaAstPeriod,
  PythonAstAttribute,
  PythonAstBinOp,
  PythonAstCall,
  PythonAstCompare,
  PythonAstComparisonOperator,
  PythonAstConstant,
  PythonAstKeyword,
  PythonAstSubscript,
  PythonAstUnaryOp,
  PythonAstUnaryOperator,
} from "./nodes"
import { evaluateOpenfiscaAstInstantOrPeriod } from "./periods"
import { AstTransformer } from "./visitors"

class XlsxFormulaGenerator extends AstTransformer<OpenfiscaAstNode, unknown> {
  constructor(
    public readonly variable: Variable,
    public readonly period: Period,
    public readonly rootParameter: NodeParameter,
    public readonly variableSummaryByName: { [name: string]: Variable },
  ) {
    super()
  }

  transform_Attribute(attribute: PythonAstAttribute): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const value = this.transform(attribute.value)
    return this.reduceErrorByKey(
      attribute,
      `${value.output}.${attribute.attr}`,
      {
        value: value.error,
      },
    )
  }

  transform_BinOp(binOp: PythonAstBinOp): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const left = this.transform(binOp.left)
    const right = this.transform(binOp.right)
    switch (binOp.op.ast_class) {
      case "Add":
        return this.reduceErrorByKey(
          binOp,
          `${left.output} + ${right.output}`,
          { left: left.error, right: right.error },
        )
      case "BitAnd":
        return this.reduceErrorByKey(
          binOp,
          `AND(${left.output},${right.output})`,
          { left: left.error, right: right.error },
        )
      case "BitOr":
        return this.reduceErrorByKey(
          binOp,
          `OR(${left.output},${right.output})`,
          { left: left.error, right: right.error },
        )
      case "Div":
        return this.reduceErrorByKey(
          binOp,
          `${left.output} / ${right.output}`,
          { left: left.error, right: right.error },
        )
      case "Mult":
        return this.reduceErrorByKey(
          binOp,
          `${left.output} * ${right.output}`,
          { left: left.error, right: right.error },
        )
      case "Sub":
        return this.reduceErrorByKey(
          binOp,
          `${left.output} / ${right.output}`,
          { left: left.error, right: right.error },
        )
      default:
        assertNever("binary operator", binOp.op)
    }
  }

  transform_built_in(builtIn: OpenfiscaAstBuiltIn): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return { input: builtIn, output: builtIn.name }
  }

  transform_Call(call: PythonAstCall): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const { args, func, keywords } = call

    const argsTransformed = this.reduceMapErrors(
      args.map((arg) => this.transform(arg)),
    )
    const funcTransformed = this.transform(func)
    const keywordsTransformed = this.reduceMapErrors(
      keywords.map((keyword) => this.transform(keyword)),
    )

    if (
      argsTransformed.error === undefined &&
      funcTransformed.error === undefined &&
      keywordsTransformed.error === undefined
    ) {
      switch (func.ast_class) {
        case "Attribute": {
          const attribute = func
          const { attr, value } = attribute
          if (value.ast_class === "entity") {
            const entity = value
            switch (attr) {
              case "nb_persons": {
                if (args.length === 0 && keywords.length === 0) {
                  // To find the number of persons in entity, count the number of ":" in column "membres" of entity.
                  // See https://learn.microsoft.com/en-us/office/troubleshoot/excel/formulas-to-count-occurrences-in-excel
                  const membersRange = `FORMULATEXT(membres_${entity.key})`
                  return {
                    input: call,
                    output: `(LEN(${membersRange})-LEN(SUBSTITUTE(${membersRange},":",""))+1)/2`,
                  }
                } else {
                  return {
                    input: call,
                    output: JSON.stringify(call),
                    error: `Unexpected arguments in method "${attr}" of entity expression`,
                  }
                }
              }
              case "sum": {
                if (args.length === 1 && keywords.length === 0) {
                  const cogCall = this.transform(args[0])
                  const membersRange = `REPLACE(FORMULATEXT(membres_${entity.key}),0,1,"")`
                  return this.reduceErrorByKey(
                    call,
                    `SUM(INDIRECT(${membersRange}) ${cogCall.output})`,
                    cogCall.error === undefined
                      ? {}
                      : { func: { args: { 0: cogCall.error } } },
                  )
                } else {
                  return {
                    input: call,
                    output: JSON.stringify(call),
                    error: `Unexpected arguments in method "${attr}" of entity expression`,
                  }
                }
              }
              default:
                return {
                  input: call,
                  output: JSON.stringify(call),
                  error: `Unknown entity method "${attr}" in entity expression`,
                }
            }
          } else {
            return {
              input: call,
              output: JSON.stringify(call),
              error: `Unknown object "${value}" for method "${attr}" in entity expression`,
            }
          }
        }
        default:
      }
    }

    return this.reduceErrorByKey(
      call,
      `${funcTransformed.output}(${[
        ...argsTransformed.output,
        ...keywordsTransformed.output,
      ].join(", ")})`,
      {
        args: argsTransformed.error,
        func: funcTransformed.error,
        keywords: keywordsTransformed.error,
      },
    )
  }

  transform_cog_call(cogCall: OpenfiscaAstCogCall): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const { cog } = cogCall
    const period = this.transform(cogCall.period)
    let output: string
    let operationError: string | undefined = undefined
    if (cogCall.operation === undefined) {
      output = `${cog.name}_${String(period.output).replace(/-/g, "_")}`
    } else if (
      cogCall.operation === "ADD" &&
      cog.periodUnit === "month" &&
      this.variable.definition_period === "year"
    ) {
      const monthlyOutputs: string[] = []
      for (let month = 1; month <= 12; month++) {
        monthlyOutputs.push(
          `${cog.name}_${period.output}_${month.toString().padStart(2, "0")}`,
        )
      }
      output = monthlyOutputs.join(" * ")
    } else if (
      cogCall.operation === "DIVIDE" &&
      cog.periodUnit === "year" &&
      this.variable.definition_period === "month"
    ) {
      const monthlyOutputs: string[] = []
      for (let month = 1; month <= 12; month++) {
        monthlyOutputs.push(
          `${cog.name}_${String(period.output).split("-")[0]} / 12`,
        )
      }
      output = monthlyOutputs.join("*")
    } else {
      operationError = `Unable to convert ${cog.name} with a definition period of ${cog.periodUnit} to ${period.output}`
      output = `${cog.name}_${String(period.output).replace(/-/g, "_")}`
    }
    return this.reduceErrorByKey(cogCall, output, {
      operation: operationError,
      period: period.error,
    })
  }

  transform_Compare(compare: PythonAstCompare): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const left = this.transform(compare.left)
    const operators = compare.ops.map((operator) =>
      transformPythonAstComparisonOperator(operator),
    )
    const comparators = this.reduceMapErrors(
      compare.comparators.map((comparator) => this.transform(comparator)),
    )
    const fragments = [left.output]
    for (const [index, operator] of operators.entries()) {
      fragments.push(operator, comparators.output[index])
    }
    return this.reduceErrorByKey(compare, fragments.join(""), {
      left: left.error,
      comparators: comparators.error,
    })
  }

  transform_Constant(constant: PythonAstConstant): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return { input: constant, output: JSON.stringify(constant.value) }
  }

  transform_entity(entity: OpenfiscaAstEntity): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return { input: entity, output: entity.key }
  }

  transform_instant_or_period_expression(
    instantOrPeriodExpression: OpenfiscaAstInstantOrPeriodExpression,
  ): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const expression = evaluateOpenfiscaAstInstantOrPeriod(
      instantOrPeriodExpression.expression,
      this.period,
    )
    return this.reduceErrorByKey(instantOrPeriodExpression, expression.output, {
      expression: expression.error,
    })
  }

  transform_keyword(keyword: PythonAstKeyword): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const value = this.transform(keyword.value)
    if (keyword.arg === undefined) {
      return {
        input: keyword,
        output: `**${value.output}`,
        error: "Can't handle yet keyword argument without name",
      }
    }
    return this.reduceErrorByKey(keyword, `${keyword.arg}=${value.output}`, {
      arg:
        keyword.arg === undefined
          ? "Can't handle yet keyword argument without name"
          : undefined,
      value: value.error,
    })
  }

  transform_parameter_at_instant(
    parameterAtInstant: OpenfiscaAstParameterAtInstant,
  ): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const instant = this.transform(parameterAtInstant.instant)
    const instantString = String(
      instant.output instanceof Period ? instant.output.start : instant.output,
    )
    if (instant.error !== undefined) {
      return this.reduceErrorByKey(
        parameterAtInstant,
        [`parameters(${instantString})`, ...parameterAtInstant.ids].join("."),
        { instant: instant.error },
      )
    }
    const [parameter, parameterError] = accessParameterFromIds(
      this.rootParameter,
      parameterAtInstant.ids,
    )
    if (parameterError !== null) {
      return this.reduceErrorByKey(
        parameterAtInstant,
        [`parameters(${instantString})`, ...parameterAtInstant.ids].join("."),
        {
          parameter: parameterError,
        },
      )
    }
    switch (parameter.class) {
      case ParameterClass.Node: {
        return this.reduceErrorByKey(
          parameterAtInstant,
          [`parameters(${instantString})`, ...parameterAtInstant.ids].join("."),
          {
            parameter:
              "Parameter is a node instead of a leaf (ie scale or value)",
          },
        )
      }
      case ParameterClass.Scale: {
        return this.reduceErrorByKey(
          parameterAtInstant,
          [`parameters(${instantString})`, ...parameterAtInstant.ids].join("."),
          {
            parameter: "Scale parameter can't be used directly in formula",
          },
        )
      }
      case ParameterClass.Value: {
        const valueAtInstant = Object.entries(parameter.values)
          .sort(([instant1], [instant2]) => instant2.localeCompare(instant1))
          .find(([instant1]) => instant1 <= instantString)?.[1]
        if (valueAtInstant === undefined) {
          return this.reduceErrorByKey(
            parameterAtInstant,
            [`parameters(${instant.output})`, ...parameterAtInstant.ids].join(
              ".",
            ),
            {
              parameter: `Parameter has no value at instant ${instant.output}`,
            },
          )
        }
        const value = valueAtInstant.value
        if (value === null) {
          return this.reduceErrorByKey(parameterAtInstant, "null", {
            parameter: `Value of parameter is null at instant ${instant.output}`,
          })
        }
        return { input: parameterAtInstant, output: value.toString() }
      }
      default:
        assertNever("parameter class", parameter)
    }
  }

  transform_period(period: OpenfiscaAstPeriod): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    return { input: period, output: this.period }
  }

  transform_Subscript(subscript: PythonAstSubscript): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const slice = this.transform(subscript.slice)
    const value = this.transform(subscript.value)
    return this.reduceErrorByKey(
      subscript,
      `${value.output}[${slice.output}]`,
      {
        value: value.error,
      },
    )
  }

  transform_UnaryOp(unaryOp: PythonAstUnaryOp): {
    input: OpenfiscaAstNode
    output: unknown
    error?: unknown
  } {
    const operand = this.transform(unaryOp.operand)
    const operator = transformPythonAstUnaryOperator(unaryOp.op)
    return this.reduceErrorByKey(unaryOp, `${operator}${operand.output}`, {
      operand: operand.error,
    })
  }

  transformGenericOutputByKey(
    input: OpenfiscaAstNode,
    outputByKey: {
      [key: string]: unknown
    },
    _errorByKey: {
      [key: string]: unknown
    },
  ): { input: OpenfiscaAstNode; output: unknown; error?: unknown } {
    return {
      input,
      output: JSON.stringify(outputByKey),
      error: `Missing XLSX transformer for ${input.ast_class}`,
    }
  }
}

function assertNever(type: string, value: never): never {
  throw new Error(`Unexpected ${type}: ${JSON.stringify(value)}`)
}

function transformPythonAstComparisonOperator(
  op: PythonAstComparisonOperator,
): string {
  switch (op.ast_class) {
    case "Eq":
      return " == "
    case "Gt":
      return " > "
    case "GtE":
      return " >= "
    case "In":
      return " in "
    case "Is":
      return " === "
    case "IsNot":
      return " !== "
    case "Lt":
      return " < "
    case "LtE":
      return " <= "
    case "NotEq":
      return " != "
    case "NotIn":
      return " not in "
    default:
      assertNever("binary operator", op)
  }
}

function transformPythonAstUnaryOperator(op: PythonAstUnaryOperator): string {
  switch (op.ast_class) {
    case "Invert":
      return "~"
    case "Not":
      return "not "
    case "UAdd":
      return "+"
    case "USub":
      return "-"
    default:
      assertNever("unary operator", op)
  }
}

export function xlsxFormulaFromOpenfiscaAst(
  variable: Variable,
  node: OpenfiscaAstNode,
  period: Period,
  rootParameter: NodeParameter,
  variableSummaryByName: { [name: string]: Variable },
): {
  input: OpenfiscaAstNode
  output: string
  error?: unknown
} {
  const xlsxFormulaGenerator = new XlsxFormulaGenerator(
    variable,
    period,
    rootParameter,
    variableSummaryByName,
  )
  const result = xlsxFormulaGenerator.transform(node)
  return typeof result.output === "string"
    ? (result as {
        input: OpenfiscaAstNode
        output: string
        error?: unknown
      })
    : {
        ...result,
        output: String(result.output),
      }
}
