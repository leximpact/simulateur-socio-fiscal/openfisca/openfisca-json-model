import type { Entity } from "../entities"
import type { PeriodUnit } from "../periods"

//
// OpenFisca Abstract Syntax Tree (AST)
//

export interface OpenfiscaAstNodeBase {
  ast_class: OpenfiscaAstClass
}

export type OpenfiscaAstNode =
  | OpenfiscaAstBuiltIn
  | OpenfiscaAstCog
  | OpenfiscaAstCogCall
  | OpenfiscaAstEntity
  | OpenfiscaAstFormula
  | OpenfiscaAstInstantOrPeriodExpression
  | OpenfiscaAstParameter
  | OpenfiscaAstParameterAtInstant
  | OpenfiscaAstPeriod
  | PythonAstNode

export type OpenfiscaAstClass =
  | "built_in"
  | "cog"
  | "cog_call"
  | "entity"
  | "formula"
  | "instant_or_period_expression"
  | "parameter"
  | "parameter_at_instant"
  | "period"

//
// OpenFisca AST Nodes
//

export interface OpenfiscaAstBuiltIn extends OpenfiscaAstNodeBase {
  ast_class: "built_in"
  name:
    | OpenfiscaAstCogCallOperation
    | "Famille"
    | "FoyerFiscal"
    | "max"
    | "Menage"
    | "min"
    | "not"
}

/// Cog = OpenFisca variable (aka dispositif, aka rouage)
export interface OpenfiscaAstCog extends OpenfiscaAstNodeBase {
  ast_class: "cog"
  entity: Entity
  name: string
  periodUnit: PeriodUnit
}

/// Use of an OpenFisca variable in formula.
export interface OpenfiscaAstCogCall extends OpenfiscaAstNodeBase {
  ast_class: "cog_call"
  cog: OpenfiscaAstCog
  operation?: OpenfiscaAstCogCallOperation
  period: OpenfiscaAstInstantOrPeriodExpression | OpenfiscaAstPeriod
}

export type OpenfiscaAstCogCallOperation = "ADD" | "DIVIDE"

export interface OpenfiscaAstEntity extends OpenfiscaAstNodeBase {
  ast_class: "entity"
  /// Entity key
  key: string
}

export interface OpenfiscaAstFormula extends OpenfiscaAstNodeBase {
  ast_class: "formula"
  returnValue: {
    input: PythonAstNode
    output: OpenfiscaAstNode
    error?: unknown
  }
  // variableValueByName: {
  //   [name: string]: {
  //     input: PythonAstNode
  //     output: OpenfiscaAstNode
  //     error?: unknown
  //   }
  // }
}

export interface OpenfiscaAstInstantOrPeriodExpression
  extends OpenfiscaAstNodeBase {
  ast_class: "instant_or_period_expression"
  expression: OpenfiscaAstInstantOrPeriodExpression | PythonAstNode
}

export interface OpenfiscaAstParameter extends OpenfiscaAstNodeBase {
  ast_class: "parameter"
  ids: string[]
}

export interface OpenfiscaAstParameterAtInstant extends OpenfiscaAstNodeBase {
  ast_class: "parameter_at_instant"
  ids: string[]
  instant: OpenfiscaAstNode // TODO: OpenfiscaAstInstant or derivated
}

export interface OpenfiscaAstPeriod extends OpenfiscaAstNodeBase {
  ast_class: "period"
}

//
// Python Abstract Syntax Tree (AST)
//

export interface PythonAstNodeBase {
  ast_class: PythonAstClass
  col_offset?: number
  end_col_offset?: number
  end_lineno?: number
  lineno?: number
}

export type PythonAstClass =
  | "Add"
  | "Arg"
  | "Arguments"
  | "Assign"
  | "Attribute"
  | "AugAssign"
  | "BinOp"
  | "BitAnd"
  | "BitOr"
  | "Call"
  | "Compare"
  | "comprehension" // Note: lowercase "c"
  | "Constant"
  | "Del"
  | "Delete"
  | "Div"
  | "Eq"
  | "For"
  | "FunctionDef"
  | "GeneratorExp"
  | "Gt"
  | "GtE"
  | "In"
  | "Invert"
  | "Is"
  | "IsNot"
  | "keyword" // Note: lowercase "k"
  | "List"
  | "ListComp"
  | "Load"
  | "Lt"
  | "LtE"
  | "Mult"
  | "Name"
  | "Not"
  | "NotEq"
  | "NotIn"
  | "Return"
  | "SetComp"
  | "Store"
  | "Sub"
  | "Subscript"
  | "Tuple"
  | "UAdd"
  | "UnaryOp"
  | "USub"

export type PythonAstNode =
  | PythonAstArg
  | PythonAstArguments
  | PythonAstAssign
  | PythonAstAttribute
  | PythonAstAugAssign
  | PythonAstBinOp
  | PythonAstBinaryOperator
  | PythonAstCall
  | PythonAstCompare
  | PythonAstComparisonOperator
  | PythonAstComprehension
  | PythonAstConstant
  | PythonAstDel
  | PythonAstDelete
  | PythonAstFor
  | PythonAstFunctionDef
  | PythonAstGeneratorExp
  | PythonAstKeyword
  | PythonAstList
  | PythonAstListComp
  | PythonAstLoad
  | PythonAstName
  | PythonAstReturn
  | PythonAstSetComp
  | PythonAstStore
  | PythonAstSubscript
  | PythonAstTuple
  | PythonAstUnaryOp
  | PythonAstUnaryOperator

//
// Python AST Nodes
//

/// Addition binary operator
export interface PythonAstAdd extends PythonAstNodeBase {
  ast_class: "Add"
}

/// A single argument in a list.
/// `arg` is a raw string of the argument name,
/// `annotation` is its annotation, such as a `Str` or `Name` node.
export interface PythonAstArg extends PythonAstNodeBase {
  ast_class: "Arg"
  annotation?: PythonAstNode[]
  arg: string
}

/// PythonAstArguments of a function
/// `posonlyargs`, `args` and `kwonlyargs` are lists of `Arg` nodes.
/// `vararg` and `kwarg` are single `Arg` nodes, referring to the `*args`, `**kwargs` parameters.
/// `kw_defaults` is a list of default values for keyword-only arguments. If one is None, the corresponding argument is required.
/// `defaults` is a list of default values for arguments that can be passed positionally.
/// If there are fewer defaults, they correspond to the last n arguments.
export interface PythonAstArguments extends PythonAstNodeBase {
  ast_class: "Arguments"
  args: PythonAstArg[]
  defaults: PythonAstNode[]
  kw_defaults: PythonAstNode[]
  kwarg?: PythonAstArg
  kwonlyargs: PythonAstArg[]
  posonlyargs: PythonAstArg[]
  vararg?: PythonAstArg
}

/// Assignment
/// `targets` is a list of nodes, and `value` is a single node.
export interface PythonAstAssign extends PythonAstNodeBase {
  ast_class: "Assign"
  targets: OpenfiscaAstNode[]
  value: OpenfiscaAstNode
}

/// PythonAstAttribute access, e.g. `d.keys`
/// `value` is a node, typically a PythonAstName.
/// `attr` is a bare string giving the name of the attribute,
/// and `ctx` is `Load`, `Store` or `Del` according to how the attribute is acted on.
export interface PythonAstAttribute extends PythonAstNodeBase {
  ast_class: "Attribute"
  attr: string
  ctx: PythonAstDel | PythonAstLoad | PythonAstStore
  value: OpenfiscaAstNode
}

/// Augmented assignment, such as `a += 1`.
/// `target` is a `Name` node (with the `Store` context),
/// `op` is `Add` or `Sub` or…,
/// `value` is a single node.
/// The `target` attribute cant be of class `Tuple` or `List`,
/// unlike the `targets` of `Assign`.
export interface PythonAstAugAssign extends PythonAstNodeBase {
  ast_class: "AugAssign"
  op: PythonAstNode
  target: PythonAstName
  value: PythonAstNode
}

export type PythonAstBinaryOperator =
  | PythonAstAdd
  | PythonAstBitAnd
  | PythonAstBitOr
  | PythonAstDiv
  | PythonAstMult
  | PythonAstSub

/// Binary operation (like addition or division)
/// `op` is the operator,
/// and `left` and `right` are any expression nodes.
export interface PythonAstBinOp extends PythonAstNodeBase {
  ast_class: "BinOp"
  left: PythonAstNode
  op: PythonAstBinaryOperator
  right: PythonAstNode
}

/// Bit and binary operator
export interface PythonAstBitAnd extends PythonAstNodeBase {
  ast_class: "BitAnd"
}

/// Bit or binary operator
export interface PythonAstBitOr extends PythonAstNodeBase {
  ast_class: "BitOr"
}

/// Function call
/// `func` is the function, which will often be a `Name` or `Attribute` object.
/// Of the arguments:
/// * `args` holds a list of the arguments passed by position.
/// * `keywords` holds a list of `keyword` objects representing arguments passed by keyword.
/// `args` and `keywords` are required, but they can be empty lists.
export interface PythonAstCall extends PythonAstNodeBase {
  args: OpenfiscaAstNode[]
  ast_class: "Call"
  func: OpenfiscaAstNode
  keywords: PythonAstKeyword[]
}

/// Comparison of two or more values.
/// `left` is the first value in the comparison,
/// `ops` the list of operators,
/// and `comparators` the list of values after the first element in the comparison.
export interface PythonAstCompare extends PythonAstNodeBase {
  ast_class: "Compare"
  comparators: PythonAstNode[]
  left: PythonAstNode
  ops: PythonAstComparisonOperator[]
}

export type PythonAstComparisonOperator =
  | PythonAstEq
  | PythonAstGt
  | PythonAstGtE
  | PythonAstIn
  | PythonAstIs
  | PythonAstIsNot
  | PythonAstLt
  | PythonAstLtE
  | PythonAstNotEq
  | PythonAstNotIn

/// One for clause in a comprehension.
/// `target` is the reference to use for each element - typically a `Name` or `Tuple` node.
/// `iter` is the object to iterate over.
/// `ifs` is a list of test expressions: each for clause can have multiple ifs.
/// `is_async` indicates a comprehension is asynchronous (using an async for instead of for).
///  The value is an integer (0 or 1).
export interface PythonAstComprehension extends PythonAstNodeBase {
  ast_class: "comprehension"
  ifs: PythonAstNode[]
  is_async: number // 0 or 1
  iter: PythonAstNode
  target: PythonAstNode
  value: PythonAstNode
}

/// PythonAstConstant value
/// The value attribute of the PythonAstConstant literal contains the object it represents.
/// The values represented can be simple types such as a number, string or undefined (None),
/// but also immutable container types (lists) if all of their elements are constant.
export interface PythonAstConstant extends PythonAstNodeBase {
  ast_class: "Constant"
  value?: number | string // TODO: Add other types.
}

/// Context of a variable, attribute, etc, indicating a delete operation
export interface PythonAstDel extends PythonAstNodeBase {
  ast_class: "Del"
}

/// Represents a `del` statement
/// `targets` is a list of nodes, such as `Name`, `Attribute` or `Subscript` nodes.
export interface PythonAstDelete extends PythonAstNodeBase {
  ast_class: "Delete"
  targets: PythonAstNode[]
}

/// Division binary operator
export interface PythonAstDiv extends PythonAstNodeBase {
  ast_class: "Div"
}

/// "Equal" comparison operator
export interface PythonAstEq extends PythonAstNodeBase {
  ast_class: "Eq"
}

/// PythonAstFor loop.
/// `target` holds the variable(s) the loop assigns to, as a single `Name`, `Tuple` or `List` node.
/// `iter` holds the item to be looped over, again as a single node.
/// `body` and `orelse` contain lists of nodes to execute.
/// Those in `orelse` are executed if the loop finishes normally, rather than via a `break` statement.
/// `type_comment` is an optional string with the type annotation as a comment.
export interface PythonAstFor extends PythonAstNodeBase {
  ast_class: "For"
  body: PythonAstNode[]
  iter: PythonAstNode
  orelse: PythonAstNode[]
  target: PythonAstNode
  type_comment?: string
}

/// Function definition
export interface PythonAstFunctionDef extends PythonAstNodeBase {
  ast_class: "FunctionDef"
  /// PythonAstArguments node
  args: PythonAstArguments
  /// PythonAstList of nodes inside the function
  body: PythonAstNode[]
  /// PythonAstList of decorators to be applied, stored outermost first
  /// (i.e. the first in the list will be applied last)
  decorator_list: unknown // TODO
  /// Function name
  name: string
  /// PythonAstReturn annotation
  return: unknown // TODO
}

/// Generator expressions.
/// `elt` is a single node representing the part that will be evaluated for each item.
/// `generators` is a list of `comprehension` nodes.
export interface PythonAstGeneratorExp extends PythonAstNodeBase {
  ast_class: "GeneratorExp"
  elt: PythonAstNode
  generators: PythonAstComprehension[]
}

/// "Greater than" comparison operator
export interface PythonAstGt extends PythonAstNodeBase {
  ast_class: "Gt"
}

/// "Greater than or equal" comparison operator
export interface PythonAstGtE extends PythonAstNodeBase {
  ast_class: "GtE"
}

/// "In" comparison operator
export interface PythonAstIn extends PythonAstNodeBase {
  ast_class: "In"
}

/// Inversion unary operator aka "~"
export interface PythonAstInvert extends PythonAstNodeBase {
  ast_class: "Invert"
}

/// "Is" comparison operator
export interface PythonAstIs extends PythonAstNodeBase {
  ast_class: "Is"
}

/// "Is not" comparison operator
export interface PythonAstIsNot extends PythonAstNodeBase {
  ast_class: "IsNot"
}

/// PythonAstKeyword argument to a function call or class definition.
/// `arg` is a raw string of the parameter name, `value` is a node to pass in.
export interface PythonAstKeyword extends PythonAstNodeBase {
  arg?: string
  ast_class: "keyword"
  value: OpenfiscaAstNode
}

/// PythonAstList.
/// `elts` holds a list of nodes representing the elements.
/// `ctx` is `Store` if the container is an assignment target (i.e. (x,y)=something),
/// and `Load` otherwise.
export interface PythonAstList extends PythonAstNodeBase {
  ast_class: "List"
  ctx: PythonAstLoad | PythonAstStore
  elts: PythonAstNode[]
}

/// PythonAstList comprehensions.
/// `elt` is a single node representing the part that will be evaluated for each item.
/// `generators` is a list of `comprehension` nodes.
export interface PythonAstListComp extends PythonAstNodeBase {
  ast_class: "ListComp"
  elt: PythonAstNode
  generators: PythonAstComprehension[]
}

/// Context of a variable, attribute, etc, indicating a load (get) operation
export interface PythonAstLoad extends PythonAstNodeBase {
  ast_class: "Load"
}

/// "Less than" comparison operator
export interface PythonAstLt extends PythonAstNodeBase {
  ast_class: "Lt"
}

/// "Less than or equal" comparison operator
export interface PythonAstLtE extends PythonAstNodeBase {
  ast_class: "LtE"
}

/// Multiplication binary operator
export interface PythonAstMult extends PythonAstNodeBase {
  ast_class: "Mult"
}

/// Variable name
/// `id` holds the name as a string,
/// and `ctx` is `Load`, `Store` or `Del` according to how the variable is acted on.
export interface PythonAstName extends PythonAstNodeBase {
  ast_class: "Name"
  ctx: PythonAstDel | PythonAstLoad | PythonAstStore
  id: string
}

/// Not unary operator aka "not"
export interface PythonAstNot extends PythonAstNodeBase {
  ast_class: "Not"
}

export interface PythonAstNotEq extends PythonAstNodeBase {
  ast_class: "NotEq"
}

export interface PythonAstNotIn extends PythonAstNodeBase {
  ast_class: "NotIn"
}

/// PythonAstReturn statement
export interface PythonAstReturn extends PythonAstNodeBase {
  ast_class: "Return"
  value?: OpenfiscaAstNode | undefined
}

/// Set comprehensions.
/// `elt` is a single node representing the part that will be evaluated for each item.
/// `generators` is a list of `comprehension` nodes.
export interface PythonAstSetComp extends PythonAstNodeBase {
  ast_class: "SetComp"
  elt: PythonAstNode
  generators: PythonAstComprehension[]
}

/// Context of a variable, attribute, etc, indicating a store (set) operation
export interface PythonAstStore extends PythonAstNodeBase {
  ast_class: "Store"
}

/// Subtraction binary operator
export interface PythonAstSub extends PythonAstNodeBase {
  ast_class: "Sub"
}

/// PythonAstSubscript, such as `l[1]`.
/// `value` is the subscripted object (usually sequence or mapping).
/// `slice` is an index, slice or key. It can be a `Tuple` and contain a `Slice`.
/// `ctx` is `Load`, `Store` or `Del` according to the action performed with the subscript.
export interface PythonAstSubscript extends PythonAstNodeBase {
  ast_class: "Subscript"
  ctx: PythonAstDel | PythonAstLoad | PythonAstStore
  slice: OpenfiscaAstNode
  value: OpenfiscaAstNode
}

/// PythonAstTuple.
/// `elts` holds a list of nodes representing the elements.
/// `ctx` is `Store` if the container is an assignment target (i.e. (x,y)=something),
/// and `Load` otherwise.
export interface PythonAstTuple extends PythonAstNodeBase {
  ast_class: "Tuple"
  ctx: PythonAstLoad | PythonAstStore
  elts: PythonAstNode[]
}

/// Addition unary operator
export interface PythonAstUAdd extends PythonAstNodeBase {
  ast_class: "UAdd"
}

/// Unary operation
/// `op` is the operator, and `operand` any expression node.
export interface PythonAstUnaryOp extends PythonAstNodeBase {
  ast_class: "UnaryOp"
  operand: PythonAstNode
  op: PythonAstUnaryOperator
}

export type PythonAstUnaryOperator =
  | PythonAstInvert
  | PythonAstNot
  | PythonAstUAdd
  | PythonAstUSub

/// Subtraction unary operator
export interface PythonAstUSub extends PythonAstNodeBase {
  ast_class: "USub"
}
