export class AstTransformer<
  InputType extends { ast_class: unknown },
  OutputType,
> {
  reduceErrorByKey(
    input: InputType,
    output: OutputType,
    errorByKey: { [key: string]: unknown },
  ): { input: InputType; output: OutputType; error?: unknown } {
    errorByKey = Object.fromEntries(
      Object.entries(errorByKey).filter(([, error]) => error != null),
    )
    const result: {
      input: InputType
      output: OutputType
      error?: unknown
    } = {
      input,
      output,
    }
    if (Object.keys(errorByKey).length !== 0) {
      result.error = errorByKey
    }
    return result
  }

  reduceMapErrors(
    results: { input: InputType; output: OutputType; error?: unknown }[],
  ): {
    input: InputType[]
    output: OutputType[]
    error?: unknown
  } {
    return reduceMapErrors(results)
  }

  transform(input: InputType): {
    input: InputType
    output: OutputType
    error?: unknown
  } {
    const transformer =
      (
        this as unknown as {
          [key: string]: (input: InputType) => {
            input: InputType
            output: OutputType
            error?: unknown
          }
        }
      )[`transform_${input.ast_class}`] ?? this.transformGeneric
    return transformer.bind(this)(input)
  }

  transformGeneric(input: InputType): {
    input: InputType
    output: OutputType
    error?: unknown
  } {
    const outputByKey: { [key: string]: unknown } = {}
    const errorByKey: { [key: string]: unknown } = {}
    for (const [key, value] of Object.entries(input)) {
      const attributeTransformer =
        (
          this as unknown as {
            [key: string]: (
              input: InputType,
              key: string,
              value: unknown,
            ) => { input: unknown; output: unknown; error?: unknown }
          }
        )[`transform_${input.ast_class}_${key}`] ??
        this.transformGenericAttribute
      const { output, error } = attributeTransformer.bind(this)(
        input,
        key,
        value,
      )
      outputByKey[key] = output
      errorByKey[key] = error
    }

    return this.transformGenericOutputByKey(input, outputByKey, errorByKey)
  }

  transformGenericAttribute(
    input: InputType,
    key: string,
    value: unknown,
  ): { error?: unknown; input: unknown; output: unknown } {
    if (value === null) {
      return { input: value, output: value }
    }
    if (Array.isArray(value)) {
      return reduceMapErrors(
        value.map((item) => this.transformGenericAttribute(input, key, item)),
      )
    }
    if (
      typeof value === "object" &&
      (value as InputType).ast_class !== undefined
    ) {
      return this.transform(value as InputType)
    }
    return { input: value, output: value }
  }

  transformGenericOutputByKey(
    input: InputType,
    outputByKey: {
      [key: string]: unknown
    },
    errorByKey: {
      [key: string]: unknown
    },
  ): { input: InputType; output: OutputType; error?: unknown } {
    return this.reduceErrorByKey(input, outputByKey as OutputType, errorByKey)
  }
}

export class AstVisitor<InputType extends { ast_class: unknown }> {
  visit(node: InputType): void {
    const visitor =
      (this as unknown as { [key: string]: (node: InputType) => void })[
        `visit_${node.ast_class}`
      ] ?? this.visitGeneric
    visitor.bind(this)(node)
  }

  visitGeneric(node: InputType): void {
    for (const [key, value] of Object.entries(node)) {
      if (key === "ast_class") {
        continue
      }
      const attributeVisitor =
        (
          this as unknown as {
            [key: string]: (
              node: InputType,
              key: string,
              value: unknown,
            ) => void
          }
        )[`visit_${node.ast_class}_${key}`] ?? this.visitGenericAttribute
      attributeVisitor.bind(this)(node, key, value)
    }
  }

  visitGenericAttribute(node: InputType, key: string, value: unknown): void {
    if (value === null) {
      return
    }
    if (Array.isArray(value)) {
      value.map((item) => this.visitGenericAttribute(node, key, item))
    }
    if (
      typeof value === "object" &&
      (value as InputType).ast_class !== undefined
    ) {
      this.visit(value as InputType)
    }
  }
}

export function reduceMapErrors<InputType, OutputType>(
  results: { input: InputType; output: OutputType; error?: unknown }[],
): {
  input: InputType[]
  output: OutputType[]
  error?: { [index: number]: unknown }
} {
  const { inputs, outputs, errorByIndex } = results.reduce(
    (accumulator, { input, output, error }, index) => {
      accumulator.inputs.push(input)
      accumulator.outputs.push(output)
      if (error !== undefined) {
        accumulator.errorByIndex[index] = error
      }
      return accumulator
    },
    { inputs: [], outputs: [], errorByIndex: {} } as {
      inputs: InputType[]
      outputs: OutputType[]
      errorByIndex: { [index: number]: unknown }
    },
  )
  const result: {
    input: InputType[]
    output: OutputType[]
    error?: { [index: number]: unknown }
  } = { input: inputs, output: outputs }
  if (Object.keys(errorByIndex).length !== 0) {
    result.error = errorByIndex
  }
  return result
}
