export interface Metadata {
  currency: string
  // Main attributes of metadata of each Python package
  distributions: { [name: string]: { [key: string]: unknown } }
  packages: PackageMetadata[]
  reforms?: ReformMetadata[]
}

export interface PackageMetadata {
  dir?: string // Removed when JSON model is finished
  name: string
  repository_url?: string
  tax_benefit_system?: boolean
  version?: string
}

export interface ReformMetadata {
  label: string
  name: string
}
