import type { PythonAstFunctionDef } from "./ast"
import type { Metadata } from "./metadata"
import type { PeriodUnit } from "./periods"
import type { ReferencesByInstant } from "./references"

export interface Formula {
  ast?: PythonAstFunctionDef // Required, but removed from variableByName for compactness
  documentation?: string
  file_path: string
  parameters?: string[]
  source_code?: string // Required, but removed from variableByName for compactness
  start_line_number: number
  stop_line_number: number
  variables?: string[]
}

export interface InputByVariableName {
  [variableName: string]: VariableValues
}

export interface Percentile {
  bucket_count: number
  bucket_mean: number
  bucket_stdev: number
  bucket_sum: number
  count_above_upper_bound: number
  lower_bound: number
  mean_above_upper_bound: number
  ratio_count_above_upper_bound: number
  sum_above_upper_bound: number
  upper_bound: number
}

export interface Variable {
  aggregate?: {
    deciles?: Percentile[]
    year: number
  }
  default_value: VariableValue
  definition_period: PeriodUnit
  description?: ReferencesByInstant
  documentation?: string
  entity: string // For France: "famille" | "foyer_fiscal" | "individu" | "menage"
  file_path: string
  formulas?: { [date: string]: Formula | null }
  /// True when variable variable is used as a input variable
  /// despite having formulas
  input?: boolean
  label?: string
  last_value_still_valid_on?: string
  /// The names of the (indirect) input variables that are added in the formula.
  /// These variables are highlighted in UI.
  linked_added_variables?: string[]
  /// The names of the (indirect) input variables that are not added in the formula.
  /// For example "part de quotient familial" or "revenu fiscal de référence" are
  /// used to calculate "impôt sur le revenu" but are not added.
  /// These variables are highlighted in UI.
  linked_other_variables?: string[]
  /// The names of the (indirect) output variables that are highlighted in UI.
  linked_output_variables?: string[]
  name: string
  /// True when latest formula of variable needs to be updated or set to null
  /// or...
  obsolete?: boolean
  possible_values?: { [name: string]: string }
  reference?: ReferencesByInstant
  referring_variables?: string[]
  start_line_number: number
  stop_line_number: number
  /// Unit added by customizations or decompositions
  /// Used when variable has a "float" value_type.
  unit?: string
  /// Short label of variable
  short_label?: string
  value_type: "bool" | "date" | "Enum" | "float" | "int" | "str"
}

export interface VariableByName {
  [name: string]: Variable
}

export type VariableValue = boolean | number | string

export type VariableValues = boolean[] | number[] | string[]

export function getVariableFormula(
  { formulas }: Variable,
  date: string,
): Formula | null {
  if (formulas === undefined) {
    return null
  }
  for (const bestDate of Object.keys(formulas).sort().reverse()) {
    if (bestDate <= date) {
      return formulas[bestDate]
    }
  }
  return null
}

export function getVariableLatestFormulaDate({
  formulas,
}: Variable): string | null {
  if (formulas === undefined) {
    return null
  }
  const latestDate = Object.keys(formulas).sort().reverse()[0] ?? null
  return formulas[latestDate] === null ? null : latestDate
}

export function newFormulaRepositoryUrl(
  metadata: Metadata,
  formula: Formula,
): string | undefined {
  const packageName = formula.file_path.split("/")[0]
  for (const packageMetadata of metadata.packages) {
    if (packageMetadata.name === packageName) {
      return `${packageMetadata.repository_url}/blob/${
        packageMetadata.version
      }/${formula.file_path}#L${formula.start_line_number}-L${
        formula.stop_line_number - 1
      }`
    }
  }
  return undefined
}

export function newVariableRepositoryUrl(
  metadata: Metadata,
  variable: Variable,
): string | undefined {
  const packageName = variable.file_path.split("/")[0]
  for (const packageMetadata of metadata.packages) {
    if (packageMetadata.name === packageName) {
      return `${packageMetadata.repository_url}/blob/${
        packageMetadata.version
      }/${variable.file_path}#L${variable.start_line_number}-L${
        variable.stop_line_number - 1
      }`
    }
  }
  return undefined
}
