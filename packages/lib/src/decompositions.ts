import type { ReferencesByInstant } from "./references"

export interface Customization extends DecompositionCore {
  description?: ReferencesByInstant
  input?: boolean
  /// The names of the (indirect) input variables that are added in the formula.
  /// These variables are highlighted in UI.
  linked_added_variables?: string[]
  /// The names of the (indirect) input variables that are not added in the formula.
  /// For example "part de quotient familial" or "revenu fiscal de référence" are
  /// used to calculate "impôt sur le revenu" but are not added.
  /// These variables are highlighted in UI.
  linked_other_variables?: string[]
  main_parameters?: string[]
  reference?: ReferencesByInstant
  variables?: string[]
}

export type CustomizationByName = { [name: string]: Customization }

export interface Decomposition extends DecompositionCore {
  label: string
}

export type DecompositionByName = { [name: string]: Decomposition }

interface DecompositionCore {
  children?: DecompositionReference[]
  /// Used only when variable is virtual (otherwise it is in variable.description).
  description?: ReferencesByInstant
  hidden?: boolean
  label?: string
  last_value_still_valid_on?: string
  obsolete?: boolean
  options?: DecompositionOptions[]
  /// Used only when variable is virtual (otherwise it is in variable.reference).
  reference?: ReferencesByInstant
  short_label?: string
  /// True when variable only exists in decomposition and not in OpenFisca-France
  virtual?: boolean
}

export type DecompositionOptions = VariablesOptions | WaterfallOptions

export interface DecompositionReference {
  name: string
  negate?: boolean
}

export interface VariablesCustomization {
  hidden?: boolean
}

export type VariablesOptions = {
  else?: VariablesCustomization
  then?: VariablesCustomization
} & {
  [name: string]: boolean[] | string[]
}

export interface WaterfallCustomization {
  children?: DecompositionReference[]
  hidden?: boolean
}

export interface WaterfallOptions {
  else?: WaterfallCustomization
  then?: WaterfallCustomization
  waterfall?: string[]
}
