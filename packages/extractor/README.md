# OpenFisca JSON Model Extractor

## _Tools to handle informations extracted in JSON or YAML format from OpenFisca parameters, variables, etc_

_Tools to extract and export JSON definitions and data from OpenFisca parameters, variables, etc_
