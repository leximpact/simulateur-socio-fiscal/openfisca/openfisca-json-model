def export_entities(tax_benefit_system):
    return {entity.key: export_entity(entity) for entity in tax_benefit_system.entities}


def export_entity(entity):
    entity_export = dict(
        key=entity.key,
    )
    doc = entity.doc
    if doc:
        doc = doc.strip()
    if doc:
        entity_export["documentation"] = doc
    if entity.is_person:
        entity_export["is_person"] = True
    else:
        entity_export["roles"] = [export_role(role) for role in entity.roles]
    if entity.label:
        entity_export["label"] = entity.label
    # TODO: Add to OpenFisca.
    # if entity.label_plural:
    #     entity_export["label_plural"] = entity.label_plural
    if entity.plural:
        entity_export["key_plural"] = entity.plural
    return entity_export


def export_role(role):
    role_export = dict(
        key=role.key,
    )
    doc = role.doc
    if doc:
        doc = doc.strip()
    if doc:
        role_export["documentation"] = doc
    if role.label:
        role_export["label"] = role.label
    # TODO: Add to OpenFisca.
    # if role.label_plural:
    #     role_export["label_plural"] = role.label_plural
    if role.plural:
        role_export["key_plural"] = role.plural
    if role.max:
        role_export["max"] = role.max
    if role.subroles:
        if role.max is not None:
            assert role.max >= len(role.subroles)
        role_export["subroles"] = [export_role(role) for role in role.subroles]
    return role_export
