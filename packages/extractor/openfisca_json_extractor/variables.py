import ast
import datetime
from enum import Enum
import inspect
import textwrap

import numpy
from openfisca_core import periods
from openfisca_core.entities import Entity, GroupEntity
from openfisca_core.variables import Variable
from openfisca_web_api.loader.variables import get_next_day

from .ast_to_json import ast_to_json
from .references import convert_single_reference
from .tax_benefit_systems import get_tax_benefit_system_packages_dir


class VariablesExporter:
    openfisca_parameters_export = None
    package_metadata = None
    tax_benefit_system = None

    def __init__(self, tax_benefit_system, openfisca_parameters_export):
        self.openfisca_parameters_export = openfisca_parameters_export
        self.packages_dir = get_tax_benefit_system_packages_dir(tax_benefit_system)
        self.tax_benefit_system = tax_benefit_system

    def export_variable(self, variable):
        assert isinstance(variable, Variable)
        absolute_file_path = inspect.getsourcefile(variable.__class__)
        file_path = absolute_file_path.replace(self.packages_dir, "")
        try:
            source_code_lines, start_line_number = inspect.getsourcelines(
                variable.__class__
            )
        except OSError:
            # Variable has been generated automatically.
            start_line_number = 0
            source_code_lines = ""
        export = dict(
            file_path=file_path,
            start_line_number=start_line_number,
            stop_line_number=start_line_number + len(source_code_lines),
        )
        for key, value in variable.__dict__.items():
            if key == "baseline_variable":
                if value is not None:
                    assert isinstance(value, Variable), value
                    export[key] = value.name
            elif key == "calculate_output":
                if value is not None:
                    assert callable(value), value
                    assert value.__name__ in (
                        "calculate_output_add",
                        "calculate_output_divide",
                    ), value
                    export[key] = value.__name__
            elif key == "cerfa_field":
                if value is not None:
                    assert (
                        isinstance(value, str)
                        or isinstance(value, dict)
                        and all(
                            isinstance(item_key, int) and isinstance(item_value, str)
                            for item_key, item_value in value.items()
                        )
                    ), value
                    export[key] = value
            elif key == "default_value":
                assert value is not None
                assert isinstance(
                    value, (bool, datetime.date, Enum, float, int, str)
                ), value
                export[key] = (
                    value.isoformat()
                    if isinstance(value, datetime.date)
                    else value.name
                    if isinstance(value, Enum)
                    else value
                )
            elif key == "definition_period":
                assert isinstance(value, str), value
                assert value in ("eternity", "month", "year"), value
                export[key] = value
            elif key in ("documentation", "label"):
                if value is not None:
                    assert isinstance(value, str), value
                    export[key] = value
            elif key == "dtype":
                assert (
                    value == bool
                    or isinstance(value, str)
                    or issubclass(value, numpy.generic)
                    or issubclass(value, object)
                ), value
                export[key] = value if isinstance(value, str) else value.__name__
            elif key == "end":
                # Handled in formulas, below.
                # export["formulas"][get_next_day(variable.end)] = None
                pass
            elif key == "entity":
                assert isinstance(value, Entity) or isinstance(value, GroupEntity), value
                export[key] = value.key
            elif key == "formulas":
                assert value is not None
                assert isinstance(value, dict) and all(
                    periods.INSTANT_PATTERN.match(item_key) and callable(item_value)
                    for item_key, item_value in value.items()
                ), value
                formula_by_start_date_export = {}
                for start_date, formula in value.items():
                    source_code_lines, start_line_number = inspect.getsourcelines(
                        formula
                    )
                    source_code = textwrap.dedent("".join(source_code_lines))
                    try:
                        module_ast = ast.parse(source_code)
                    except SyntaxError:
                        print("Error for variable:", variable.name)
                        print(source_code)
                        raise
                    assert isinstance(module_ast, ast.Module)
                    assert len(module_ast.body) == 1
                    formula_ast = module_ast.body[0]
                    formula_export = dict(
                        ast=ast_to_json(formula_ast),
                        file_path=file_path,
                        source_code=source_code,
                        start_line_number=start_line_number,
                        stop_line_number=start_line_number + len(source_code_lines),
                    )
                    if formula.__doc__:
                        formula_export["documentation"] = textwrap.dedent(
                            formula.__doc__
                        )
                    formula_by_start_date_export[start_date] = formula_export
                if variable.end:
                    formula_by_start_date_export[get_next_day(variable.end)] = None
                if len(formula_by_start_date_export) > 0:
                    export[key] = formula_by_start_date_export
            elif key == "introspection_data":
                # Ignore introspection_data added by recent versions of openfisca-core.
                pass
            elif key in ("is_neutralized", "is_period_size_independent"):
                assert isinstance(value, bool)
                if value is True:
                    export[key] = value
            elif key == "json_type":
                if value is not None:
                    assert isinstance(value, str)
                    assert value in (
                        "boolean",
                        "integer",
                        "number",
                        "string",
                    ), value
                    export[key] = value
            elif key == "max_length":
                if value is not None:
                    assert isinstance(value, int), value
                    export[key] = value
            elif key == "name":
                assert isinstance(value, str)
                export[key] = value
            elif key == "possible_values":
                assert issubclass(value, Enum), value
                export[key] = {item.name: item.value for item in list(value)}
            elif key == "reference":
                if value is not None:
                    assert isinstance(value, list) and all(
                        isinstance(item_value, str) for item_value in value
                    ), value
                    if len(value) > 0:
                        export[key] = {
                            "0001-01-01": [
                                convert_single_reference(item) for item in value
                            ]
                        }
            elif key == "set_input":
                if value is not None:
                    assert callable(value), value
                    assert value.__name__ in (
                        "set_input_dispatch_by_period",
                        "set_input_divide_by_period",
                    ), value
                    export[key] = value.__name__
            elif key == "unit":
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            elif key == "value_type":
                assert value in (bool, datetime.date, float, int, str) or issubclass(
                    value, Enum
                ), value
                export[key] = value.__name__
            else:
                print("Variable - Unhandled key:", key, "for:", variable.name)
        return export


def export_variables(tax_benefit_system, openfisca_parameters_export):
    variablesExporter = VariablesExporter(
        tax_benefit_system, openfisca_parameters_export
    )
    return {
        name: variablesExporter.export_variable(variable)
        for name, variable in tax_benefit_system.variables.items()
    }
