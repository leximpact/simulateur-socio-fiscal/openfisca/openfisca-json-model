import importlib
import inspect
import os

import pkg_resources


def export_tax_benefit_system_package_metadata_by_name(tax_benefit_system):
    baseline = tax_benefit_system
    package_metadata_by_name = {}
    while baseline is not None:
        module = inspect.getmodule(baseline)
        package_name = module.__name__.split(".")[0]
        try:
            distribution = pkg_resources.get_distribution(package_name)
        except pkg_resources.DistributionNotFound:
            package_metadata = {"name": package_name}
        else:
            package = importlib.import_module(package_name)
            package_file_path = inspect.getsourcefile(package)
            # Remove __init__.py from path.
            package_dir = os.path.dirname(package_file_path)
            package_metadata = {
                "dir": package_dir,
                "name": package_name,
                "version": distribution.version,
            }
            url = None
            for metadata in distribution._get_metadata(distribution.PKG_INFO):
                if "Home-page" in metadata:
                    url = metadata.split(":", 1)[1].strip(" ")
                    break
                if "Project-URL: Repository" in metadata:
                    url = metadata.split(",", 1)[1].strip(" ")
            if url is not None:
                package_metadata["repository_url"] = url
        package_metadata_by_name[package_metadata["name"]] = package_metadata
        baseline = baseline.baseline
        if baseline is None:
            package_metadata["tax_benefit_system"] = True
    return package_metadata_by_name


def get_tax_benefit_system_packages_dir(tax_benefit_system):
    """Retrieve directory containing the packages of the tax-benefit system and its
    reforms."""
    baseline = tax_benefit_system
    packages_dir = None
    while baseline is not None:
        module = inspect.getmodule(baseline)
        package_name = module.__name__.split(".")[0]
        package = importlib.import_module(package_name)
        package_file_path = inspect.getsourcefile(package)
        # Remove __init__.py from path.
        package_dir = os.path.dirname(package_file_path)
        package_parent_dir = os.path.dirname(package_dir)
        if packages_dir is None:
            packages_dir = package_parent_dir
        else:
            assert packages_dir == package_parent_dir
        baseline = baseline.baseline
    return packages_dir + os.path.sep
