#! /usr/bin/env python


"""Export an OpenFisca tax benefit system to JSON."""


import argparse
import importlib.metadata
import json
import logging
import os
import shutil
import sys

from openfisca_core.scripts import build_tax_benefit_system

from ..parameters import export_parameters
from ..tax_benefit_systems import export_tax_benefit_system_package_metadata_by_name
from ..variables import export_variables


args = None


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-c",
        "--country-package",
        action="store",
        help="country package to use.",
    )
    parser.add_argument(
        "-d",
        "--data-dir",
        action="store",
        help="path of directory that will contain the exported JSON files.",
    )
    parser.add_argument(
        "-e",
        "--extension",
        action="store",
        help="extension to load",
        nargs="*",
    )
    parser.add_argument(
        "-r",
        "--reform",
        action="store",
        help="Python path of reform to apply to the country package",
        nargs="*",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="increase output verbosity",
    )
    global args
    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.WARNING, stream=sys.stdout
    )

    with open(
        os.path.join(args.data_dir, "metadata.json"), encoding="utf-8"
    ) as metadata_file:
        metadata = json.load(metadata_file)
    package_metadata_by_name = {
        package_metadata["name"]: package_metadata
        for package_metadata in metadata["packages"]
    }
    reforms_metadata = metadata["reforms"] = []

    reforms_dir = os.path.join(args.data_dir, "reforms")
    if os.path.exists(reforms_dir):
        shutil.rmtree(reforms_dir)
    os.makedirs(reforms_dir)

    tax_benefit_system = build_tax_benefit_system(
        args.country_package,
        args.extension,
        args.reform,
    )

    for reform_entry_point in importlib.metadata.entry_points().get(
        "openfisca.reforms", []
    ):
        reform_name = reform_entry_point.name
        reform_class = reform_entry_point.load()
        reform = reform_class(tax_benefit_system)

        reforms_metadata.append(
            {"label": reform_class.name or reform_name, "name": reform_name}
        )

        new_package_metadata_by_name = (
            export_tax_benefit_system_package_metadata_by_name(reform)
        )
        package_metadata_by_name.update(new_package_metadata_by_name)

        parameters_export = export_parameters(reform)
        variables_export = export_variables(reform, parameters_export)

        reform_dir = os.path.join(reforms_dir, reform_name)
        os.makedirs(reform_dir)

        with open(
            os.path.join(reform_dir, "raw_processed_parameters.json"),
            "w",
            encoding="utf-8",
        ) as parameters_file:
            json.dump(
                parameters_export,
                parameters_file,
                ensure_ascii=False,
                indent=2,
                sort_keys=True,
            )

        variables_dir = os.path.join(reform_dir, "variables")
        if os.path.exists(variables_dir):
            shutil.rmtree(variables_dir)
        os.makedirs(variables_dir)
        for variable_name, variable_export in variables_export.items():
            if args.verbose:
                print(f'Writing variable "{variable_name}"…')
            with open(
                os.path.join(variables_dir, f"{variable_name}.json"),
                "w",
                encoding="utf-8",
            ) as variable_file:
                json.dump(
                    variable_export,
                    variable_file,
                    ensure_ascii=False,
                    indent=2,
                    sort_keys=True,
                )

    packages_metadata = sorted(
        package_metadata_by_name.values(),
        key=lambda package_metadata: package_metadata["name"],
    )
    metadata["packages"] = packages_metadata
    with open(
        os.path.join(args.data_dir, "metadata.json"), "w", encoding="utf-8"
    ) as metadata_file:
        json.dump(
            metadata,
            metadata_file,
            ensure_ascii=False,
            indent=2,
            sort_keys=True,
        )


if __name__ == "__main__":
    sys.exit(main())
