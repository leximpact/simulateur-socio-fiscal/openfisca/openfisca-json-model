import re

from openfisca_core import periods


HTTP_URL_RE = re.compile(
    r"^(http|https)://[a-zA-Z0-9@:%._+~#?&/=]{2,256}\.[a-z]{2,6}([-a-zA-Z0-9@:%._+~#?&/=;]*)$",  # noqa: E501
    flags=re.I,
)
JSESSIONID_RE = re.compile(r";jsessionid=.*?(?=\?)")


def check_reference(value):
    return (
        isinstance(value, str)
        or isinstance(value, list)
        and all(
            isinstance(item, str)
            or isinstance(item, dict)
            and all(
                item_key in ("href", "title")
                and (item_value is None or isinstance(item_value, str))
                for item_key, item_value in item.items()
            )
            for item in value
        )
        or isinstance(value, dict)
        and (
            all(
                item_key in ("href", "title")
                and (item_value is None or isinstance(item_value, str))
                for item_key, item_value in value.items()
            )
            or all(
                periods.INSTANT_PATTERN.match(item_key)
                and check_reference_at_instant(item_value)
                for item_key, item_value in value.items()
            )
        )
    )


def check_reference_at_instant(value):
    return (
        isinstance(value, str)
        or isinstance(value, dict)
        and all(
            item_key in ("href", "title")
            and (item_value is None or isinstance(item_value, str))
            for item_key, item_value in value.items()
        )
        or isinstance(value, list)
        and all(
            isinstance(item, str)
            or isinstance(item, dict)
            and all(
                item_key in ("href", "title")
                and (item_value is None or isinstance(item_value, str))
                for item_key, item_value in item.items()
            )
            for item in value
        )
    )


def convert_reference(value):
    return (
        {instant: convert_reference_at_instant(item) for instant, item in value.items()}
        if isinstance(value, dict)
        and all(
            periods.INSTANT_PATTERN.match(item_key)
            and check_reference_at_instant(item_value)
            for item_key, item_value in value.items()
        )
        else {"0001-01-01": convert_reference_at_instant(value)}
    )


def convert_reference_at_instant(value):
    return (
        [convert_single_reference(item) for item in value]
        if isinstance(value, list)
        else [convert_single_reference(value)]
    )


def convert_single_reference(value):
    # `reference` is either a string or a dict with `href` and/or `title`.
    reference = (
        (dict(href=value) if HTTP_URL_RE.match(value) else dict(title=value))
        if isinstance(value, str)
        else {
            item_name: item_value
            for item_name, item_value in value.items()
            if item_value is not None
        }
    )
    href = reference.get("href")
    if href is not None:
        reference["href"] = JSESSIONID_RE.sub("", href)
    return reference
