import ast


def ast_to_json(node: ast.AST):
    node_json = {"ast_class": node.__class__.__name__}

    # Visit fields.
    for field_name in node._fields:
        field_value = getattr(node, field_name)
        field_value = (
            ast_to_json(field_value)
            if isinstance(field_value, ast.AST)
            else [ast_to_json(item) for item in field_value]
            if isinstance(field_value, (list, tuple))
            else field_value.decode("ascii")
            if isinstance(field_value, bytes)
            else field_value
        )
        if field_value is not None:
            node_json[field_name] = field_value

    # Visit attributes (ie 'lineno', 'col_offset', 'end_lineno', 'end_col_offset').
    for attribute_name in node._attributes:
        attribute_value = getattr(node, attribute_name, None)
        if attribute_value is not None:
            node_json[attribute_name] = attribute_value

    return node_json
