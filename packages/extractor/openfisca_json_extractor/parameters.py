import math

from openfisca_core import periods
from openfisca_core.parameters import (
    Bracket,
    Parameter,
    ParameterAtInstant,
    ParameterNode,
    Scale,
)

from .references import (
    check_reference,
    convert_reference,
)
from .tax_benefit_systems import get_tax_benefit_system_packages_dir


class ParametersExporter:
    packages_dir = None
    tax_benefit_system = None

    def __init__(self, tax_benefit_system):
        self.packages_dir = get_tax_benefit_system_packages_dir(tax_benefit_system)
        self.tax_benefit_system = tax_benefit_system

    def export_bracket(self, bracket):
        assert isinstance(bracket, Bracket)
        export = {}
        for key, value in bracket.__dict__.items():
            if key in ("amount", "average_rate", "base", "rate", "threshold"):
                assert isinstance(value, Parameter), (value, type(value))
                parameter_export = self.export_parameter(value, None)
                assert all(
                    key in ("file_path", "values") for key in parameter_export.keys()
                ), parameter_export
                export[key] = parameter_export["values"]
            elif key == "children":
                assert isinstance(value, dict)
                # Ignore children because they are also visible as bracket attributes.
            elif key in ("description", "documentation"):
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            elif key == "documentation_start":
                if value is not None:
                    assert isinstance(value, bool)
                    export[key] = value
            elif key == "file_path":
                if value is not None:
                    assert isinstance(value, str)
                    # Ignore file path, because it is the same as the one in the parent
                    # parameter.
                    # if value.startswith(self.packages_dir):
                    #     export[key] = value.replace(self.packages_dir, "")
                    # else:
                    #     # Ignore file_path when it is not in parameters directory.
                    #     pass
            elif key == "metadata":
                merge_metadata_export(
                    export, self.export_parameter_metadata(bracket, value)
                )
            elif key == "name":
                assert isinstance(value, str)
                # Ignore name ,because it is generated from the name of the parent
                # parameter, by adding "[index of bracket]".
                # export[key] = value
            else:
                print("Bracket - Unhandled key:", key, "at:", bracket.file_path)
        return export

    def export_parameter(self, parameter, ids):
        export = {}
        if ids is not None:
            # Recreate name from IDs, because OpenFisca preprocessors are susceptible
            # to corrupt name.
            export["name"] = ".".join(ids)
        if isinstance(parameter, Parameter):
            for key, value in parameter.__dict__.items():
                if key in ("description", "documentation"):
                    if value is not None:
                        assert isinstance(value, str)
                        export[key] = value
                elif key == "documentation_start":
                    if value is not None:
                        assert isinstance(value, bool)
                        export[key] = value
                elif key == "file_path":
                    if value is not None:
                        assert isinstance(value, str)
                        if value.startswith(self.packages_dir):
                            export[key] = value.replace(self.packages_dir, "")
                        else:
                            # Ignore file_path when it is not in parameters directory.
                            pass
                elif key == "metadata":
                    merge_metadata_export(
                        export, self.export_parameter_metadata(parameter, value)
                    )
                elif key == "name":
                    # Don't export name attribute here, because it is (eventually)
                    # regenerated from ids above.
                    # assert isinstance(value, str)
                    # export[key] = value
                    pass
                elif key == "reference":
                    assert check_reference(value), value
                    export[key] = convert_reference(value)
                elif key == "values_history":
                    assert value == parameter
                elif key == "values_list":
                    assert isinstance(value, list) and all(
                        isinstance(item_value, ParameterAtInstant)
                        for item_value in value
                    )
                    export["values"] = {
                        instant: parameter_at_instant
                        for instant, parameter_at_instant in (
                            self.export_parameter_at_instant(parameter, item_value)
                            for item_value in value
                        )
                    }
                else:
                    print("Parameter - Unhandled key:", key, "at:", parameter.file_path)
        elif isinstance(parameter, ParameterNode):
            for key, value in parameter.__dict__.items():
                if key == "children":
                    assert isinstance(value, dict)
                    for child_id, child_parameter in value.items():
                        export[child_id] = self.export_parameter(
                            child_parameter, None if ids is None else ids + [child_id]
                        )
                elif key in ("description", "documentation"):
                    if value is not None:
                        assert isinstance(value, str)
                        export[key] = value
                elif key == "documentation_start":
                    if value is not None:
                        assert isinstance(value, bool)
                        export[key] = value
                elif key == "file_path":
                    if value is not None:
                        assert isinstance(value, str)
                        if value.startswith(self.packages_dir):
                            export[key] = value.replace(self.packages_dir, "")
                        else:
                            # Ignore file_path when it is not in parameters directory.
                            pass
                elif key == "metadata":
                    merge_metadata_export(
                        export, self.export_parameter_metadata(parameter, value)
                    )
                elif key == "name":
                    # Don't export name attribute here, because it is (eventually)
                    # regenerated from ids above.
                    # assert isinstance(value, str)
                    # export[key] = value
                    pass
                elif key in parameter.children:
                    continue
                else:
                    print(
                        "ParameterNode - Unhandled key:",
                        key,
                        "at:",
                        parameter.file_path,
                    )
        elif isinstance(parameter, Scale):
            for key, value in parameter.__dict__.items():
                if key == "brackets":
                    assert (
                        isinstance(value, list)
                        and len(value) > 0
                        and all(isinstance(item_value, Bracket) for item_value in value)
                    ), value
                    export[key] = [self.export_bracket(bracket) for bracket in value]
                elif key in ("description", "documentation"):
                    if value is not None:
                        assert isinstance(value, str)
                        export[key] = value
                elif key == "documentation_start":
                    if value is not None:
                        assert isinstance(value, bool)
                        export[key] = value
                elif key == "file_path":
                    if value is not None:
                        assert isinstance(value, str)
                        if value.startswith(self.packages_dir):
                            export[key] = value.replace(self.packages_dir, "")
                        else:
                            # Ignore file_path when it is not in parameters directory.
                            pass
                elif key == "metadata":
                    merge_metadata_export(
                        export, self.export_parameter_metadata(parameter, value)
                    )
                elif key == "name":
                    # Don't export name attribute here, because it is (eventually)
                    # regenerated from ids above.
                    # assert isinstance(value, str)
                    # export[key] = value
                    pass
                else:
                    print("Scale - Unhandled key:", key, "at:", parameter.file_path)
        else:
            print("Unkwnown class of parameter:", parameter.__class__.__name__)
        return export

    def export_parameter_at_instant(self, parameter, parameter_at_instant):
        assert isinstance(parameter_at_instant, ParameterAtInstant)
        export = {}
        instant = None
        for key, value in parameter_at_instant.__dict__.items():
            if key == "file_path":
                if value is not None:
                    assert isinstance(value, str)
                    assert value == parameter.file_path
                    # Ignore file path, because it is the same as the one in the parent
                    # parameter.
                    # if value.startswith(self.packages_dir):
                    #     export[key] = value.replace(self.packages_dir, "")
                    # else:
                    #     # Ignore file_path when it is not in parameters directory.
                    #     pass
            elif key == "instant_str":
                assert isinstance(value, str)
                instant = value
            elif key == "metadata":
                # Ignore ParameterAtInstant metadata, because they are initiated with
                # the metadata of the parent parameter. A ParameterAtInstant doesn't
                # have its own metadata.
                pass
            elif key == "name":
                assert isinstance(value, str)
                # Ignore name, because it is generated from the name of the parent
                # parameter, by adding "[instant]".
                # export[key] = value
            elif key == "value":
                assert (
                    value is None
                    or isinstance(value, (float, int, str))
                    or isinstance(value, list)
                    and all(isinstance(item_value, str) for item_value in value)
                ), value
                export[key] = (
                    None if isinstance(value, float) and math.isnan(value) else value
                )
            else:
                print(
                    "ParameterAtInstant - Unhandled key:",
                    key,
                    "at:",
                    parameter_at_instant.file_path,
                )
        assert instant is not None
        return instant, export

    def export_parameter_metadata(self, parameter, metadata):
        assert isinstance(metadata, dict)
        if len(metadata) == 0:
            return None
        export = {}
        for key, value in metadata.items():
            if key == "amount_unit":
                if value is not None:
                    assert (
                        parameter.__class__.__name__ == "ParameterScale"
                    ), parameter.__class__.__name__
                    assert (
                        isinstance(value, str)
                        or isinstance(value, dict)
                        and all(
                            periods.INSTANT_PATTERN.match(item_key)
                            and isinstance(item_value, str)
                            for item_key, item_value in value.items()
                        )
                    )
                    export[key] = value
            elif key in ("description", "documentation"):
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            elif key == "documentation_start":
                if value is not None:
                    assert isinstance(value, bool)
                    export[key] = value
            elif key == "inflator":
                # Added for Leximpact.
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            elif key == "inflator_reference":
                if value is not None:
                    assert check_reference(value), value
                    export[key] = convert_reference(value)
            elif key == "ipp_csv_id":
                # Added for harmonization between "Barèmes IPP" & OpenFisca-France
                # parameters
                if value is not None:
                    if isinstance(parameter, Scale):
                        assert (
                            isinstance(value, dict)
                            and all(
                                item_key
                                in (
                                    "amount",
                                    "average_rate",
                                    "base",
                                    "rate",
                                    "threshold",
                                )
                                and isinstance(item_value, str)
                                for item_key, item_value in value.items()
                            )
                            or isinstance(value, str)
                        ), (
                            parameter,
                            value,
                        )
                    else:
                        assert isinstance(parameter, Parameter), (parameter, metadata)
                        assert isinstance(value, str)
                    export[key] = value
            elif key == "label_en":
                # Added for Barèmes IPP
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            elif key == "last_value_still_valid_on":
                # Added for Leximpact.
                assert isinstance(value, str)
                assert periods.INSTANT_PATTERN.match(value), value
                export[key] = value
            elif key == "notes":
                # Added for Leximpact
                if value is not None:
                    assert check_reference(value), value
                    export[key] = convert_reference(value)
            elif key == "official_journal_date":
                # Added for Leximpact
                if value is not None:
                    assert isinstance(value, dict) and all(
                        periods.INSTANT_PATTERN.match(item_key)
                        and all(
                            periods.INSTANT_PATTERN.match(instant.strip())
                            for instant in item_value.split(";")
                        )
                        for item_key, item_value in value.items()
                    ), value
                    export[key] = value
            elif key == "order":
                # Added for harmonization between "Barèmes IPP" & OpenFisca-France
                # parameters
                assert isinstance(parameter, ParameterNode)
                if value is not None:
                    assert isinstance(value, list)
                    assert all(isinstance(item, str) for item in value)
                    export[key] = value
            elif key == "period":
                # Used by UK
                if value is not None:
                    assert value in (
                        "hour",  # UK
                        "month",  # UK
                        "week",  # UK
                        "year",  # UK
                    ), value
                    export[key] = value
            elif key == "rate_unit":
                if value is not None:
                    assert (
                        parameter.__class__.__name__ == "ParameterScale"
                    ), f"{parameter.__class__.__name__} {parameter.name} contains {key}"
                    assert (
                        isinstance(value, str)
                        or isinstance(value, dict)
                        and all(
                            periods.INSTANT_PATTERN.match(item_key)
                            and isinstance(item_value, str)
                            for item_key, item_value in value.items()
                        )
                    )
                    export[key] = value
            elif key == "reference":
                if value is not None:
                    assert check_reference(value), value
                    export[key] = convert_reference(value)
            elif key == "threshold_unit":
                if value is not None:
                    assert (
                        parameter.__class__.__name__ == "ParameterScale"
                    ), parameter.__class__.__name__
                    assert (
                        isinstance(value, str)
                        or isinstance(value, dict)
                        and all(
                            periods.INSTANT_PATTERN.match(item_key)
                            and isinstance(item_value, str)
                            for item_key, item_value in value.items()
                        )
                    )

                    export[key] = value
            elif key == "type":
                assert (
                    parameter.__class__.__name__ == "ParameterScale"
                ), parameter.__class__.__name__
                assert value in ("marginal_rate", "single_amount"), value
                # Ignore type, because it is set by bracket:
                # export[key] = value
            elif key == "unit":
                if value is not None:
                    assert (
                        isinstance(value, str)
                        or isinstance(value, dict)
                        and all(
                            periods.INSTANT_PATTERN.match(item_key)
                            and isinstance(item_value, str)
                            for item_key, item_value in value.items()
                        )
                    )
                    export[key] = value
            elif key == "short_label":
                # Added for Leximpact.
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            elif key == "short_label_en":
                # Added for Barèmes IPP
                if value is not None:
                    assert isinstance(value, str)
                    export[key] = value
            else:
                print(
                    "Metadata - Unhandled key:",
                    key,
                    "=",
                    value,
                    "at:",
                    parameter.file_path,
                )
        return export


def export_parameters(tax_benefit_system):
    parameters_export = ParametersExporter(tax_benefit_system).export_parameter(
        tax_benefit_system.parameters, []
    )

    # Delete node added by preprocessing.
    del parameters_export["cotsoc"]

    return parameters_export


def merge_metadata_export(export, metadata_export):
    if metadata_export is not None:
        for key in ("description", "documentation"):
            value = metadata_export.get(key)
            if value is not None:
                if value != export.get(key):
                    assert export.get(key) is None, (
                        f"Key '{key}' is present both in parameter ({export[key]}) and"
                        + f" its metadata ({metadata_export[key]})"
                    )
                    export[key] = value
                del metadata_export[key]
        for key in ("documentation_start",):
            value = export.get(key)
            if value is not None:
                if value != metadata_export.get(key):
                    assert metadata_export.get(key) is None, (
                        f"Key '{key}' is present both in parameter ({export[key]}) and"
                        + f" its metadata ({metadata_export[key]})"
                    )
                    metadata_export[key] = value
                del export[key]
        if len(metadata_export) > 0:
            export["metadata"] = metadata_export
